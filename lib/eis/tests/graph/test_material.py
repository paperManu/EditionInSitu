import uuid
from unittest import TestCase, mock

from eis.graph.material import Material, MaterialTexture
from eis.graph.model import Model
from eis.graph.texture import Texture


class TestMaterialTexture(TestCase):
    # region Copy

    def test_copy1(self):
        texture = Texture()
        original = MaterialTexture(
            texture=texture,
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        copy = original.copy()

        self.assertNotEqual(original.texture, copy.texture)
        self.assertNotEqual(original.texture_id, copy.texture_id)
        self.assertEqual(original.usage, copy.usage)

        self.assertEqual(original.texture.name, copy.texture.name)
        self.assertEqual(original.texture.data, copy.texture.data)
        self.assertEqual(original.texture.format, copy.texture.format)
        self.assertEqual(original.texture.size_x, copy.texture.size_x)
        self.assertEqual(original.texture.size_y, copy.texture.size_y)
        self.assertEqual(original.texture.component_type, copy.texture.component_type)
        self.assertEqual(original.texture.compression, copy.texture.compression)

    def test_copy2(self):
        original = MaterialTexture(
            texture_id=uuid.uuid4(),
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        copy = original.copy()

        self.assertEqual(original.texture, copy.texture)
        self.assertEqual(original.texture_id, copy.texture_id)
        self.assertEqual(original.usage, copy.usage)

    def test_copy_shared1(self):
        texture = Texture()
        original = MaterialTexture(
            texture=texture,
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        copy = original.copy_shared()

        self.assertEqual(original.texture, copy.texture)
        self.assertEqual(original.texture_id, copy.texture_id)
        self.assertEqual(original.usage, copy.usage)

    def test_copy_shared2(self):
        original = MaterialTexture(
            texture_id=uuid.uuid4(),
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        copy = original.copy_shared()

        self.assertEqual(original.texture, copy.texture)
        self.assertEqual(original.texture_id, copy.texture_id)
        self.assertEqual(original.usage, copy.usage)

    # endregion

    # region Texture

    def test_setting_texture_retrieves_texture_id(self):
        mt = MaterialTexture()
        texture = Texture()
        mt.texture = texture
        self.assertEqual(mt.texture, texture)
        self.assertEqual(mt.texture_id, texture.uuid)

    def test_constructing_texture_retrieves_texture_id(self):
        texture = Texture()
        mt = MaterialTexture(texture=texture)
        self.assertEqual(mt.texture, texture)
        self.assertEqual(mt.texture_id, texture.uuid)

    def test_setting_texture_without_material(self):
        mt = MaterialTexture()
        texture = Texture()
        with mock.patch.object(texture, "added_to_model") as added, \
                mock.patch.object(texture, "removed_from_model") as removed:
            mt.texture = texture
            added.assert_not_called()
            removed.assert_not_called()

    def test_swapping_texture_without_material(self):
        mt = MaterialTexture()
        texture1 = Texture()
        mt.texture = texture1
        texture2 = Texture()
        with mock.patch.object(texture1, "added_to_model") as added1, \
                mock.patch.object(texture1, "removed_from_model") as removed1, \
                mock.patch.object(texture2, "added_to_model") as added2, \
                mock.patch.object(texture2, "removed_from_model") as removed2:
            mt.texture = texture2
            added1.assert_not_called()
            removed1.assert_not_called()
            added2.assert_not_called()
            removed2.assert_not_called()

    def test_removing_texture_without_material(self):
        mt = MaterialTexture()
        texture = Texture()
        mt.texture = texture
        with mock.patch.object(texture, "added_to_model") as added, \
                mock.patch.object(texture, "removed_from_model") as removed:
            mt.texture = None
            added.assert_not_called()
            removed.assert_not_called()

    def test_setting_texture(self):
        model = Model()
        material = Material()
        material.owners[model] = 1
        mt = MaterialTexture()
        mt.material = material
        texture = Texture()
        with mock.patch.object(texture, "added_to_model") as added, \
                mock.patch.object(texture, "removed_from_model") as removed:
            mt.texture = texture
            added.assert_called_once_with(model)
            removed.assert_not_called()

    def test_swapping_texture(self):
        model = Model()
        material = Material()
        material.owners[model] = 1
        mt = MaterialTexture()
        mt.material = material
        texture1 = Texture()
        mt.texture = texture1
        texture2 = Texture()
        with mock.patch.object(texture1, "added_to_model") as added1, \
                mock.patch.object(texture1, "removed_from_model") as removed1, \
                mock.patch.object(texture2, "added_to_model") as added2, \
                mock.patch.object(texture2, "removed_from_model") as removed2:
            mt.texture = texture2
            added1.assert_not_called()
            removed1.assert_called_once_with(model)
            added2.assert_called_once_with(model)
            removed2.assert_not_called()

    def test_removing_texture(self):
        model = Model()
        material = Material()
        material.owners[model] = 1
        mt = MaterialTexture()
        mt.material = material
        texture = Texture()
        mt.texture = texture
        with mock.patch.object(texture, "added_to_model") as added, \
                mock.patch.object(texture, "removed_from_model") as removed:
            mt.texture = None
            added.assert_not_called()
            removed.assert_called_once_with(model)

    # endregion

    # region Model

    def test_adding_to_model_retrieves_nothing(self):
        model = Model()
        mt = MaterialTexture()
        texture = Texture()
        with mock.patch.object(model, "get_texture_by_uuid", return_value=texture) as get, \
                mock.patch.object(texture, "added_to_model") as added:
            mt.added_to_model(model)
            get.assert_not_called()
            added.assert_not_called()

    def test_adding_to_model_retrieves_texture_if_id(self):
        model = Model()
        texture = Texture()
        mt = MaterialTexture(texture_id=texture.uuid)
        with mock.patch.object(model, "get_texture_by_uuid", return_value=texture) as get, \
                mock.patch.object(texture, "added_to_model") as added:
            mt.added_to_model(model)
            get.assert_called_once_with(texture.uuid)
            self.assertEqual(mt.texture, texture)
            added.assert_called_once_with(model)

    def test_adding_to_model_does_not_retrieve_texture_if_instance(self):
        model = Model()
        texture = Texture()
        mt = MaterialTexture(texture=texture)
        with mock.patch.object(model, "get_texture_by_uuid", return_value=texture) as get, \
                mock.patch.object(texture, "added_to_model") as added:
            mt.added_to_model(model)
            get.assert_not_called()
            self.assertEqual(mt.texture, texture)
            added.assert_called_once_with(model)

    def test_removing_from_model(self):
        model = Model()
        texture = Texture()
        mt = MaterialTexture(texture=texture)
        with mock.patch.object(texture, "removed_from_model") as removed:
            mt.removed_from_model(model)
            removed.assert_called_once_with(model)

    # endregion

    ...


class TestMaterial(TestCase):

    # region Copy

    def test_copy(self):
        texture = Texture()
        material_texture = MaterialTexture(
            texture=texture,
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        original = Material(
            name="Material",
            color=(0.1, 0.2, 0.3, 0.4),
            normal_map_strength=0.5,
            refraction=0.6,
            roughness=0.7,
            metallic=0.8,
            shading_model=Material.ShadingModel.EMISSIVE,
            material_textures=[material_texture]
        )

        with mock.patch.object(texture, "copy", wraps=texture.copy) as texture_copy:
            copy = original.copy()
            texture_copy.assert_called_once_with()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.color, copy.color)
        self.assertEqual(original.normal_map_strength, copy.normal_map_strength)
        self.assertEqual(original.refraction, copy.refraction)
        self.assertEqual(original.roughness, copy.roughness)
        self.assertEqual(original.metallic, copy.metallic)
        self.assertEqual(original.shading_model, copy.shading_model)
        self.assertNotEqual(original.material_textures, copy.material_textures)

    def test_copy_unless_managed(self):
        texture = Texture()
        texture.managed = True
        material_texture = MaterialTexture(texture=texture)
        original = Material(material_textures=[material_texture])

        with mock.patch.object(texture, "copy", wraps=texture.copy) as texture_copy:
            copy = original.copy()
            texture_copy.assert_not_called()

        self.assertEqual(len(copy.material_textures), 0)

    def test_copy_shared(self):
        texture = Texture()
        material_texture = MaterialTexture(
            texture=texture,
            usage=MaterialTexture.TextureUsage.BASE_COLOR
        )
        original = Material(
            name="Material",
            color=(0.1, 0.2, 0.3, 0.4),
            normal_map_strength=0.5,
            refraction=0.6,
            roughness=0.7,
            metallic=0.8,
            shading_model=Material.ShadingModel.EMISSIVE,
            material_textures=[material_texture]
        )

        copy = original.copy_shared()

        self.assertNotEqual(original, copy)
        self.assertNotEqual(original.uuid, copy.uuid)
        self.assertEqual(original.name, copy.name)
        self.assertEqual(original.color, copy.color)
        self.assertEqual(original.normal_map_strength, copy.normal_map_strength)
        self.assertEqual(original.refraction, copy.refraction)
        self.assertEqual(original.roughness, copy.roughness)
        self.assertEqual(original.metallic, copy.metallic)
        self.assertEqual(original.shading_model, copy.shading_model)
        self.assertNotEqual(original.material_textures, copy.material_textures)
        self.assertEqual(original.material_textures[0].texture, copy.material_textures[0].texture)

    def test_copy_shared_unless_managed(self):
        texture = Texture()
        texture.managed = True
        material_texture = MaterialTexture(texture=texture)
        original = Material(material_textures=[material_texture])

        with mock.patch.object(texture, "copy_shared", wraps=texture.copy_shared) as texture_copy:
            copy = original.copy_shared()
            texture_copy.assert_not_called()

        self.assertEqual(len(copy.material_textures), 0)

    # endregion

    # region Texture

    def test_add_texture(self):
        model = Model()
        material = Material()
        material.owners[model] = 1
        mt = MaterialTexture()
        with mock.patch.object(mt, "added_to_model") as added:
            material.add_texture(mt)
            self.assertEqual(material.material_textures, [mt])
            self.assertEqual(mt.material, material)
            added.assert_called_once_with(model)

    def test_remove_texture(self):
        model = Model()
        material = Material()
        material.owners[model] = 1
        mt = MaterialTexture()
        material.add_texture(mt)
        with mock.patch.object(mt, "removed_from_model") as removed:
            material.remove_texture(mt)
            self.assertEqual(material.material_textures, [])
            self.assertEqual(mt.material, None)
            removed.assert_called_once_with(model)

    # endregion

    # region Model

    def test_added_to_model(self):
        model = Model()
        texture = MaterialTexture()
        material = Material(material_textures=[texture])
        with mock.patch.object(model, "add_material") as add, \
                mock.patch.object(texture, "added_to_model") as added:
            material.added_to_model(model)
            add.assert_called_once_with(material)
            added.assert_called_once_with(model)

    def test_removed_from_model(self):
        model = Model()
        texture = MaterialTexture()
        material = Material(material_textures=[texture])
        with mock.patch.object(model, "remove_material") as remove, \
                mock.patch.object(texture, "removed_from_model") as removed:
            material.removed_from_model(model)
            remove.assert_called_once_with(material)
            removed.assert_called_once_with(model)

    # endregion

    ...
