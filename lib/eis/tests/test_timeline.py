from unittest import TestCase

from eis.graph.animation_curve import AnimationCurve
from eis.graph.model import Model
from eis.graph.scene import Scene
from eis.timeline import Timeline
from .mock.editor import MockEditor_extended


class MockModel(Model):
    def __init__(self):
        super().__init__()


class MockScene(Scene):
    def __init__(self):
        super().__init__(model=MockModel())


class EditorTimeline(MockEditor_extended):
    def __init__(self):
        super().__init__()
        defaultEditorConfig = {"timeline.looping":True, "timeline.duration":60}
        self._config = defaultEditorConfig


class TestTimeline(TestCase):

    def setUp(self):
       editor = EditorTimeline()
       self.timeline = Timeline(editor)

    def test_time(self):
        self.timeline.time = 2.5
        self.assertEqual(self.timeline.time, 2.5)

        # Looping ON
        # if time < 0.0, go to the end of the timeline
        self.timeline.time = -1.0
        self.assertEqual(self.timeline.time, self.timeline._editor.config.get("timeline.duration"))
        # if time > timeline.duration -> go to beginning of timeline
        self.timeline.time = 100.0
        self.assertEqual(self.timeline.time, 0.0)

       # update config, set looping to false
        self.timeline._editor.config.update({"timeline.looping":False})

        # Looping OFF
        # verify that you stay at 0.0 if the time provided is < 0.0
        self.timeline.time = -1.0
        self.assertEqual(self.timeline.time, 0.0)
        # if time > timeline.duration -> stay at the end of the timeline
        self.timeline.time = 100.0
        self.assertEqual(self.timeline.time, self.timeline._editor.config.get("timeline.duration"))

    def test_next_keyframe(self):
        self.timeline._editor.load_scene(MockScene())
        curve = AnimationCurve()
        curve.add_keyframe(0.0, 0.0)
        curve.add_keyframe(1.0, 1.0)
        curve.add_keyframe(2.0, 2.0)

        curve2 = AnimationCurve()
        curve2.add_keyframe(0.5, 0.5)
        curve2.add_keyframe(1.5, 1.5)
        curve2.add_keyframe(2.5, 2.5)

        self.timeline._editor.scene.model.add_animation_curve(curve)
        self.timeline._editor.scene.model.add_animation_curve(curve2)

        self.timeline.time = 1.0
        self.timeline.next_keyframe()
        self.assertEqual(self.timeline.time, 1.5)

    def test_previous_keyframe(self):
        self.timeline._editor.load_scene(MockScene())
        curve = AnimationCurve()
        curve.add_keyframe(0.0, 0.0)
        curve.add_keyframe(1.0, 1.0)
        curve.add_keyframe(2.0, 2.0)

        curve2 = AnimationCurve()
        curve2.add_keyframe(0.5, 0.5)
        curve2.add_keyframe(1.5, 1.5)
        curve2.add_keyframe(2.5, 2.5)

        self.timeline._editor.scene.model.add_animation_curve(curve)
        self.timeline._editor.scene.model.add_animation_curve(curve2)

        self.timeline.time = 1.0
        self.timeline.previous_keyframe()
        self.assertEqual(self.timeline.time, 0.5)

    def test_reset(self):
        self.timeline.reset()
        self.assertEqual(self.timeline.time, 0.0)
        self.timeline.reset(15.0)
        self.assertEqual(self.timeline.time, 15.0)

    def test_start(self):
        self.timeline.start()
        self.assertEqual(self.timeline._running, True)
        self.assertEqual(self.timeline._reverse, False)
        self.assertEqual(self.timeline._stop_time, None)

        # stop the timeline
        self.timeline.stop()

        self.timeline.start(reverse=True, stop_time=15.0)
        self.assertEqual(self.timeline._running, True)
        self.assertEqual(self.timeline._reverse, True)
        self.assertEqual(self.timeline._stop_time, 15.0)

    def test_step_animation(self):
        # looping on, reverse off
        self.timeline.start()
        self.timeline._step_animation(dt=15.0)
        self.assertEqual(self.timeline.time, 15.0)
        self.timeline._step_animation(dt=65.0)
        self.assertEqual(self.timeline.time, 0.0)

        self.timeline.stop()
        self.timeline.reset()

        self.timeline.start(stop_time=30)
        self.timeline._step_animation(dt=35.0)
        self.assertEqual(self.timeline.time, 30.0)

        self.timeline.stop()
        self.timeline.reset()

        # looping on, reverse on
        self.timeline.start(reverse=True)
        self.timeline._step_animation(dt=15.0)
        self.assertEqual(self.timeline.time, self.timeline._editor.config.get("timeline.duration"))

        self.timeline.stop()
        self.timeline.reset()

        # looping on, reverse on
        self.timeline.start(reverse=True, stop_time=30)
        self.timeline._step_animation(dt=15)
        self.assertEqual(self.timeline.time, 30.0)

        self.timeline.stop()
        self.timeline.reset()

       # looping off
        self.timeline._editor.config.update({"timeline.looping":False})

        self.timeline.start()
        self.timeline._step_animation(dt=65.0)
        self.assertEqual(self.timeline.time, self.timeline._editor.config.get("timeline.duration"))

        self.timeline.stop()
        self.timeline.reset()

        self.timeline.start(reverse=True)
        self.timeline._step_animation(dt=15.0)
        self.assertEqual(self.timeline.time, 0.0)

        self.timeline.stop()
        self.timeline.reset()

        self.timeline.start()
        self.timeline._step_animation(dt=30.0)
        self.timeline.stop()
        self.timeline.start(reverse=True)
        self.timeline._step_animation(dt=15.0)
        self.assertEqual(self.timeline.time, 15.0)

    def test_stop(self):
        self.timeline.stop()
        self.assertEqual(self.timeline.running, False)
