import os
import unittest
from math import pi
from unittest import TestCase
import numpy as np
from panda3d.core import Mat4, Quat, Vec3, NodePath

from eis.engine import defaultEngineConfig
from eis.engines.render_pipeline.converter import RenderPipelineConverter
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.graph.model import Model
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.euler import Euler, EulerOrder


class RenderPipelineEngineTest(TestCase):

    def test_rotation_chain(self):
        original_rot = Quaternion()
        self.assertEqual(original_rot, Quaternion([0., 0., 0., 1.]))

        euler_rot = Euler([0., pi / 4., pi / 4.])

        quat_rot = Quaternion.from_euler(euler_rot, order=EulerOrder.XYZ)
        self.assertAlmostEqual(quat_rot[0], 0.1464466, 5)
        self.assertAlmostEqual(quat_rot[1], 0.3535534, 5)
        self.assertAlmostEqual(quat_rot[2], 0.3535534, 5)
        self.assertAlmostEqual(quat_rot[3], 0.8535534, 5)

        check_rot = Euler.from_quaternion(quat_rot)
        print(euler_rot, check_rot)
        self.assertTrue(np.allclose(euler_rot, check_rot))

    def test_matrices1(self):
        engine = RenderPipelineEngine()
        vals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 22, 33, 44, 55, 66]
        m1 = Mat4(*vals)
        m2 = engine.eis_matrix(m1)
        self.assertEqual([*m2.flat], vals)
        m3 = engine.engine_matrix(m2)
        self.assertEqual(m1, m3)

    def test_matrices2(self):
        engine = RenderPipelineEngine()
        vals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 22, 33, 44, 55, 66]
        m1 = Matrix44(vals)
        m2 = engine.engine_matrix(m1)
        self.assertEqual([col for row in m2 for col in row], vals)
        m3 = engine.eis_matrix(m2)
        self.assertEqual([col for row in m1 for col in row], [col for row in m3 for col in row])

    def test_quaternions1(self):
        engine = RenderPipelineEngine()
        vals = [1, 2, 3, 4]

        q1 = Quat(*vals)
        self.assertEqual(q1.get_w(), vals[3])
        self.assertEqual(q1.get_x(), vals[0])
        self.assertEqual(q1.get_y(), vals[1])
        self.assertEqual(q1.get_z(), vals[2])

        self.assertEqual([*q1], vals)

        q2 = engine.eis_quaternion(q1)
        self.assertEqual(q2.w, vals[3])
        self.assertEqual(q2.x, vals[0])
        self.assertEqual(q2.y, vals[1])
        self.assertEqual(q2.z, vals[2])

        self.assertEqual([*q2], vals)

        q3 = engine.engine_quaternion(q2)
        self.assertEqual(q3.get_w(), vals[3])
        self.assertEqual(q3.get_x(), vals[0])
        self.assertEqual(q3.get_y(), vals[1])
        self.assertEqual(q3.get_z(), vals[2])

        self.assertEqual(q1, q3)

    def test_quaternions2(self):
        engine = RenderPipelineEngine()
        vals = [1, 2, 3, 4]

        q1 = Quaternion(vals)
        self.assertEqual(q1.x, vals[0])
        self.assertEqual(q1.y, vals[1])
        self.assertEqual(q1.z, vals[2])
        self.assertEqual(q1.w, vals[3])

        self.assertEqual([*q1], vals)

        q2 = engine.engine_quaternion(q1)
        self.assertEqual(q2.get_x(), vals[0])
        self.assertEqual(q2.get_y(), vals[1])
        self.assertEqual(q2.get_z(), vals[2])
        self.assertEqual(q2.get_w(), vals[3])

        self.assertEqual([*q2], vals)

        q3 = engine.eis_quaternion(q2)
        self.assertEqual(q3.x, vals[0])
        self.assertEqual(q3.y, vals[1])
        self.assertEqual(q3.z, vals[2])
        self.assertEqual(q3.w, vals[3])

        self.assertEqual([*q1], [*q3])

    def test_vector1(self):
        engine = RenderPipelineEngine()
        vals = [1, 2, 3]
        v1 = Vec3(*vals)
        v2 = engine.eis_vector(v1)
        self.assertEqual([*v2], vals)
        v3 = engine.engine_vector(v2)
        self.assertEqual(v1, v3)


@unittest.skip
class TestRenderPipelineConverter(TestCase):
    def setUp(self):
        self.engine = RenderPipelineEngine()
        self.engine.initialize()

    def tearDown(self):
        self.engine.shutdown()
        del self.engine

    # NOTE: Skipping only because its long to initialize the engine
    def test_conversion_pass(self):
        converter = RenderPipelineConverter()
        path = os.path.realpath(os.path.join(os.path.dirname(__file__),
                                             "../../../../../res/scenes/Sponza/export/Sponza.bam"))
        model = self.engine.read_model_from_file(path=path)
        self.assertIsNotNone(model)
        self.assertIsInstance(model, Model)
        node_path = converter.eis_to_engine(model)
        self.assertIsNotNone(node_path)
        self.assertIsInstance(node_path, NodePath)

    # FIXME: We have to rethink equality for entities
    # @unittest.expectedFailure
    def test_conversion_validity(self):
        converter = RenderPipelineConverter()
        path = os.path.realpath(os.path.join(os.path.dirname(__file__),
                                             "../../../../../res/scenes/Sponza/export/Sponza.bam"))
        model = self.engine.read_model_from_file(path=path)
        node_path = converter.eis_to_engine(model)
        model_converted_back = converter.engine_to_eis(node_path)

        self.assertEqual(model, model_converted_back)
