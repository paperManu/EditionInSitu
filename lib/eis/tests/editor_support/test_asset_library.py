from unittest import TestCase

from eis.editor_support.asset_library import AssetLibrary
from ..mock.editor import MockEditor
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


class EditorAssetLibrary(MockEditor):
    def __init__(self):
        super().__init__()
        self._rotation = Quaternion()

    @property
    def rotation(self) -> Quaternion:
        return self._rotation.copy()


class TestAssetLibrary(TestCase):

    def setUp(self):
        self.library = AssetLibrary(editor=EditorAssetLibrary())

    def test_location(self):
        # we are getting a copy when the getter is called
        self.assertTrue(self.library.location is not self.library._location)
        a_location = Vector3([1.0, 2.0, 3.0])
        self.library.location = a_location
        self.assertTrue(self.library._location is not a_location)
        self.assertEqual(self.library.location, a_location)
