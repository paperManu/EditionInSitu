import math

from unittest import TestCase

from eis.engines.dummy.picker import DummyPicker
from eis.inputs.htc_vive import PeerHTCVive
from eis.inputs.support.vrpn import VRPN_AVAILABLE
from ..mock.editor import DummyEngine, MockEditor
from satmath.euler import Euler
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3


if VRPN_AVAILABLE:
    from eis.inputs.htc_vive import HTCVive, HTCViveControllerVRPN

    class TestHTCViveControllerVRPN(TestCase):
        def setUp(self):
            self.controller = HTCViveControllerVRPN(host="a_host", device="a_device", config={})

        def test_tracking_space_offset(self):
            self.assertTrue(self.controller.tracking_space_offset is not self.controller._tracking_space_offset)
            self.assertEqual(type(self.controller.tracking_space_offset), Matrix44)

            a_matrix = Matrix44.from_x_rotation(math.pi / 2.0)
            self.controller.tracking_space_offset = a_matrix
            self.assertEqual(self.controller.tracking_space_offset, a_matrix)

        def test_tracker_rotation(self):
            self.assertTrue(self.controller.tracker_rotation is not self.controller._tracker_rotation)
            self.assertEqual(type(self.controller.tracker_rotation), Euler)

            a_rotation = Euler.from_x_rotation(math.pi / 2.0)
            self.controller.tracker_rotation = a_rotation
            self.assertEqual(self.controller.tracker_rotation, a_rotation)

        def test_location_copy(self):
            self.assertTrue(self.controller.location is not self.controller._location)
            self.assertEqual(type(self.controller.location), Vector3)

        def test_rotation_copy(self):
            self.assertTrue(self.controller.rotation is not self.controller._rotation)
            self.assertEqual(type(self.controller.rotation), Quaternion)

        def test_matrix_copy(self):
            self.assertTrue(self.controller.matrix is not self.controller._matrix)
            self.assertEqual(type(self.controller.matrix), Matrix44)


    class TestHTCVive(TestCase):
        def setUp(self):
            picker = DummyPicker(engine=DummyEngine(), editor=MockEditor())
            primary = HTCViveControllerVRPN(host="a_host", device="a_device", config={})
            secondary = HTCViveControllerVRPN(host="a_host", device="a_device", config={})
            self.controllers = HTCVive(primary=primary, secondary=secondary, config={}, mapping_config={}, picker=picker)

        def test_primary_matrix_copy(self):
            self.assertTrue(self.controllers.primary_matrix is not self.controllers._primary_matrix)
            self.assertEqual(type(self.controllers.primary_matrix), Matrix44)

        def test_secondary_matrix_copy(self):
            self.assertTrue(self.controllers.secondary_matrix is not self.controllers._secondary_matrix)
            self.assertEqual(type(self.controllers.secondary_matrix), Matrix44)


class TestPeerHTCVive(TestCase):
    def setUp(self):
        self.peer_htc_vive = PeerHTCVive()

    def test_primary_matrix_copy(self):
        self.assertTrue(self.peer_htc_vive.primary_matrix is not self.peer_htc_vive._primary_matrix)
        self.assertEqual(type(self.peer_htc_vive.primary_matrix), Matrix44)

    def test_secondary_matrix_copy(self):
        self.assertTrue(self.peer_htc_vive.secondary_matrix is not self.peer_htc_vive._secondary_matrix)
        self.assertEqual(type(self.peer_htc_vive.secondary_matrix), Matrix44)

