"""Unit Tests for gltf converter gltf.py"""

import glob
import json
import os
import shutil

from freezegun import freeze_time
from math import pi
from unittest import TestCase

from eis.converters.gltf import GLTFConverter
from eis.converters.gltf import GLTFExporter, GLTFImporter
from eis.graph.model import Model
from eis.graph.scene import Scene
from io_scene_gltf2.io.com.gltf2_io import gltf_from_dict
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

EXPORT_DIR = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../export"
))

RES_DIR = os.path.realpath(os.path.join(
    os.path.dirname(__file__),
    "../res"
))

# Helper functions for removing test files from res and export directories.
def clean_export():
    exports = glob.glob(os.path.join(EXPORT_DIR, '*.gltf'))
    for export in exports:
        shutil.rmtree(export)

def clean_res():
    files = glob.glob(os.path.join(RES_DIR, '*'))
    for f in files:
        os.remove(f)


# Unit Tests for Class GLTFExporter
class TestExport(TestCase):
    """Unit Tests for GLTFExporter.export method."""
    def setUp(self):
        clean_export()

    def tearDown(self):
        # Clear gltf test exports.
        clean_export()

    def test_sets_default_file_name(self):
        """Assert if scene.name is empty, default gltf file name is used."""
        export_path = os.path.join(EXPORT_DIR, 'scene_with_default_name.gltf') 
        scene = Scene()
        # Set scene name as empty to trigger default gltf file name
        scene._name = ""

        gltf_exporter = GLTFExporter(scene=scene, filename=export_path)
        # The default gltf export file name is based on the current time,
        # so we have to mock python datetime.
        with freeze_time('2020-01-01'):
            gltf_exporter.export()

        # Assert the correct gltf file has been created.
        file_name = os.path.join(
            export_path,
            'exported_gltf_scene_1_1_2020.gltf'
        )
        self.assertTrue(os.path.isfile(file_name))

    def test_sets_scene_name_as_file_name(self):
        """Assert if scene.name is not empty, it is set as the gltf file name.
        """
        export_path = os.path.join(EXPORT_DIR, 'scene_with_name.gltf')
        scene = Scene()

        self.assertNotEqual(scene.name, "")

        gltf_exporter = GLTFExporter(scene=scene, filename=export_path)
        gltf_exporter.export()

        scene_name = '.'.join((scene.name.replace(' ', '_'), 'gltf'))
        file_name = os.path.join(
            export_path,
            scene_name
        )

        self.assertTrue(os.path.isfile(file_name))

    def test_sets_scene_name_as_gltf_attribute(self):
        """Assert if scene.name is not empty, it is set correctly as a gltf
        scene.name attribtue.
        """
        export_path = os.path.join(EXPORT_DIR, 'scene_with_name.gltf')
        scene = Scene()

        self.assertNotEqual(scene.name, "")

        gltf_exporter = GLTFExporter(scene=scene, filename=export_path)
        gltf_exporter.export()

        scene_name = scene.name.replace(' ', '_')
        file_name = os.path.join(
            export_path,
            '.'.join((scene_name, 'gltf'))
        )

        with open(file_name) as fp:
            self.assertTrue(fp)
            gltf_data = json.load(fp)

        scene_json = gltf_data['scenes'][0]
        self.assertTrue(scene_json['name'], scene_name)


# Tests for GLTFImporter
class TestParseJson(TestCase):
    def setUp(self):
        self.gltf_importer = GLTFImporter(path="")

    def test_throws_warning_if_no_scenes(self):
        """Assert if the given gltf contains no scenes, a warning is thrown and
        None is returned.
        """
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": None
        }
        gltf_data = gltf_from_dict(content)

        with self.assertLogs(logger='eis.converters.gltf', level='WARNING') as logging_watcher:
            scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(scene_name, None)
        self.assertIn(
            'WARNING:eis.converters.gltf:Trying to import a glTF file with no scene.',
            logging_watcher.output
        )

    def test_returns_scene_name(self):
        scene_name = "TestScene"
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": scene_name}
            ],
            "scene": 0
        }
        
        gltf_data = gltf_from_dict(content)

        parsed_scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(parsed_scene_name, scene_name)

    def test_scene_index_defaults_to_zero(self):
        """Assert if the 'scene' property is not set, the importer defaults to
        the first scene (scene 0).
        """
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene 0"},
                {"name": "Scene 1"}
            ]
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(scene_name, "Scene 0")

    def test_scene_name_defaults_to_empty_string(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": None}
            ],
            "scene": 0
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(scene_name, "")

    def test_root_node_index_set(self):
        """Assert the index for the root node is set correctly."""
        root_index = 1
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {
                    "name": "Scene",
                    "nodes": [
                        root_index
                    ]
                }
            ],
            "scene": 0,
            "nodes": [
                {"name": "DummyNode"},
                {"name": "RootNode"}
            ]
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._root_index, root_index)

    def test_nodes_defaults_to_empty_list(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene"},
            ],
            "nodes": None
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._nodes, [])

    def test_images_defaults_to_empty_list(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene"},
            ],
            "images": None
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._images, [])

    def test_textures_defaults_to_empty_list(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene"},
            ],
            "textures": None
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._textures, [])

    def test_materials_defaults_to_empty_list(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene"},
            ],
            "materials": None
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._materials, [])

    def test_meshes_defaults_to_empty_list(self):
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"name": "Scene"},
            ],
            "meshes": None
        }

        gltf_data = gltf_from_dict(content)

        scene_name = self.gltf_importer._parse_json(gltf_data)

        self.assertEqual(self.gltf_importer._meshes, [])


# Unit Tests for class GLTFImporter
class TestImportGltf(TestCase):
    """Unit Tests for method GLTFImporter.import_gltf"""
    def setUp(self):
        clean_res()
        self.gltf_importer = GLTFImporter(path="")

    def test_root_added(self):
        """Assert that if the scene's root node has a transformation, a new
        root node is added with no transformation.
        """
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"nodes": [0]}
            ],
            "scene": 0,
            "nodes": [
                {
                    "name": "old root",
                    "matrix": [
                        1, 1, 1, 1,
                        1, 1, 1, 1,
                        1, 1, 1, 1,
                        1, 1, 1, 1
                    ]
                }
            ]
        }

        path = os.path.join(RES_DIR, 'scene_without_root.gltf')
        with open(path, 'w') as fp:
            json.dump(content, fp)

        self.gltf_importer._path = path

        scene = self.gltf_importer.import_gltf()

        root = scene.model.root

        # Assert a new root node has been created with the identity matrix.
        self.assertIsNotNone(root)
        self.assertEqual(root.matrix, Matrix44.identity())

        # Assert the original node is a child of the new root.
        self.assertEqual(len(root.children), 1)
        node = root.children[0]
        self.assertEqual(node.name, 'old root')

    def test_root_not_added(self):
        """Assert if the scene already has a root with no transformation, then
        a new one is not added.
        """
        # Transform identity matrix to y-up gltf coordinate system
        root_matrix = Matrix44.from_axis_rotation(
            Vector3((1.0, 0.0, 0.0)),
            pi / 2.0
        )
        content = {
            "asset": {
                "version": "2.0"
            },
            "scenes": [
                {"nodes": [0]}
            ],
            "scene": 0,
            "nodes": [
                {
                    "name": "root node",
                    "matrix": list(root_matrix.flatten())
                }
            ]
        }

        path = os.path.join(RES_DIR, 'scene_with_root.gltf')
        with open(path, 'w') as fp:
            json.dump(content, fp)

        self.gltf_importer._path = path

        scene = self.gltf_importer.import_gltf()

        root = scene.model.root

        self.assertIsNotNone(root)
        self.assertEqual(root.matrix, Matrix44.identity())
        self.assertEqual(root.name, 'root node')

        # Assert no new nodes have been added
        self.assertEqual(len(root.children), 0)

    def test_bad_file_path_throws_warning(self):
        """Assert if a non-existent gltf file path is provided, a warning is
        thrown and None is returned.
        """
        path = "./fake_file_path_123456789.gltf"

        self.gltf_importer._path = path


        with self.assertLogs(logger='eis.converters.gltf', level='WARNING') as logging_watcher:
            scene = self.gltf_importer.import_gltf()

        self.assertEqual(scene, None)
        self.assertIn(
            'WARNING:eis.converters.gltf:Provided gltf path does not exist.',
            logging_watcher.output
        )

    def test_sets_scene_name(self):
        """Assert the EiS Scene name is set to be the same as the gltf model.
        """
        path = os.path.join(RES_DIR, 'scene_with_name.gltf')
        scene_name = 'Scene With Name'

        self.gltf_importer._path = path

        scene = self.gltf_importer.import_gltf()

        self.assertEqual(scene.name, scene_name)
