import importlib
import inspect
import logging
from pathlib import Path
from typing import Dict, Optional
from eis.display.components.menus.menu import Menu

logger = logging.getLogger(__name__)


class MenuGeometry():
    """Handles loading different types of menu geometries """

    def __init__(self, config) -> None:
        self._menu_geoms: Optional[Dict] = {}
        self._current_geom: Optional[Menu] = None
        self._editor_config: Dict = config
        self._collect_menu_geoms()
        self._current_geom = self._menu_geoms[self._get_menu_geometry_from_config()]

    def _get_menu_geometry_from_config(self) -> str:
        """
        Looks for a configuration of menu geometry. If it doesn't find one, it returns a default RadialMenu
        :return: String
        """
        return self._editor_config.get('menu.geometry') or 'RadialMenu'

    def _collect_menu_geoms(self) -> None:
        geoms: Dict = {}
        menu_geoms_path = Path(__file__).parent.joinpath('menus', 'menu_geoms')
        pkg = ".".join([__package__, 'menus', 'menu_geoms'])
        for m in menu_geoms_path.iterdir():
            if m.is_file():
                module_path = ".".join([pkg, m.with_suffix('').name])
                module_name = importlib.import_module(module_path)
                for name, obj in inspect.getmembers(module_name, inspect.isclass):
                    obj_name = obj.__name__
                    obj_module = inspect.getmodule(obj).__name__
                    if obj_module == module_name.__name__:
                        geoms[obj_name] = obj
                self._menu_geoms = geoms
            else:
                logger.debug("{m} is not a file so ignoring as possible menu geometry class")

    @property
    def menu_geometries(self) -> Dict:
        """
        Available Menu geometries
        :return: Dict of Menu classes
        """
        return self._menu_geoms

    @property
    def current_menu_geometry(self) -> Menu:
        """
        Currently chosen menu geometry
        :return: Menu
        """
        return self._current_geom
