import logging
from typing import TypeVar

from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu
from eis.display.components.menus.editor_timeline_menu import editor_timeline_menu
from eis.graph.object_3d import Object3D

logger = logging.getLogger(__name__)

EditingPresentationMenu = TypeVar('EditingPresentationMenu')
PresentationMenu = TypeVar('PresentationMenu')


def editing_presentation_menu(cls: Menu) -> EditingPresentationMenu:

    class EditingPresentationMenu(editor_timeline_menu(cls)):
        def __init__(self):
            super().__init__(name="Presentation Menu")
            self._first_slide_button = self.add_item("First slide", self._first_slide)
            self._previous_slide_button = self.add_item("Previous slide", self._previous_slide)
            self._next_slide_button = self.add_item("Next slide", self._next_slide)
            self._last_slide_button = self.add_item("Last slide", self._last_slide)
            self._set_slide_button = self.add_item("Set slide", self._set_slide)
            self._insert_slide_button = self.add_item("Insert slide", self._insert_slide)
            self._remove_slide_button = self.add_item("Remove slide", self._remove_slide)
            self.add_item("Back", self._back)

        def _back(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.edit_scene()
            self.close()

        def _first_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.first_slide()

        def _previous_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.previous_slide()

        def _next_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.next_slide()

        def _last_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.last_slide()

        def _insert_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.insert_slide()

        def _set_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.set_slide()

        def _remove_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.remove_slide()

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            timeline = self.editor.timeline
            time_on_slide = (timeline.time % self.editor.presentation_manager._duration_between_slides) == 0.0
            self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
            self._next_slide_button.enabled = not timeline.running
            self._set_slide_button.enabled = time_on_slide
            self._insert_slide_button.enabled = time_on_slide
            self._remove_slide_button.enabled = time_on_slide

        def update_menu_components(self) -> None:
            super().update_menu_components()
            timeline = self.editor.timeline
            time_on_slide = (timeline.time % self.editor.presentation_manager._duration_between_slides) == 0.0
            self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
            self._next_slide_button.enabled = not timeline.running
            self._set_slide_button.enabled = time_on_slide
            self._insert_slide_button.enabled = time_on_slide and self._selected_keyframe is not None
            self._remove_slide_button.enabled = time_on_slide and self._selected_keyframe is not None
            self._first_slide_button.enabled = not timeline.running
            self._last_slide_button.enabled = not timeline.running

    return EditingPresentationMenu


def presentation_menu(cls: Menu) -> PresentationMenu:

    class PresentationMenu(editor_timeline_menu(cls)):
        def __init__(self):
            super().__init__(name="Presentation Menu")

            self._first_slide_button = self.add_item("First slide", self._first_slide)
            self._previous_slide_button = self.add_item("Previous slide", self._previous_slide)
            self._next_slide_button = self.add_item("Next slide", self._next_slide)
            self._last_slide_button = self.add_item("Last slide", self._last_slide)
            self.add_item("Hide picker", self._deactivate_picker_input)
            self.add_item("Back", self._back)

        def _back(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.machine.welcome()
            self.close()

        def _first_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.first_slide()

        def _previous_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.previous_slide()

        def _next_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.next_slide()

        def _last_slide(self, target: Object3D, input: LocalInputMethod) -> None:
            self.editor.presentation_manager.last_slide()

        def _deactivate_picker_input(self, target: Object3D, input: LocalInputMethod) -> None:
            input.picker_active = False
            self.close()

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            input.picker_active = True
            timeline = self.editor.timeline
            self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
            self._next_slide_button.enabled = not timeline.running

        def update_menu_components(self) -> None:
            super().update_menu_components()
            timeline = self.editor.timeline
            self._previous_slide_button.enabled = timeline.time != 0.0 and not timeline.running
            self._next_slide_button.enabled = not timeline.running
            self._first_slide_button.enabled = not timeline.running
            self._last_slide_button.enabled = not timeline.running
