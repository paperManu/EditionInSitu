from enum import Enum
import logging
import math
from typing import Any, List, Optional

from eis.client.input import LocalInputMethod
from eis.display.components.buttons.button import Button
from eis.display.components.cursor import Cursor
from eis.display.components.menus.menu import Menu
from eis.graph.camera import Camera
from eis.graph.primitives.shapes.arrow import Arrow
from eis.graph.object_3d import Object3D
from eis.graph.texture_video import TextureVideo
from satmath.euler import Euler

logger = logging.getLogger(__name__)


def editing_object_menu(cls: Menu):

    class EditingObjectMenu(cls):
        def __init__(self):
            super().__init__(name="EditingObject Menu")
            self.currently_active_object: Optional[Object3D] = None

            def _assets(target: Object3D, input: LocalInputMethod) -> None:
                assert(self.editor)
                self.editor.machine.select_assets(
                    input=input,
                    zrot=Euler.from_matrix(self.matrix_world).z - Euler.from_matrix(self.editor.matrix).z,
                    select_material=True,
                    target_object=self.currently_active_object
                )

            self._asset_button = self.add_item("Asset", _assets)

            def _play_videos(target: Object3D, input: LocalInputMethod) -> None:
                video_textures = self._get_video_textures()
                for video_texture in video_textures:
                    video_texture.video_playback = True

            self._video_play_button = self.add_item("Play videos", _play_videos)

            def _pause_videos(target: Object3D, input: LocalInputMethod) -> None:
                video_textures = self._get_video_textures()
                for video_texture in video_textures:
                    video_texture.video_playback = False

            self._video_pause_button = self.add_item("Pause videos", _pause_videos)

            def _back(target: Object3D, input: LocalInputMethod) -> None:
                assert(self.editor)
                self.editor.machine.edit_scene()
                self.close()

            self.add_item("Back", _back)

        def _get_video_textures(self) -> List[TextureVideo]:
            assert(self._state)
            assert(self._state._root_object is not None)
            active_object = self._state._root_object
            if active_object is None or active_object.material is None or not active_object.material.material_textures:
                return []

            video_textures = []
            for material_texture in active_object.material.material_textures:
                if isinstance(material_texture.texture, TextureVideo):
                    video_textures.append(material_texture.texture)

            return video_textures

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opening(input)
            self.currently_active_object = self.editor.last_active_object
            self._asset_button.enabled = self.editor.last_active_object is not None

            self._video_play_button.enabled = self.editor.last_active_object is None
            self._video_pause_button.enabled = self.editor.last_active_object is None

        def on_closed(self) -> None:
            super().on_closed()
            self.currently_active_object = None

    return EditingObjectMenu


class Transformations(Enum):
    TRANSLATING = 0
    ROTATING = 1
    SCALING = 2


def change_transformation_menu(cls: Menu):

    class ChangeTransformationMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Change Transformation Menu")

            def _translate(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.state_object.enabled_transformation = Transformations.TRANSLATING
                self.close()

            self._translate_button = self.add_item("Translate", _translate)

            def _rotate(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.state_object.enabled_transformation = Transformations.ROTATING
                self.close()

            self._rotate_button = self.add_item("Rotate", _rotate)

            def _scale(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.state_object.enabled_transformation = Transformations.SCALING
                self.close()

            self._scale_button = self.add_item("Scale", _scale)

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            self.editor.machine.state_object.transform_lock = True
            self._translate_button.enabled = Transformations(
                self.editor.machine.state_object.enabled_transformation) != Transformations.TRANSLATING
            self._rotate_button.enabled = Transformations(
                self.editor.machine.state_object.enabled_transformation) != Transformations.ROTATING
            self._scale_button.enabled = Transformations(
                self.editor.machine.state_object.enabled_transformation) != Transformations.SCALING

        def on_closed(self) -> None:
            super().on_closed()
            self.editor.machine.state_object.transform_lock = False

    return ChangeTransformationMenu


def transforming_menu(cls: Menu):

    class TransformingMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Transforming Menu")

            # States Menu
            def _change_transformation(target: Object3D, input: LocalInputMethod) -> None:
                self._manager.open_menu(input, change_transformation_menu(cls)())

            self.add_item("Change transformation", _change_transformation)

            def _exit(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.edit_scene()
                self.close()

            self.add_item("Exit", _exit)

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            self.editor.machine.state_object.transform_lock = True

        def on_closed(self) -> None:
            super().on_closed()
            self.editor.machine.state_object.transform_lock = False

    return TransformingMenu


def camera_menu(cls: Menu):

    class CameraMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Camera Menu")
            self._attach_camera_button = self.add_item("Attach camera", self._attach_camera)
            self._detach_camera_button = self.add_item("Detach camera", self._detach_camera)

        def _attach_camera(self, target: Button, input: LocalInputMethod) -> None:
            self.editor.camera = self.editor.active_object
            self.close()

        def _detach_camera(self, target: Button, input: LocalInputMethod) -> None:
            self.editor.camera = None
            self.close()

        def on_opened(self, input: LocalInputMethod) -> None:
            super().on_opened(input)
            self.editor.machine.state_object.selection_lock = True
            self._attach_camera_button.enabled = self.editor.active_object is not None and self.editor.camera is None and type(
                self.editor.active_object) is Camera
            self._detach_camera_button.enabled = self.editor.camera is not None

        def on_closed(self) -> None:
            super().on_closed()
            self.editor.machine.state_object.selection_lock = False
    return CameraMenu


def fly_menu(cls: Menu):

    class FlyMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Fly Menu")

            def _back(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.state_object._on_exit()

            self.add_item("Return", _back)
    return FlyMenu


def walk_menu(cls: Menu):

    class WalkMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Walk Menu")

            def _back(target: Object3D, input: LocalInputMethod) -> None:
                self.editor.machine.state_object._on_exit()

            self.add_item("Return", _back)
    return WalkMenu


class NavigatingCursor(Cursor):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        diameter = self.radius * 2
        self.custom_cursor = Arrow(
            arrow_length=diameter,
            arrow_width=diameter,
            head_length_ratio=2.0 / 3.0,
            width=2
        )
        self.custom_cursor.x_rotation = math.pi / 4.0
