from eis.client.input import LocalInputMethod
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu import Menu
from eis.graph.light import Light
from eis.graph.object_3d import Object3D
from eis.graph.sound_object import SoundObject
from eis.graph.sound_objects.video_sound_object import VideoSoundObject


def configuration_menu(cls: Menu):

    class ConfigurationMenu(cls):
        def __init__(self) -> None:
            super().__init__(name="Configuration Menu")

            def _calibrate(target: Button, input: LocalInputMethod) -> None:
                assert(self.editor is not None)
                self.editor.machine.calibrate()
                self.close()

            self.add_item("Calibrate", _calibrate)

            def _mouse_picker(target: Button, input: LocalInputMethod) -> None:
                for input_method in input.user.input_methods.values():
                    if str(type(input_method)) == "eis.engines.render_pipeline.input.RenderPipelineMovementController":
                        input_method.picker_active = not input_method.picker_active
                self.close()

            self.add_item("Toggle mouse picker", _mouse_picker)

            def _deactivate_picker_input(target: Button, input: LocalInputMethod) -> None:
                input.picker_active = False
                self.close()

            self.add_item("Hide picker", _deactivate_picker_input)

            def _make_sound_light_invisible(target: Button, input: LocalInputMethod) -> None:
                assert(self.editor is not None)
                for child in self.editor.scene.model.root.children:
                    if child.name == "Added_objects_layer":
                        self._sound_layer = child
                        break

                visible = None
                for item in self._sound_layer.children:
                    is_sound = False
                    for child in item.children:
                        if isinstance(child, SoundObject) and not isinstance(child, VideoSoundObject):
                            is_sound = True
                    if is_sound:
                        if item in self.editor.passive_objects:
                            self.editor.remove_passive_object(item)
                        if visible is None:
                            visible = not item.visible
                        item.visible = visible
                        item.pickable = visible

                def hide_child_lights(node: Object3D) -> None:
                    for child in node.children:
                        if isinstance(child, Light):
                            child.visible = not child.visible
                        hide_child_lights(child)

                hide_child_lights(self.editor.scene.model.root)
                self.close()

            self.add_item("Sound/Light visibility", _make_sound_light_invisible)

    return ConfigurationMenu
