import os
from typing import List

from eis.commands.engine import LoadSceneCommand
from eis.client.input import LocalInputMethod
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu import Menu


def import_gltf_menu(cls: Menu):

    class ImportGLTFMenu(cls):
        def __init__(self, scenes: List[str]) -> None:
            super().__init__(name="Load glTF scene")
            for s in scenes:
                self.add_item(os.path.basename(os.path.dirname(s)), self._load_scene, data=s)

        def _load_scene(self, target: Button, input: LocalInputMethod) -> None:
            scene = self.editor.load_scene_from_file(path=target.data)
            if scene:
                self.editor.engine.show_status("LOADING SCENE")
                self.editor.machine.client.session.command(LoadSceneCommand(scene))
            self.close()

    return ImportGLTFMenu
