from typing import List

from eis.client.input import LocalInputMethod
from eis.commands.engine import LoadSceneFileCommand
from eis.display.components.buttons.button import Button
from eis.display.components.menus.menu import Menu


def load_scene_menu(cls: Menu):

    class LoadSceneMenu(cls):
        def __init__(self, scenes: List[str]) -> None:
            super().__init__(name="Load EiS scene")
            for s in scenes:
                self.add_item(s, self._load_scene, data=s)

        def _load_scene(self, target: Button, input: LocalInputMethod) -> None:
            assert(self.editor is not None)
            self.editor.machine.client.session.command(LoadSceneFileCommand(target.data))
            self.close()

    return LoadSceneMenu
