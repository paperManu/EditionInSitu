import logging

from abc import ABCMeta, abstractmethod
from collections import deque
from enum import Enum, unique
from threading import Thread, Event
from typing import Any, Callable, ClassVar, Deque, Dict, Generator, Generic, Optional, TYPE_CHECKING, Tuple, Type, TypeVar

from eis.cache_manager import CacheManager
from eis.client.input import LocalInputMethod
from eis.graph.mesh import Mesh
from eis.graph.model import Model, ModelDelegate
from eis.graph.object_3d import Object3D
from eis.graph.scene import Scene
from eis.picker import Picker
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore

if TYPE_CHECKING:
    from eis.graph.base import GraphBase, GraphProxyBase
    from eis.client.editor import ClientEditor

logger = logging.getLogger(__name__)

defaultEngineConfig: Dict[str, Any] = {
    'renderer.projection': 'perspective',
    'renderer.fov': 90,
    'renderer.culling': 'none',
    'renderer.near': 0.1,
    'renderer.far': 1000.0,
    'cache.timeout': 0
}

O = TypeVar('O')
M = TypeVar('M')
Q = TypeVar('Q')
V = TypeVar('V')

ModelReadCallback = Callable[[Optional[Model]], None]
ModelReadResult = Tuple[int, Optional[Model]]
SceneLoadedCallback = Callable[[Optional[Scene]], None]
ModelLoadedCallback = Callable[[Optional[Model]], None]
ModelLoadResult = Tuple[int, bool, Model, Optional[Object3D]]


class EngineDelegate(ModelDelegate, metaclass=ABCMeta):
    """
    Engine Delegate Base Class
    """


def register_engine(name: str) -> Callable:
    """
    Decorator for registering an engine
    """

    def deco(cls: Type['Engine']) -> Callable:
        Engine.register(cls, name)
        return cls

    return deco


def proxy(engine: Type['Engine'], proxied_type: Type['GraphBase']) -> Callable:
    """
    Decorator used to register graph proxies

    :param engine: Type[Engine] - Engine type for which to register the proxy
    :param proxied_type: Type[GraphBase] - Type being proxied
    :return:
    """

    def deco(cls: Type['GraphProxyBase[Any]']) -> Callable:
        Engine.register_proxy(engine, proxied_type, cls)
        return cls

    return deco


class Engine(Generic[O, M, Q, V], metaclass=ABCMeta):
    """
    Engine

    Generic type vars are the following:
    M: Type used for 4x4 matrices
    Q: Type used for quaternions
    V: Type used for 3D vectors
    """

    _engines: Dict[str, Type['Engine']] = dict()
    _proxies: ClassVar[Dict[Type['Engine'], Dict[Type['GraphBase'], Type['GraphProxyBase[Any]']]]] = dict()

    @unique
    class ProjectionType(str, Enum):
        PERSPECTIVE = "perspective"
        SPHERICAL = "spherical"

    @classmethod
    def register(cls, engine_type: Type['Engine'], engine_type_name: str) -> None:
        """
        Register an Engine
        :param engine_type: Engine class to register
        """
        if engine_type_name not in cls._engines:
            cls._engines[engine_type_name] = engine_type
        else:
            raise Exception(f"An engine typed {engine_type_name} has already been registered")

    @classmethod
    def instantiate(cls, engine_type_name: str, config: Optional[Dict[str, Any]] = None) -> Optional['Engine']:
        """
        Instantiate an engine based on its type name
        :param engine_type_name: Type name of the desired engine
        :param config: Configuration for the instantiated engine
        :return: Return an engine based on its type name, if available
        """
        if engine_type_name in cls._engines:
            return cls._engines[engine_type_name](config=config)
        return None

    @classmethod
    def register_proxy(cls, engine: Type['Engine'], proxied_type: Type['GraphBase'], proxy: Type['GraphProxyBase[Any]']) -> None:
        """
        Register a proxy class

        :param engine: Type[Engine] - Engine type for which to register the proxy
        :param proxied_type: Type[GraphBase] - Type being proxied
        :param proxy: proxy: Type['GraphProxyBase[Any]']
        :return: None
        """
        if engine not in cls._proxies:
            engine_proxies: Dict[Type['GraphBase'], Type['GraphProxyBase[Any]']] = dict()
            cls._proxies[engine] = engine_proxies
        else:
            engine_proxies = cls._proxies[engine]
        if proxied_type in engine_proxies:
            raise Exception(
                "Proxy \"{}\" for type \"{}\" in engine \"{}\" can't be registered; already registered as \"{}\"".format(
                    proxy, proxied_type, engine, engine_proxies[proxied_type]
                )
            )

        # logger.debug("Registering proxy \"{}\" for type \"{}\" in engine \"{}\"".format(proxy, proxied_type, engine))
        engine_proxies[proxied_type] = proxy

    def get_proxy(self, proxied: 'GraphBase') -> Optional['GraphProxyBase[Any]']:
        """
        Get a proxy instance for the passes proxy_type.
        It is assumed that this is called on a type or instance of the
        engine type requested because we use `cls` to lookup the proxies.

        :param proxied_type: Type[GraphBase] - Type to get a proxy for
        :return: Optional[GraphProxyBase[Any]]
        """
        engine_proxies = Engine._proxies.get(type(self))
        if not engine_proxies:
            logger.warning("No proxies for engine {}".format(type(self)))
            return None

        proxy_type: Optional[Type[GraphProxyBase[Any]]] = None
        for proxied_type in proxied.__class__.mro():
            proxy_type = engine_proxies.get(proxied_type)
            if proxy_type:
                break

        if not proxy_type:
            logger.warning("No proxy for object {} of type \"{}\"".format(proxied, type(proxied)))
            return None

        return proxy_type(engine=self, proxied=proxied)

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        """
        Create an engine

        :param config: Optional[Dict[str, Any]] - Configuration
        """
        assert config is None or isinstance(config, dict)

        self._config = defaultEngineConfig
        if config is not None:
            self._config.update(config)
        self._delegate: Optional[EngineDelegate] = None

        # Config
        self._projection_type: Engine.ProjectionType = self._config.get(
            'renderer.projection', Engine.ProjectionType.PERSPECTIVE)
        self._spherical_inverted: bool = self._config.get(
            'renderer.spherical_inverted', False)
        self._default_culling: Mesh.Culling = self._config.get(
            'renderer.culling', Mesh.Culling.NONE)
        self._fov: float = self._config.get('renderer.fov', 90)
        CacheManager.set_lifetime(self._config.get('cache.timeout') * 1000.0)  # We keep it in ms

        # Internals

        # Conversion threads
        self._read_event = Event()  # type: ignore
        self._read_tasks: Deque[Tuple[int, str]] = deque()
        self._pending_read_counter = 0
        self._pending_read_callbacks: Dict[int, ModelReadCallback] = {}
        self._read_models_results_deque: Deque[ModelReadResult] = deque()
        self._read_thread = Thread(target=self._read_runner, args=(
            self._read_event, self._read_tasks, self._read_models_results_deque), daemon=True, name="Model Read Runner")
        self._read_thread.start()

        self._convert_event = Event()  # type: ignore
        self._convert_tasks: Deque[Tuple[int, Model, Optional[Object3D[Any]]]] = deque()
        self._pending_convert_counter = 0
        self._pending_convert_callbacks: Dict[int, ModelLoadedCallback] = {}
        self._converted_models_results_deque: Deque[ModelLoadResult] = deque()
        self._convert_thread = Thread(target=self._convert_runner, args=(
            self._convert_event, self._convert_tasks, self._converted_models_results_deque), daemon=True, name="Model Convert Runner")
        self._convert_thread.start()

        self._root: Object3D = Object3D(name="Engine Root")

    @property
    def default_culling(self) -> Mesh.Culling:
        return self._default_culling

    @property
    def delegate(self) -> Optional[EngineDelegate]:
        return self._delegate

    @delegate.setter
    def delegate(self, value: Optional[EngineDelegate]) -> None:
        if self._delegate != value:
            self._delegate = value

    @property
    def root(self) -> Object3D:
        return self._root

    def _read_models_results(self) -> Generator[ModelReadResult, None, None]:
        """
        Generator yielding read models and the callbacks
        :return: Generator[ModelReadResult, None, None]
        """
        while True:
            try:
                yield self._read_models_results_deque.popleft()
            except IndexError:
                break

    def _converted_models_results(self) -> Generator[ModelLoadResult, None, None]:
        """
        Generator yielding converted models that are ready to be added (or not)
        :return: Generator[ConversionResult None, None]
        """
        while True:
            try:
                yield self._converted_models_results_deque.popleft()
            except IndexError:
                break

    @abstractmethod
    def initialize(self) -> None:
        """
        Setup the engine
        NOTE: If you are still asking yourself why you need this method, its to separate the initialization for the tests!

        :return: None
        """

    def step(self, now: float, dt: float) -> None:
        # Advertise the models that were loaded, if any
        # NOTE: Maybe this could be put in a task that would run less frequently than the framerate
        for id, model in self._read_models_results():
            read_callback = self._pending_read_callbacks.get(id)
            if read_callback:
                read_callback(model)

        # Add models that are finished being converted
        # NOTE: Maybe this could be put in a task that would run less frequently than the framerate
        for id, success, model, parent in self._converted_models_results():
            added = False
            if success:
                added = self._add_model(model=model, parent=parent)
            converted_callback = self._pending_convert_callbacks.get(id)
            if converted_callback:
                converted_callback(model if success and added else None)

        CacheManager.check_validity()

        self._root.step(now=now, dt=dt)

    @abstractmethod
    def read_model_from_file(self, path: str) -> Optional[Model]:
        """
        Reads a model from a file into a Model
        This does not adds the Model to the Engine's scene graph

        :param path: str - File path
        :return: Model - Model object
        """

    def read_model_from_file_async(self, path: str, callback: ModelReadCallback) -> None:
        """
        Reads a model from a file into a Model asynchronously
        You must provide a callback that will be called with the model when it is ready.
        :param path: str - File path
        :param callback: ModelReadCallback - Callback used when the model is ready
        :return: None
        """

        self._pending_read_callbacks[self._pending_read_counter] = callback
        self._read_tasks.append((self._pending_read_counter, path))
        self._pending_read_counter += 1
        self._read_event.set()

    def _read_runner(self, event: Event, tasks: 'Deque[Tuple[int, str]]', results: 'Deque[ModelReadResult]') -> None:
        while True:
            event.wait()
            event.clear()
            while True:
                try:
                    id, path = tasks.popleft()
                except IndexError:
                    break
                model = self.read_model_from_file(path=path)
                results.append((id, model))

    def load_scene(self, scene: Scene, callback: Optional[SceneLoadedCallback] = None) -> None:
        """
        Loads a scene in the engine

        :param scene: Scene to load
        :param callback: Optional[ModelLoadedCallback]
        :return:None
        """
        assert isinstance(scene, Scene)
        logger.debug("Loading scene \"{}\"".format(scene))

        def model_loaded(model: Optional[Model]) -> None:
            if callback:
                callback(scene)

            # Only after callback do we set the engine/delegate, we don't want
            # to risk calling it while the scene is being set up
            scene.engine = self

        self.add_model(model=scene.model, callback=model_loaded)

    def add_model(self, model: Model, parent: Optional[Object3D] = None, callback: Optional[ModelLoadedCallback] = None, asynchrone: bool = True) -> None:
        """
        Loads a model in the engine
        This adds the model in the engine's tree as a standalone branch,
        do not use this method if you want to add content to a scene.

        :param model: Model - Model
        :param parent: Optional[Object3D]
        :param callback: Optional[ModelLoadedCallback]
        :param asynchrone: If true, load the model asynchronously. Otherwise load it immediately
        :return: None
        """

        assert isinstance(model, Model)

        logger.debug("Adding model {}".format(model))

        if asynchrone:
            if callback:
                self._pending_convert_callbacks[self._pending_convert_counter] = callback
            self._convert_tasks.append((self._pending_convert_counter, model, parent))
            self._pending_convert_counter += 1
            self._convert_event.set()
        else:
            success = self.convert_model(model)
            added = False
            if success:
                added = self._add_model(model, parent)
            if callback is not None:
                callback(model if success and added else None)

    def _convert_runner(self, event: Event, tasks: 'Deque[Tuple[int, Model, Optional[Object3D]]]', results: 'Deque[ModelLoadResult]') -> None:
        while True:
            event.wait()
            event.clear()
            while True:
                try:
                    id, model, parent = tasks.popleft()
                except IndexError:
                    break
                result = self.convert_model(model=model)
                results.append((id, result is not None, model, parent))

    @abstractmethod
    def convert_model(self, model: Model) -> O:
        """
        Converts a model into the specific engine type
        :param model: Model
        :return: O
        """

    @abstractmethod
    def _add_model(self, model: Model, parent: Optional[Object3D] = None) -> bool:
        """
        Loads a model in the engine

        :param model: Model - Model
        :param parent: Optional[Object3D] - Parent to attach the model to
        :return: bool - Success of the operation
        """

    @abstractmethod
    def move_camera(self, matrix: Matrix44) -> None:
        """
        Move the camera

        :param matrix: Matrix44
        :return: None
        """
        pass

    @abstractmethod
    def get_picker(self, editor: 'ClientEditor') -> Picker:
        """
        Get the picker for this engine

        :param editor: Client editor
        :return: A Picker
        """
        pass

    @abstractmethod
    def get_movement_controller(
        self,
        config: Dict[str, Any],
        mapping_config: Dict[str, Any],
        picker: Picker
    ) -> Optional[LocalInputMethod]:
        """
        Get the movement controller for this engine
        :param config: Global configuration
        :param mapping_config: Input mapping configuration
        :param picker: Picker object
        :return: Return a LocalInputMethod, or None
        """
        pass

    @abstractmethod
    def show_status(self, status: str) -> None:
        """
        Show a status text on the screen
        :param status: str
        :return: None
        """

    @abstractmethod
    def hide_status(self) -> None:
        """
        Hide the status previously set with `show_status`
        :return: None
        """

    @abstractmethod
    def engine_matrix(self, mat: Matrix44) -> M:
        """
        Convert an EIS matrix into the format required for the engine.

        :param mat: Matrix44
        :return: M
        """
        pass

    @abstractmethod
    def eis_matrix(self, mat: M) -> Matrix44:
        """
        Convert an engine matrix into the format required for eis.

        :param mat: M
        :return: Matrix44
        """
        pass

    @abstractmethod
    def engine_quaternion(self, quat: Quaternion) -> Q:
        """
        Convert an EIS quaternion into the format required for the engine.

        :param quat: Quaternion
        :return: Q
        """
        pass

    @abstractmethod
    def eis_quaternion(self, quat: Q) -> Quaternion:
        """
        Convert an engine quaternion into the format required for EIS.

        :param quat: Q
        :return: Quaternion
        """
        pass

    @abstractmethod
    def engine_vector(self, vec: Vector3) -> V:
        """
        Convert an EIS vector into the format required for the engine.

        :param vec: Vector3
        :return: V
        """
        pass

    @abstractmethod
    def eis_vector(self, vec: V) -> Vector3:
        """
        Convert an engine vector into the format required for EIS.

        :param vec: V
        :return: Vector3
        """
        pass


T = TypeVar('T')


class EngineConverter(Generic[T], metaclass=ABCMeta):
    """
    Engine converter
    Converts the scene graph from/to EIS to/from the engine
    For each engine, the various abstract method must be defined
    """

    @classmethod
    @abstractmethod
    def engine_to_eis(cls, engine_graph: T) -> Optional[Model]:
        """
        Convert from the engine to EIS
        """
