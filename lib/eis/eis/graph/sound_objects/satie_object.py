from typing import Optional, Any, Type

from eis import EISEntityId
from eis.graph.sound_object import SoundObject
from satnet.entity import entity


@entity(id=EISEntityId.SATIE_OBJECT)
class SatieObject(SoundObject):

    _fields = ['_group', '_plugin']

    def __init__(
            self,
            name: Optional[str] = None,
            group: Optional[str] = None,
            plugin: Optional[str] = None,
            *args,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            *args,
            **kwargs)

        self._group = group or "default"
        self._plugin = plugin

    def update_sound_proxy(self, engine: 'Engine') -> None:
        if not self._sound_proxy:
            self._sound_proxy = engine.get_proxy(self)
            if self._sound_proxy:
                self._sound_proxy.make()
            else:
                logger.error("Could not make sound proxy for {}".format(self))

            if self._properties is not None:
                for name, value in self._properties.items():
                    self._activate_property(name, value)

    def _copy(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy(
            graph_type,
            *args,
            group=self._group,
            plugin=self._plugin,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['SatieObject'], *args: Any, **kwargs: Any) -> 'SatieObject':
        return super()._copy_shared(
            graph_type,
            *args,
            group=self._group,
            plugin=self._plugin,
            **kwargs
        )
