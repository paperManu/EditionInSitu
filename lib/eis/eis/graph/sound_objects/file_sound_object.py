from typing import Optional, Any, Type

from eis import EISEntityId
from eis.graph.sound_object import SoundObject
from satnet.entity import entity


@entity(id=EISEntityId.FILE_SOUND_OBJECT)
class FileSoundObject(SoundObject):

    _fields = ['_path']

    def __init__(
            self,
            name: Optional[str] = None,
            path: Optional[str] = None,
            **kwargs: Any
    ) -> None:
        super().__init__(
            name=name,
            **kwargs)

        self._path: Optional[str] = path

    @property
    def path(self) -> Optional[str]:
        return self._path

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()
        self._audio_handle = None
        self._plugin = "MonoIn"

    def _instantiate_sound(self) -> None:
        if self._path is None:
            return
        super()._instantiate_sound()

    def update_sound_proxy(self, engine: 'Engine') -> None:
        if not self._sound_proxy:
            self._sound_proxy = engine.get_proxy(self)
            if self._sound_proxy:
                self._sound_proxy.make()
            else:
                logger.error("Could not make sound proxy for {}".format(self))

            if self._properties is not None:
                for name, value in self._properties.items():
                    self._activate_property(name, value)

    def _copy(self, graph_type: Type['FileSoundObject'], *args: Any, **kwargs: Any) -> 'FileSoundObject':
        return super()._copy(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['FileSoundObject'], *args: Any, **kwargs: Any) -> 'FileSoundObject':
        return super()._copy_shared(
            graph_type,
            *args,
            path=self._path,
            **kwargs
        )