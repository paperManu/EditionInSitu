import os

from typing import Any, Callable, Optional

from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph.object_3d import Object3D
from satnet.entity import entity


@entity(id=EISEntityId.CAMERA)
class Camera(Object3D):

    _camera_3d_model: Optional[Object3D] = None

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    def initialize(self) -> None:
        super().initialize()

        self._is_editor = False  # Whether the editor is linked to this camera or not
        self._editor_callback: Optional[Callable] = None
        self._interactive = True
        self._added_model_to_scene = False

    def __str__(self) -> str:
        return "\033[0;34mCamera\033[0;0m({}, \033[1;30m{}\033[0;0m)".format(self._name, self._uuid)

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)

        for behavior in self.behaviors:
            ret += behavior.trace(level=level + 1)
        for child in self._children:
            ret += child.trace(level=level + 1)
        return ret

    def _load_model_and_add_to_scene(self) -> None:
        if Camera._camera_3d_model is None:
            Camera._camera_3d_model = self.load_model()
        if Camera._camera_3d_model is not None:
            self.add_child(Camera._camera_3d_model.copy())
        self._added_model_to_scene = True

    def added_to_scene(self) -> None:
        super().added_to_scene()
        if CLIENT and self._scene.editor is not None and not self._added_model_to_scene:
            self._load_model_and_add_to_scene()

    def load_model(self) -> Optional[Object3D]:
        path = os.path.join(os.path.realpath(".."), self._scene.editor.config.get("camera.model"))
        scene = self._scene.editor.load_scene_from_file(path=path)
        if scene is not None:
            return scene.model.root
        return None

    def on_scene_loaded(self) -> None:
        super().on_scene_loaded()
        if CLIENT and self._scene.editor is not None and not self._added_model_to_scene:
            self._load_model_and_add_to_scene()

    def set_as_editor_camera(self, is_editor: bool, callback: Optional[Callable]) -> None:
        """
        Set this camera so that the editor follows it. This should not be called manually, change
        the camera on the editor instead (this function will then be called by the editor)
        This method has no effect when called on the server side.
        """
        if CLIENT and is_editor != self._is_editor:
            self._editor_callback = callback
            self.visible = not is_editor  # Make the object invisible (or back visible) if it is the editor camera
            self.pickable = not is_editor
            self._is_editor = is_editor
            self._scene.editor.active_object = None
            self._scene.editor.clear_passive_objects()

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)
        if self._is_editor and self._editor_callback is not None:
            # Callback that sets the camera position to this objects position
            self._editor_callback()

    def removed_from_scene(self) -> None:
        super().removed_from_scene()
        if self._is_editor and self._scene.editor is not None:
            self._scene.editor.camera = None
