import logging
import numpy
from abc import abstractmethod
from typing import Any, Callable, Generic, List, Optional, Tuple, TYPE_CHECKING, TypeVar, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.base import GraphProxyBase, UniqueGraphEntity
from eis.graph.behavior import Behavior
from eis.graph.material import Material
from satmath.euler import Euler  # type: ignore
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3  # type: ignore
from satnet.entity import entity

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.client.input import LocalInputMethod
    from eis.engine import Engine
    from eis.graph.mesh import Mesh
    from eis.graph.model import Model
    from eis.graph.scene import Scene

logger = logging.getLogger(__name__)

O = TypeVar('O', bound='Object3D[Any]')


class Object3DProxy(Generic[O], GraphProxyBase[O]):

    def __init__(self, engine: 'Engine', proxied: O) -> None:
        super().__init__(engine=engine, proxied=proxied)

    @property
    @abstractmethod
    def bound_box(self) -> Tuple[Vector3, Vector3]:
        """
        Returns the bounding box of this object.
        :return: The bounding box
        """
        raise NotImplementedError

    @abstractmethod
    def add_child(self, child: O) -> None:
        pass

    @abstractmethod
    def gui(self, gui: bool) -> None:
        pass

    @abstractmethod
    def remove_child(self, child: O) -> None:
        pass

    @abstractmethod
    def remove(self) -> None:
        pass

    @abstractmethod
    def dispose(self) -> None:
        pass

    @abstractmethod
    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        pass


P = TypeVar('P', bound=Object3DProxy[Any])


@entity(id=EISEntityId.OBJECT_3D)
class Object3D(Generic[P], UniqueGraphEntity[P]):
    """
    3D object
    """

    _fields = ['_children', '_interactive', '_layer', '_mesh_id', '_pickable', '_synced_behaviors']

    def matrix_changed(self, sync: Sync, previous_value: Matrix44, value: Matrix44) -> None:
        self._dirty_decomposition = True
        self._dirty_matrix = False
        if self.proxy:
            self.proxy.set_matrix(self._matrix, self._matrix_offset)

    def matrix_offset_changed(self, sync: Sync, previous_value: Matrix44, value: Matrix44) -> None:
        self._dirty_decomposition_offset = True
        self._dirty_matrix_offset = False
        if self.proxy:
            self.proxy.set_matrix(self._matrix, self._matrix_offset)

    matrix = Sync[Matrix44]('_matrix', Matrix44.identity(), on_changed=matrix_changed)
    matrix_offset = Sync[Matrix44]('_matrix_offset', Matrix44.identity(), on_changed=matrix_offset_changed)
    visible = Sync[bool]('_visible', True, on_changed=UniqueGraphEntity.sync_invalidate)

    def __init__(
            self,
            name: Optional[str] = None,
            children: Optional[List['Object3D']] = None,
            interactive: Optional[bool] = None,
            mesh: Optional['Mesh'] = None,
            mesh_id: Optional[UUID] = None,
            matrix: Optional[Matrix44] = None,
            matrix_offset: Optional[Matrix44] = None,
            layer: Optional[int] = None,
            synced_behaviors: Optional[List[Behavior]] = None,
            local_behaviors: Optional[List[Behavior]] = None
    ) -> None:
        super().__init__(name=name)

        # Serializable
        self._children: List[Object3D] = children or []
        self._interactive = interactive if interactive is not None else False
        self._matrix = matrix if matrix is not None else Matrix44.identity()
        self._matrix_offset = matrix_offset if matrix_offset is not None else Matrix44.identity()
        self._layer = layer if layer is not None else 0

        self._synced_behaviors: List[Behavior] = synced_behaviors if synced_behaviors is not None else []

        self._mesh = mesh

        # Local
        self._parent: Optional[Object3D] = None
        self._scene: Optional['Scene'] = None
        self._editor: Optional['ClientEditor'] = None

        self._local_behaviors: List[Behavior] = local_behaviors if local_behaviors is not None else []

        location, rotation, scale = self._matrix.decompose()
        self._location = location
        self._rotation = Euler.from_quaternion(rotation)
        self._scale = scale
        self._dirty_matrix = False
        self._dirty_decomposition = False
        self._gui = False

        # Callback
        if self._mesh:
            self._mesh.added_to_object3d(self)
        self._mesh_id = mesh.uuid if mesh is not None else mesh_id

        # Offset
        self._location_offset = Vector3((0.0, 0.0, 0.0))
        self._rotation_offset = Euler((0.0, 0.0, 0.0))
        self._scale_offset = Vector3((1.0, 1.0, 1.0))
        self._dirty_matrix_offset = False
        self._dirty_decomposition_offset = False

        # Interaction
        self._pickable = True
        self._cursor_location = Vector3()
        self._cursor_down = False
        self._cursor_entered = False
        self._cursor_over = False
        self._cursor_exited = False
        self._cursor_out = True

    def __str__(self) -> str:
        return "\033[0;32m" + super().__str__()

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = indent + "{}\n".format(self)

        if self._mesh:
            ret += self._mesh.trace(level=level + 1)
        else:
            ret += indent + "    Mesh(" + str(self._mesh_id) + ")\n"

        for behavior in self.behaviors:
            ret += behavior.trace(level=level + 1)
        for child in self._children:
            ret += child.trace(level=level + 1)
        return ret

    @property
    def mesh(self) -> Optional['Mesh']:
        return self._mesh

    @mesh.setter
    def mesh(self, value: Optional['Mesh']) -> None:
        if self._mesh != value:
            if self._mesh is not None:
                self._mesh.removed_from_object3d(self)
                if self._model is not None:
                    self._mesh.removed_from_model(self._model)
            self._mesh = value
            if self._mesh is not None:
                self._mesh.added_to_object3d(self)
                if self._model is not None:
                    self._mesh.added_to_model(self._model)
            # Take id from mesh
            self._mesh_id = self._mesh.uuid if self._mesh else None

    @property
    def mesh_id(self) -> Optional[UUID]:
        return self._mesh_id

    @property
    def bound_box(self) -> Tuple[Vector3, Vector3]:
        if self._proxy is not None:
            return self._proxy.bound_box
        elif self._mesh is not None:
            bbox = (self.matrix_world.mul_vector3(self._mesh.bound_box[0]),
                    self.matrix_world.mul_vector3(self._mesh.bound_box[1]))
            return bbox
        else:
            return (Vector3(), Vector3())

    @property
    def hierarchical_bound_box(self) -> Tuple[Vector3, Vector3]:
        """
        Return the bounding box for this object and all its children
        :return: The bounding box
        """
        if self._proxy is not None:
            bbox = self._proxy.bound_box
        elif self._mesh is not None:
            bbox = self._mesh.bound_box
            bbox = (
                self.matrix_world.mul_vector3(bbox[0]),
                self.matrix_world.mul_vector3(bbox[1])
            )
        else:
            bbox = (Vector3(), Vector3())

        for child in self._children:
            child_bbox = child.hierarchical_bound_box
            bbox = (
                numpy.minimum(bbox[0], child_bbox[0]),
                numpy.maximum(bbox[1], child_bbox[1])
            )

        return bbox

    @property
    def parent(self) -> Optional['Object3D']:
        return self._parent

    @parent.setter
    def parent(self, value: Optional['Object3D'] = None) -> None:
        if self._parent != value:
            if self._parent:
                self.removed_from_parent()
            self._parent = value
            if self._parent:
                self.added_to_parent()

    def set_model(self, model: Optional['Model'], silent: bool = False) -> None:
        if self._model != model:
            # Propagate in hierarchy
            for child in self._children:
                child.set_model(model, True)

            # Call `super` last, because otherwise we'll queue network messages for
            # `self` before any of the children's dependencies are sent.
            super().set_model(model, silent)

    @property
    def editor(self) -> Optional['ClientEditor']:
        return self._editor

    @editor.setter
    def editor(self, value: Optional['ClientEditor']) -> None:
        if self._editor != value:
            if self._editor is not None:
                self.removed_from_editor()
            self._editor = value
            if self._editor is not None:
                self.added_to_editor()

            # Propagate in hierarchy
            for child in self._children:
                child.editor = self._editor

    @property
    def scene(self) -> Optional['Scene']:
        return self._scene

    @scene.setter
    def scene(self, value: Optional['Scene']) -> None:
        if self._scene != value:
            if self._scene is not None:
                self.removed_from_scene()
            self._scene = value
            if self._scene is not None:
                self.added_to_scene()

            # Propagate in hierarchy
            for child in self._children:
                child.scene = self._scene

    @property
    def children(self) -> List['Object3D']:
        return self._children

    @property
    def gui(self) -> bool:
        return self._gui

    @gui.setter
    def gui(self, gui: bool) -> None:
        if self._gui == gui:
            return

        self._gui = gui
        for child in self._children:
            child.gui = gui

        if self.proxy:
            self.proxy.gui(gui)

    @property
    def layer(self) -> int:
        return self._layer

    @layer.setter
    def layer(self, layer: int) -> None:
        if self._layer == layer:
            return

        self._layer = layer
        for child in self._children:
            child.layer = self._layer

    @property
    def behaviors(self) -> List[Behavior]:
        return self._synced_behaviors + self._local_behaviors

    @property
    def x(self) -> float:
        return self._location.x

    @x.setter
    def x(self, value: float) -> None:
        if self._location.x != value:
            self._location.x = value
            self._dirty_matrix = True

    @property
    def y(self) -> float:
        return self._location.y

    @y.setter
    def y(self, value: float) -> None:
        if self._location.y != value:
            self._location.y = value
            self._dirty_matrix = True

    @property
    def z(self) -> float:
        return self._location.z

    @z.setter
    def z(self, value: float) -> None:
        if self._location.z != value:
            self._location.z = value
            self._dirty_matrix = True

    @property
    def location(self) -> Vector3:
        return self._location.copy()

    @location.setter
    def location(self, value: Vector3) -> None:
        if self._location != value:
            self._location = value.copy()
            self._dirty_matrix = True

    @property
    def x_rotation(self) -> float:
        return self._rotation.x

    @x_rotation.setter
    def x_rotation(self, value: float) -> None:
        if self._rotation.x != value:
            self._rotation.x = value
            self._dirty_matrix = True

    @property
    def y_rotation(self) -> float:
        return self._rotation.y

    @y_rotation.setter
    def y_rotation(self, value: float) -> None:
        if self._rotation.y != value:
            self._rotation.y = value
            self._dirty_matrix = True

    @property
    def z_rotation(self) -> float:
        return self._rotation.z

    @z_rotation.setter
    def z_rotation(self, value: float) -> None:
        if self._rotation.z != value:
            self._rotation.z = value
            self._dirty_matrix = True

    @property
    def rotation(self) -> Euler:
        return self._rotation.copy()

    @rotation.setter
    def rotation(self, value: Euler) -> None:
        if self._rotation != value:
            self._rotation = value.copy()
            self._dirty_matrix = True

    @property
    def rotation_as_quat(self) -> Quaternion:
        return Quaternion.from_euler(self._rotation)

    @rotation_as_quat.setter
    def rotation_as_quat(self, value: Quaternion) -> None:
        self._rotation = Euler.from_quaternion(value)

    @property
    def x_scale(self) -> float:
        return self._scale.x

    @x_scale.setter
    def x_scale(self, value: float) -> None:
        if self._scale.x != value:
            self._scale.x = value
            self._dirty_matrix = True

    @property
    def y_scale(self) -> float:
        return self._scale.y

    @y_scale.setter
    def y_scale(self, value: float) -> None:
        if self._scale.y != value:
            self._scale.y = value
            self._dirty_matrix = True

    @property
    def z_scale(self) -> float:
        return self._scale.z

    @z_scale.setter
    def z_scale(self, value: float) -> None:
        if self._scale.z != value:
            self._scale.z = value
            self._dirty_matrix = True

    @property
    def scale(self) -> Vector3:
        return self._scale.copy()

    @scale.setter
    def scale(self, value: Vector3) -> None:
        if self._scale != value:
            self._scale = value.copy()
            self._dirty_matrix = True

    @property
    def matrix_world(self) -> Matrix44:
        if self._parent is None:
            return self._matrix.copy()
        return self._parent.matrix_world * self.matrix

    @matrix_world.setter
    def matrix_world(self, matrix: Matrix44) -> None:
        if self._parent is not None:
            parent_matrix = self._parent.matrix_world
        else:
            parent_matrix = Matrix44.identity()
        self.matrix = parent_matrix.inverse * matrix

    @property
    def x_offset(self) -> float:
        return self._location_offset.x

    @x_offset.setter
    def x_offset(self, value: float) -> None:
        if self._location_offset.x != value:
            self._location_offset.x = value
            self._dirty_matrix_offset = True

    @property
    def y_offset(self) -> float:
        return self._location_offset.y

    @y_offset.setter
    def y_offset(self, value: float) -> None:
        if self._location_offset.y != value:
            self._location_offset.y = value
            self._dirty_matrix_offset = True

    @property
    def z_offset(self) -> float:
        return self._location_offset.z

    @z_offset.setter
    def z_offset(self, value: float) -> None:
        if self._location_offset.z != value:
            self._location_offset.z = value
            self._dirty_matrix_offset = True

    @property
    def location_offset(self) -> Vector3:
        return self._location_offset.copy()

    @location_offset.setter
    def location_offset(self, value: Vector3) -> None:
        if self._location_offset != value:
            self._location_offset = value.copy()
            self._dirty_matrix_offset = True

    @property
    def x_rotation_offset(self) -> float:
        return self._rotation_offset.x

    @x_rotation_offset.setter
    def x_rotation_offset(self, value: float) -> None:
        if self._rotation_offset.x != value:
            self._rotation_offset.x = value
            self._dirty_matrix_offset = True

    @property
    def y_rotation_offset(self) -> float:
        return self._rotation_offset.y

    @y_rotation_offset.setter
    def y_rotation_offset(self, value: float) -> None:
        if self._rotation_offset.y != value:
            self._rotation_offset.y = value
            self._dirty_matrix_offset = True

    @property
    def z_rotation_offset(self) -> float:
        return self._rotation_offset.z

    @z_rotation_offset.setter
    def z_rotation_offset(self, value: float) -> None:
        if self._rotation_offset.z != value:
            self._rotation_offset.z = value
            self._dirty_matrix_offset = True

    @property
    def rotation_offset(self) -> Euler:
        return self._rotation_offset.copy()

    @rotation_offset.setter
    def rotation_offset(self, value: Euler) -> None:
        if self._rotation_offset != value:
            self._rotation_offset = value.copy()
            self._dirty_matrix_offset = True

    @property
    def x_scale_offset(self) -> float:
        return self._scale_offset.x

    @x_scale_offset.setter
    def x_scale_offset(self, value: float) -> None:
        if self._scale_offset.x != value:
            self._scale_offset.x = value
            self._dirty_matrix_offset = True

    @property
    def y_scale_offset(self) -> float:
        return self._scale_offset.y

    @y_scale_offset.setter
    def y_scale_offset(self, value: float) -> None:
        if self._scale_offset.y != value:
            self._scale_offset.y = value
            self._dirty_matrix_offset = True

    @property
    def z_scale_offset(self) -> float:
        return self._scale_offset.z

    @z_scale_offset.setter
    def z_scale_offset(self, value: float) -> None:
        if self._scale_offset.z != value:
            self._scale_offset.z = value
            self._dirty_matrix_offset = True

    @property
    def scale_offset(self) -> Vector3:
        return self._scale_offset.copy()

    @scale_offset.setter
    def scale_offset(self, value: Vector3) -> None:
        if self._scale_offset != value:
            self._scale_offset = value.copy()
            self._dirty_matrix_offset = True

    @property
    def material(self) -> Optional[Material]:
        """
        Get the material of the first geometry, if it exists
        """
        if self._mesh is not None and self._mesh.geometries:
            return self._mesh.geometries[0]._material
        return None

    @material.setter
    def material(self, value: Optional[Material]) -> None:
        """
        Set the materials for all geometries
        """
        if self._mesh is None:
            return

        for geometry in self._mesh.geometries:
            geometry.material = value

    @property
    def materials(self) -> Optional[List[Material]]:
        """
        Get all materials for this object
        """
        if self._mesh is not None:
            materials: List[Material] = []
            for geometry in self._mesh.geometries:
                if not geometry.material:
                    continue
                materials.append(geometry.material)
            return materials

        return None

    @property
    def interactive(self) -> bool:
        return self._interactive

    @interactive.setter
    def interactive(self, value: bool) -> None:
        if self._interactive != value:
            self._interactive = value

    @property
    def pickable(self) -> bool:
        return self._pickable

    @pickable.setter
    def pickable(self, value: bool) -> None:
        if self._pickable != value:
            self._pickable = value

    @property
    def interactive_ancestry(self) -> bool:
        if self._interactive:
            return True
        elif self._parent is not None:
            return self._parent.interactive_ancestry
        else:
            return False

    @property
    def closest_interactive(self) -> Optional['Object3D']:
        if self._interactive:
            return self
        elif self._parent is not None:
            return self._parent.closest_interactive
        else:
            return None

    @property
    def cursor_location(self) -> Vector3:
        return self._cursor_location

    @property
    def cursor_down(self) -> bool:
        return self._cursor_down

    def on_cursor_pressed(self, input: Optional['LocalInputMethod']) -> None:
        pass

    def on_cursor_released(self, input: Optional['LocalInputMethod']) -> None:
        pass

    @property
    def cursor_entered(self) -> bool:
        return self._cursor_entered

    def on_cursor_entered(self, input: Optional['LocalInputMethod']) -> None:
        pass

    @property
    def cursor_over(self) -> bool:
        return self._cursor_over

    def on_cursor_over(self, input: Optional['LocalInputMethod']) -> None:
        pass

    @property
    def cursor_exited(self) -> bool:
        return self._cursor_exited

    def on_cursor_exited(self) -> None:
        pass

    @property
    def cursor_out(self) -> bool:
        return self._cursor_out

    def reset_hover(self) -> None:
        self.set_hover(False)

    def set_hover(self, hovered: bool, input: Optional['LocalInputMethod'] = None, location: Vector3 = Vector3(), down: bool = False):
        hover_changed = False

        entered = hovered and not self._cursor_over
        if self._cursor_entered != entered:
            self._cursor_entered = entered
            if self._cursor_entered:
                self.on_cursor_entered(input)
            hover_changed = True

        exited = not hovered and self._cursor_over
        if self._cursor_exited != exited:
            self._cursor_exited = exited
            if self._cursor_exited:
                self.on_cursor_exited()
            hover_changed = True

        if self._cursor_over != hovered:
            self._cursor_over = hovered
            hover_changed = True
        if self._cursor_over:
            self.on_cursor_over(input)

        if self._cursor_out == hovered:
            self._cursor_out = not hovered
            hover_changed = True
        # Having an on_cursor out would be silly, wouldn't it? ;)

        if self._cursor_down != down:
            was_down = self._cursor_down
            if hovered:
                self._cursor_down = down
            if hovered and not was_down and self._cursor_down:
                self.on_cursor_pressed(input)
            elif was_down and not self._cursor_down:
                # Release even when outside
                self.on_cursor_released(input)

        if hover_changed:
            self.invalidate()

        return hover_changed

    def initialize(self) -> None:
        """
        Initializes the object and its hierarchy
        Needs to be called with constructing or after deserializing

        :return: None
        """
        super().initialize()

        # Propagate hierarchy
        for child in self._children:
            child.parent = self

        for behavior in self.behaviors:
            behavior.graphbase = self

    def added_to_parent(self) -> None:
        """
        Called when object is added to a parent (rooted or not)
        Usually in the `add_child[_at]`, `remove_child` or the `initialize` stack.

        :return: None
        """

        assert(self._parent)
        # Get the model/scene from the parent if we have one.
        # This setter will trigger added_to_model or removed_from_model accordingly
        if self._parent and self._parent.model:
            self.set_model(self._parent.model)
        else:
            # NOTE: NOTHING! Because otherwise the scene's model gets its own self referencing model overwritten
            pass

        # This setter will trigger added_to_scene or removed_from_scene accordingly
        if self.parent and self._parent.scene:
            self.scene = self._parent.scene
            # This setter will trigger added_to_editor
            if self._parent.editor:
                self.editor = self._parent.editor
        else:
            # NOTE: NOTHING! Because otherwise the scene's model gets its own self referencing model overwritten
            pass

    def removed_from_parent(self) -> None:
        # No parent means we're also detached from the model/scene if we were previously attached
        # This setter will trigger removed_from_model if needed
        self.set_model(None)
        # This setter will trigger removed_from_scene if needed
        self.scene = None
        # This setter will trigger removed_from_editor if needed
        self.editor = None

    def added_to_model(self, silent: bool = False) -> None:
        assert self._model
        super().added_to_model(silent)

        # Register ourselves last, so that dependencies are already registered before.
        self._model.add_object(self, silent)

        # Retrieve if we only hold the reference id
        if self._mesh is None and self._mesh_id is not None:
            # Retrieve the referenced instance
            self._mesh = self._model.get_mesh_by_uuid(self._mesh_id)
            if self._mesh is not None:
                self._mesh.added_to_object3d(self)
            else:
                logger.warning("Could not retrieve mesh id \"{}\"".format(self._mesh_id))

        # Add to Model
        if self._mesh is not None:
            self._mesh.added_to_model(self._model)

        for behavior in self.behaviors:
            behavior.added_to_model(self._model)

    def removed_from_model(self, silent: bool = False) -> None:
        assert self._model
        super().removed_from_model(silent)

        # Unregister ourselves
        self._model.remove_object(self, silent)

        # Remove from model
        if self._mesh is not None:
            self._mesh.removed_from_model(self._model)

        for behavior in self.behaviors:
            behavior.removed_from_model(self._model)

    def added_to_scene(self) -> None:
        for behavior in self.behaviors:
            behavior.scene = self._scene

    def removed_from_scene(self) -> None:
        for behavior in self.behaviors:
            behavior.scene = None

    def added_to_editor(self) -> None:
        pass

    def removed_from_editor(self) -> None:
        pass

    def on_scene_loaded(self) -> None:
        for child in self._children:
            child.on_scene_loaded()

        if self._scene is not None and self._scene.editor is not None:
            eisserver = self._scene.editor.delegate
            if eisserver is not None and self._model is not None:
                eisserver.set_object_behaviors(model=self._model, object=self)

    def on_scene_unloaded(self) -> None:
        for child in self._children:
            child.on_scene_loaded()

        if self._scene is not None and self._scene.editor is not None:
            eisserver = self._scene.editor.delegate
            if eisserver is not None and self._model is not None:
                eisserver.unset_object_behaviors(model=self._model, object=self)

    def step(self, now: float, dt: float) -> None:
        # Behaviors can affect our behavior ;) so run them first
        for behavior in self.behaviors:
            behavior.step(now=now, dt=dt)

        if self._dirty_matrix:
            # Use the setter in order to trigger the property sync
            self.matrix = Matrix44.from_translation(
                self._location) * Matrix44.from_euler(self._rotation) * Matrix44.from_scale(self._scale)
            # The matrix setter sets this to true but in this case we already have current decomposition
            self._dirty_decomposition = False
            self._dirty_matrix = False

        if self._dirty_decomposition:
            # Update local coordinates from matrix
            location, rotation, scale = self._matrix.decompose()
            self._location = location.copy()
            self._rotation = Euler.from_quaternion(rotation)
            self._scale = scale
            self._dirty_decomposition = False

        if self._dirty_matrix_offset:
            # Use the setter in order to trigger the property sync
            self.matrix_offset = Matrix44.from_translation(
                self._location_offset) * Matrix44.from_euler(self._rotation_offset) * Matrix44.from_scale(self._scale_offset)
            # The matrix setter sets this to true but in this case we already have current decomposition
            self._dirty_decomposition_offset = False
            self._dirty_matrix_offset = False

        if self._dirty_decomposition_offset:
            # Update local coordinates from matrix
            location, rotation, scale = self._matrix_offset.decompose()
            self._location_offset = location.copy()
            self._rotation_offset = Euler.from_quaternion(rotation)
            self._scale_offset = scale
            self._dirty_decomposition_offset = False

        super().step(now=now, dt=dt)

        for child in self._children:
            child.step(now=now, dt=dt)

        if self._model and self._mesh_id and (not self._mesh or self._mesh.uuid != self._mesh_id):
            self._mesh = self._model.get_mesh_by_uuid(self._mesh_id)
            self.invalidate()

        if self._mesh:
            self._mesh.step(now=now, dt=dt)

    def traverse(self, callback: Callable[['Object3D'], Any]) -> None:
        """
        Traverses the hierarchy top down, calling the passed method with every object.

        :param callback: Callable[['Object3D'], Any]
        :return: None
        """
        callback(self)
        for child in self._children:
            child.traverse(callback)

    def traverse_shallow(self, callback: Callable[['Object3D'], bool]) -> None:
        """
        Traverses the hierarchy top down, calling the passed method with every object.
        It wills top descending into children if the callback returns False.

        :param callback: Callable[['Object3D'], bool]
        :return: None
        """
        callback(self)
        for child in self._children:
            child.traverse(callback)

    def add_child(self, child: 'Object3D') -> 'Object3D':
        return self.add_child_at(child, len(self._children))

    def add_child_at(self, child: 'Object3D', index: int) -> 'Object3D':
        self._children.insert(index, child)
        child.parent = self
        if self.proxy:
            child.update_proxy(engine=self.proxy.engine)
            self.proxy.add_child(child)
        child.gui = self._gui
        return child

    def apply_offset(self) -> None:
        self._matrix = self._matrix_offset * self._matrix
        self._matrix_offset = Matrix44.identity()

    @property
    def matrix_world_with_offset(self) -> Matrix44:
        if self._parent is None:
            return self._matrix_offset * self._matrix
        return self._parent.matrix_world_with_offset * self._matrix_offset * self.matrix

    def remove_child(self, child: 'Object3D') -> Optional['Object3D']:
        if child in self._children:
            self._children.remove(child)
            child.parent = None
            if self.proxy:
                self.proxy.remove_child(child)
            return child
        else:
            return None

    def remove_all_children(self) -> None:
        for child in self._children:
            child.parent = None
        self._children = []

    def remove(self) -> None:
        """
        Remove the object from the graph. The reference to the object still
        exists within the engine, but not in the scene graph.
        :return: None
        """
        if self.parent:
            self.parent.remove_child(self)
        if self.proxy:
            self.proxy.remove()

    def dispose(self) -> None:
        """
        Disconnect the object from the graph and delete everthing from memory.
        If you want to remove a node from the graph and keep it around for later,
        remove() should be called instead.
        :return: None
        """
        if self.proxy:
            self.proxy.dispose()

    def find_by_uuid(self, uuid: UUID) -> Optional['Object3D']:
        """
        Get an Object3D in the hierarchy by uuid.

        :param uuid: UUID
        :return: Optional[Object3D]
        """
        if self._uuid == uuid:
            return self
        for child in self._children:
            result = child.find_by_uuid(uuid)
            if result:
                return result
        return None

    def find_by_name(self, name: str) -> Optional['Object3D']:
        """
        Get an Object3D in the hierarchy by name.

        :param name: str
        :return: Optional[Object3D]
        """
        if self._name == name:
            return self
        for child in self._children:
            result = child.find_by_name(name)
            if result:
                return result
        return None

    def add_behavior(self, behavior: Behavior, synchronize: bool = True) -> None:
        """
        Add a behavior to the graph entity.

        :param behavior: Behavior
        :return: None
        """
        assert behavior

        # Control that the behavior can be set to this object
        if not behavior.can_control(self):
            return

        for existing_behavior in self.behaviors:
            if type(existing_behavior) == type(behavior):
                logger.warning("There already is a behavior of type \"{}\" on this object".format(type(behavior)))
                return

        if synchronize:
            self._synced_behaviors.append(behavior)
        else:
            self._local_behaviors.append(behavior)
        behavior.graphbase = self

    def get_behavior_by_type(self, behavior_type: Type[Behavior]) -> Optional[Behavior]:
        """
        Get the behavior of the specified type or None if there is none

        :param behavior_type: Behavior type
        :return: Behavior of the specified type or None
        """
        for behavior in self.behaviors:
            if isinstance(behavior, behavior_type):
                return behavior
        return None

    def remove_behavior(self, behavior: Behavior) -> None:
        """
        Remove a behavior from the graph entity

        :param behavior: Behavior
        :return: None
        """
        assert behavior
        if behavior in self._synced_behaviors:
            self._synced_behaviors.remove(behavior)
            behavior.graphbase = None
        elif behavior in self._local_behaviors:
            self._local_behaviors.remove(behavior)
            behavior.graphbase = None

    def remove_behavior_by_type(self, behavior_type: Type[Behavior]) -> None:
        """
        Remove all behaviors of the given type

        :param behavior_type: Behavior type
        :return: None
        """
        for behavior in self.behaviors:
            if isinstance(behavior, behavior_type):
                self.remove_behavior(behavior)

    def _copy(self, graph_type: Type['Object3D'], *args: Any, **kwargs: Any) -> 'Object3D':
        return super()._copy(
            graph_type,
            *args,
            children=[child.copy() for child in self._children if not child.managed],
            interactive=self._interactive,
            matrix=self._matrix.copy(),
            matrix_offset=self._matrix_offset.copy(),
            layer=self._layer,
            mesh=self._mesh.copy() if self._mesh is not None and not self._mesh.managed else None,
            mesh_id=self._mesh_id if self._mesh is None or not self._mesh.managed else None,
            synced_behaviors=self._synced_behaviors.copy(),  # We don't want to share behaviors
            local_behaviors=self._local_behaviors.copy(),
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Object3D'], *args: Any, **kwargs: Any) -> 'Object3D':
        return super()._copy_shared(
            graph_type,
            *args,
            children=[child.copy_shared() for child in self._children if not child.managed],
            interactive=self._interactive,
            matrix=self._matrix.copy(),
            matrix_offset=self._matrix_offset.copy(),
            layer=self._layer,
            mesh=self._mesh if self._mesh is not None and not self._mesh.managed else None,  # Shared mesh
            mesh_id=self._mesh_id if self._mesh is None or not self._mesh.managed else None,
            synced_behaviors=self._synced_behaviors.copy(),  # We don't want to share behaviors
            local_behaviors=self._local_behaviors.copy(),
            **kwargs
        )
