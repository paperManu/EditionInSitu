import logging
from typing import Any, List, Optional, Type

from eis import EISEntityId
from eis.constants import SERVER
from eis.editor_support.switcher_manager import SwitcherManager
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.sound_objects.video_sound_object import VideoSoundObject
from eis.graph.texture import Texture
from satnet.entity import entity

logger = logging.getLogger(__name__)


@entity(id=EISEntityId.TEXTURE_VIDEO)
class TextureVideo(Texture):
    """
    Video texture object
    """

    _fields = ['video_uri']  # type: ignore

    def __init__(
            self,
            name: Optional[str] = None,
            video_uri: Optional[str] = None,
            **kwargs: Any) -> None:
        kwargs['format'] = 'bgra'
        super().__init__(name=name, **kwargs)
        self._video_uri = video_uri or ""
        self._video_handle: Optional[int] = None
        self._started = False
        self._sound_objects: List[VideoSoundObject] = []

    @property
    def video_uri(self) -> str:
        return self._video_uri

    @video_uri.setter
    def video_uri(self, uri: str) -> None:
        # Delete the previous video and create a new one with this URI
        self._video_uri = uri
        if self._video_handle is not None:
            self._stop_video()
            self._start_video()

    # Use whenever changing the value of the handle
    def _change_video_handle(self, handle: Optional[int]) -> None:
        if self._video_handle != handle:
            self._video_handle = handle
            for sound_object in self._sound_objects:
                sound_object.audio_handle = self._video_handle

    @property
    def video_playback(self) -> Optional[bool]:
        if SERVER or self._video_handle is None:
            return None

        return SwitcherManager().is_video_playing(handle=self._video_handle)

    @video_playback.setter
    def video_playback(self, play: bool) -> None:
        if SERVER or self._video_handle is None:
            return

        SwitcherManager().toggle_video_playback(handle=self._video_handle, play=play)

    def _start_video(self) -> bool:
        if SERVER:
            return True

        if self._started:
            return True

        # callback
        def cb(data: Any, height: int, width: int) -> None:
            self.size_x = width
            self.size_y = height
            self.data = bytes(data)

        switcher_manager = SwitcherManager()

        if self._video_handle is None:
            self._change_video_handle(switcher_manager.new_video(self._video_uri))

        if self._video_handle is None:
            return False

        # register callback to switcher manager
        switcher_manager.link_video(handle=self._video_handle, callback=cb)
        self._started = True
        return True

    def _stop_video(self) -> None:
        if SERVER:
            return

        if not self._started:
            return

        # Should delete video and change the behavior of the hover
        if self._video_handle is not None:
            switcher_manager = SwitcherManager()
            switcher_manager.unlink_video(self._video_handle)
            switcher_manager.close_video(self._video_handle)
            self._change_video_handle(None)
            self._started = False

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model)
        if not self._started and len(self._owners) > 0:
            self._start_video()

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model)
        if self._started:
            self._stop_video()

    def added_to_object3d(self, object3d: 'Object3D') -> None:
        # Make sure there isn't already a sound object for this object3d
        for child in object3d.children:
            if isinstance(child, VideoSoundObject):
                if child in self._sound_objects:
                    return

        sound_video_object = VideoSoundObject(name='', handle=self._video_handle)
        self._sound_objects.append(sound_video_object)
        object3d.add_child(sound_video_object)

    def removed_from_object3d(self, object3d: 'Object3D') -> None:
        found = False
        for sound_object in self._sound_objects:
            if sound_object.parent is object3d:
                object3d.remove_child(sound_object)
                self._sound_objects.remove(sound_object)
                found = True
                break
        if not found:
            logger.warning('Failed to find and delete the sound playback for this video texture')

    def _copy(self, graph_type: Type['Texture'], *args: Any, **kwargs: Any) -> 'Texture':
        return super()._copy(
            graph_type,
            *args,
            video_uri=self._video_uri,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Texture'], *args: Any, **kwargs: Any) -> 'Texture':
        return super()._copy_shared(
            graph_type,
            *args,
            video_uri=self._video_uri,
            **kwargs
        )
