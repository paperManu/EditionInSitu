from typing import Tuple

Vertex = Tuple[float, float, float]
Coord = Tuple[float, float, float]
Normal = Tuple[float, float, float]
TexCoord = Tuple[float, float]
Color = Tuple[float, float, float, float]  # RGBA
