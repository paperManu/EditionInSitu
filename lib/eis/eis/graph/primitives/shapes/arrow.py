from typing import Any, Type

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.primitives.shape import Shape
from satnet.entity import entity


@entity(id=EISEntityId.SHAPE_ARROW)
class Arrow(Shape):
    # Don't sync points, we manage them internally
    Shape.points.enabled = False

    arrow_length = Sync[float]('_arrow_length', 1.0, on_changed=Shape.sync_invalidate)
    arrow_width = Sync[float]('_arrow_width', 1.0, on_changed=Shape.sync_invalidate)
    head_length_ratio = Sync[float]('_head_length_ratio', 0.5, on_changed=Shape.sync_invalidate)
    head_width_ratio = Sync[float]('_head_width_ratio', 0.5, on_changed=Shape.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            arrow_length: float = 1.0,
            arrow_width: float = 1.0,
            head_length_ratio=0.5,
            head_width_ratio=0.5,
            **kwargs: Any

    ) -> None:
        super().__init__(*args, **kwargs)

        self._arrow_length = arrow_length
        self._arrow_width = arrow_width
        self._head_length_ratio = head_length_ratio
        self._head_width_ratio = head_width_ratio

    def update(self) -> None:
        hl = self._arrow_length / 2
        hw = self._arrow_width / 2
        head_bottom = hl - (self._arrow_length * self._head_length_ratio)
        bar_hw = (self._arrow_width * self._head_width_ratio) / 2

        self.points = [
            (0, hl, 0,),
            (hw, head_bottom, 0),
            (bar_hw, head_bottom, 0),
            (bar_hw, -hl, 0),
            (-bar_hw, -hl, 0),
            (-bar_hw, head_bottom, 0),
            (-hw, head_bottom, 0),
            (0, hl, 0)
        ]

        super().update()

    def _copy(self, graph_type: Type['Arrow'], *args: Any, **kwargs: Any) -> 'Arrow':
        return super()._copy(
            graph_type,
            *args,
            arrow_length=self._arrow_length,
            arrow_width=self._arrow_width,
            head_length_ratio=self._head_length_ratio,
            head_width_ratio=self._head_width_ratio,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Arrow'], *args: Any, **kwargs: Any) -> 'Arrow':
        return super()._copy_shared(
            graph_type,
            *args,
            arrow_length=self._arrow_length,
            arrow_width=self._arrow_width,
            head_length_ratio=self._head_length_ratio,
            head_width_ratio=self._head_width_ratio,
            **kwargs
        )
