import logging
from typing import Any, Dict, Optional, Type, TYPE_CHECKING
from uuid import UUID

from eis import EISEntityId
from eis.cache_manager import CacheManager, Cacheable
from eis.entity import Sync
from eis.graph.geometry import Geometry
from eis.graph.material import Material
from eis.graph.mesh import Mesh
from eis.graph.object_3d import Object3D

if TYPE_CHECKING:
    from eis.graph.model import Model

logger = logging.getLogger(__name__)


class PrimitiveSignature:
    def __init__(self, primitive_type: EISEntityId, parameters: Dict[str, Any]) -> None:
        self._signature = parameters
        self._signature['type'] = primitive_type
        self._signature_hash = hash(frozenset(self._signature.items()))

    @property
    def signature_hash(self) -> int:
        return self._signature_hash


class PrimitiveCached(Cacheable):
    def __init__(self, primitive: 'Primitive') -> None:
        super().__init__()
        self._primitive = primitive

    def get_content(self) -> 'Primitive':
        return self._primitive


class Primitive(Object3D):

    _fields = ['_material_id']

    def material_changed(self, sync: Sync, previous_material: Optional[Material], material: Optional[Material]) -> None:
        """
        Set the material for the geometry
        """
        if previous_material != material:
            if self._model is not None and previous_material is not None and previous_material in self._model.materials:
                previous_material.removed_from_model(self._model)
            if self._model is not None and self._material is not None:
                self._material.added_to_model(self._model)
            # Take id from material
            self._material_id = self._material.uuid if self._material else None
            self._geometry.material = material
            self.invalidate_graph()

    material = Sync[Material]('_material', Material(), on_changed=material_changed)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,
            **kwargs: Any) -> None:
        self._primitive_signature = None
        if 'signature' in kwargs:
            self._primitive_signature = kwargs['signature']
            del kwargs['signature']  # Do not pass that to the object, it's only for the Primitive's class use

        # self._geometry and self._mesh are managed, so we have to keep track of the material by ourselve
        self._material = material
        self._material_id = material.uuid if material is not None else material_id

        self._geometry = Geometry(name=name, material=material, material_id=material_id)
        self._geometry.managed = True  # Do not sync over the network
        '''self._do_build = True'''

        mesh = Mesh(name=name, geometries=[self._geometry])
        mesh.managed = True  # Do not sync over the network

        if 'mesh' in kwargs:
            del kwargs['mesh']  # Make sure not to specify another mesh in kwargs

        super().__init__(*args, name=name, mesh=mesh, **kwargs)

    def trace(self, level: int = 0) -> str:
        indent = "    " * level
        ret = super().trace(level=level)
        if self._material:
            ret += self._material.trace(level=level + 1)
        else:
            ret += indent + "    Material(" + str(self.material_id) + ")\n"
        return ret

    def initialize(self) -> None:
        super().initialize()

        # As the mesh is generated algoritmically every time a Primitive is created,
        # the mesh_id has to be the same as self._mesh.uuid.
        if self._mesh is not None:
            self._mesh_id = self._mesh.uuid

        self._geometry.material = self._material
        self.build()

        # Old code using caches (isn't working), see issue "Bug EIS-229" on JIRA
        '''
        # Build the primitive from the initialization parameters if it was never built before
        signature_hash = self._primitive_signature.signature_hash if self._primitive_signature else None
        cached = CacheManager.get_cacheable(key='primitives', signature=signature_hash)
        if cached:
            self._geometry = cached.get_content().geometry.copy_shared()
            cached.last_used = time()
            mesh = Mesh(name=self._name, geometries=[self._geometry])
            mesh.managed = True  # Do not sync over the network
            self.mesh = mesh
            self._do_build = False
            self.build()
        else:
            self.build()
            if signature_hash is not None:
                CacheManager.add_cacheable(key='primitives', signature=signature_hash, cacheable=PrimitiveCached(primitive=self))
            else:
                logger.warning('Could not cache primitive of type {}'.format(type(self).__name__))
        '''

    def sync_invalidate(self, sync: Sync, previous_value: Any, value: Any) -> None:
        self.invalidate()

    @property
    def geometry(self) -> Geometry:
        return self._geometry

    @property
    def material_id(self) -> Optional[UUID]:
        return self._material_id

    def added_to_model(self, silent: bool) -> None:
        model = self._model

        # We handle the material before the rest, to make sure that when deserializing
        # on the client side, the material is deserialized first

        # Retrieve if we only hold the reference id
        if not self._material and self._material_id:
            # Retrieve the referenced instance, setter will take care of registration
            self.material = model.get_material_by_uuid(self._material_id)
            if not self._material:
                logger.warning("Could not retrieve material id \"{}\"".format(self._material_id))

        # Add to model
        if self._material is not None:
            self._material.added_to_model(model)

        super().added_to_model(silent=silent)

    def removed_from_model(self, silent: bool) -> None:
        super().removed_from_model(silent=silent)

        # Remove from model
        if self._material is not None and self._material in self._model.materials:
            self._material.removed_from_model(self._model)

    def build(self) -> None:
        """
        Build the primitive
        :return: None
        """
        self._geometry.vertices.clear()
        self._geometry.normals.clear()
        self._geometry.texcoords.clear()
        self._geometry.primitives.clear()
        self._geometry.invalidate()

    def update(self) -> None:
        # Build before calling super, this way, the geometry will be ready when updating the proxy
        self.build()

        # Make sure there are as many geom colors as there are vertices
        geom_colors = self._geometry.colors
        geom_vertices = self._geometry.vertices
        if len(geom_colors) > 0 and len(geom_colors) != len(geom_vertices):
            if len(geom_colors) > len(geom_vertices):
                geom_colors = geom_colors[0:len(geom_vertices)]
            else:
                geom_colors.append([(1.0, 1.0, 1.0, 1.0)] * (len(geom_vertices) - len(geom_colors)))

        # Old code using caches (isn't working), see issue "Bug EIS-229" on JIRA
        '''
        if self._do_build:
            self.build()
        else:
            self._do_build = True
        '''

        self._geometry._geometry_updated = True
        super().update()

    def _copy(self, graph_type: Type['Primitive'], *args: Any, **kwargs: Any) -> 'Primitive':
        return super()._copy(
            graph_type,
            *args,
            material=self._material.copy() if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Primitive'], *args: Any, **kwargs: Any) -> 'Primitive':
        return super()._copy_shared(
            graph_type,
            *args,
            # Materials are shared, so no copy, not even _shared
            material=self._material if self._material is not None and not self._material.managed else None,
            material_id=self._material_id if self._material is None or not self._material.managed else None,
            **kwargs
        )
