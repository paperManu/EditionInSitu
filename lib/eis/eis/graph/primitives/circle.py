import math
from typing import Any, Optional, Type
from uuid import UUID

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.material import Material
from eis.graph.primitives import Primitive, PrimitiveSignature
from satmath.matrix44 import Matrix44
from satnet.entity import entity


@entity(id=EISEntityId.PRIMITIVE_CIRCLE)
class Circle(Primitive):
    """
    Inspired by: https://github.com/mrdoob/three.js/blob/dev/src/geometries/CircleGeometry.js
    """

    radius = Sync[float]('_radius', 0.5, on_changed=Primitive.sync_invalidate)
    segments = Sync[int]('_segments', 32, on_changed=Primitive.sync_invalidate)
    theta_start = Sync[float]('_theta_start', 0.00, on_changed=Primitive.sync_invalidate)
    theta_length = Sync[float]('_theta_length', math.pi * 2.0, on_changed=Primitive.sync_invalidate)

    def __init__(
            self,
            *args: Any,
            name: Optional[str] = None,
            matrix: Optional[Matrix44] = None,
            material: Optional[Material] = None,
            material_id: Optional[UUID] = None,

            radius: float = 0.5,
            segments: int = 32,
            theta_start: float = 0.0,
            theta_length: float = math.pi * 2.0,
            **kwargs
    ) -> None:
        parameters = {
            'radius': radius,
            'segments': segments,
            'theta_start': theta_start,
            'theta_length': theta_length
        }
        kwargs['signature'] = PrimitiveSignature(primitive_type=EISEntityId.PRIMITIVE_CIRCLE, parameters=parameters)

        super().__init__(
            *args,
            name=name,
            matrix=matrix,
            material=material,
            material_id=material_id,
            **kwargs
        )

        self._radius = radius
        self._segments = segments
        self._theta_start = theta_start
        self._theta_length = theta_length

        self._vertex_count = 0

    def build(self) -> None:
        super().build()

        # center point
        self._geometry.vertices.append((0.0, 0.0, 0.0))
        self._geometry.normals.append((0.0, 0.0, 1.0))
        self._geometry.texcoords.append((0.5, 0.5))

        for s in range(0, self._segments + 1):
            segment = self._theta_start + s / self._segments * self._theta_length

            # vertex
            vertex = self._radius * math.cos(segment), self._radius * math.sin(segment), 0.0
            self._geometry.vertices.append(vertex)

            # normal
            self._geometry.normals.append((0.0, 0.0, 1.0))

            # uvs
            self._geometry.texcoords.append((
                (vertex[0] / self._radius + 1.0) / 2.0,
                (vertex[1] / self._radius + 1.0) / 2.0
            ))

        # indices
        for i in range(1, self._segments + 1):
            self._geometry.primitives.append([i, i + 1, 0])

    # region Lifecycle

    # endregion

    # region Copy

    def _copy(self, graph_type: Type['Circle'], *args: Any, **kwargs: Any) -> 'Circle':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            segments=self._segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['Circle'], *args: Any, **kwargs: Any) -> 'Circle':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            segments=self._segments,
            theta_start=self._theta_start,
            theta_length=self._theta_length,
            **kwargs
        )

    # endregion
    ...
