import math
from typing import Any, Type

from eis.entity import Sync
from eis.graph.helpers import AxisHelper
from eis.graph.material import Material
from eis.graph.primitives.torus import Torus


class RotationHelper(AxisHelper):
    tube_radius = 0.025
    tubular_segments = 96

    def update_axis(self, sync: Sync, value: Any) -> None:
        pass
        #self._x_axis.radius = self._radius
        #self._y_axis.radius = self._radius
        #self._z_axis.radius = self._radius

    def __init__(
            self,
            *args: Any,
            **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)

        self._x_material = Material(color=(1.00, 0.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._x_material.managed = True
        self._x_axis = Torus(
            material=self._x_material,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._x_axis.managed = True
        self._x_axis.y_rotation = math.pi / 2.0
        self.add_child(self._x_axis)

        self._y_material = Material(color=(0.00, 1.00, 0.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._y_material.managed = True
        self._y_axis = Torus(
            material=self._y_material,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._y_axis.managed = True
        self._y_axis.x_rotation = math.pi / 2.0
        self.add_child(self._y_axis)

        self._z_material = Material(color=(0.00, 0.00, 1.00, 0.50), shading_model=Material.ShadingModel.EMISSIVE)
        self._z_material.managed = True
        self._z_axis = Torus(
            material=self._z_material,
            tube=RotationHelper.tube_radius,
            tubular_segments=RotationHelper.tubular_segments
        )
        self._z_axis.managed = True
        self.add_child(self._z_axis)

    def _copy(self, graph_type: Type['RotationHelper'], *args: Any, **kwargs: Any) -> 'RotationHelper':
        return super()._copy(
            graph_type,
            *args,
            radius=self._radius,
            **kwargs
        )

    def _copy_shared(self, graph_type: Type['RotationHelper'], *args: Any, **kwargs: Any) -> 'RotationHelper':
        return super()._copy_shared(
            graph_type,
            *args,
            radius=self._radius,
            **kwargs
        )
