import random
from math import sin

from eis import EISEntityId
from eis.graph.behavior import Behavior, behavior
from eis.graph.object_3d import Object3D
from satmath.vector3 import Vector3


@behavior(id=EISEntityId.BEHAVIOR_RANDOM_MOTION)
class RandomMotionBehavior(Behavior):
    """
    Behavior which moves the object seemingly randomly
    :param speed_factor: float - The higher the speed factor, the faster the movement
    :param motion_scale: float - Scale of the movement area, 1.0 corresponds to a cube of 2x2x2
    :param offset_location: bool - If true, applies the movement through the offset matrix
    """

    _fields = ['_speed_factor', '_motion_scale', '_offset_location']  # type: ignore

    def __init__(self, speed_factor: float = 1.0, motion_scale: float = 1.0, offset_location: bool = False) -> None:
        super().__init__()
        self._speed_factor = speed_factor
        self._motion_scale = motion_scale
        self._offset_location = offset_location
        self._randomness_x = random.uniform(0.5, 2.0)
        self._randomness_y = random.uniform(0.5, 2.0)
        self._randomness_z = random.uniform(0.5, 2.0)

        self._starting_position = Vector3((0.0, 0.0, 0.0))

    def step(self, now: float, dt: float) -> None:
        if self._graphbase:
            assert(isinstance(self._graphbase, Object3D))
            speed = self._speed_factor
            location_offset = Vector3((
                sin(now * self._randomness_x * speed),
                sin(now * self._randomness_y * speed),
                sin(now * self._randomness_z * speed),
            )) * self._motion_scale
            if self._offset_location:
                self._graphbase.location_offset = location_offset
            else:
                self._graphbase.location = self._starting_position + location_offset

    def added_to_object(self) -> None:
        super().added_to_object()
        if self._graphbase:
            assert(isinstance(self._graphbase, Object3D))
            self._starting_position = self._graphbase.location
