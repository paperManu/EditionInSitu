import math
import random
from eis import EISEntityId
from eis.constants import SERVER
from eis.graph.behavior import Behavior, behavior
from eis.graph.object_3d import Object3D


@behavior(id=EISEntityId.BEHAVIOR_TEST)
class TestBehavior(Behavior):

    def __init__(self) -> None:
        super().__init__()
        self.a = random.random()
        self.b = random.random()
        self.c = random.random()
        self.d = random.random()

    def step(self, now: float, dt: float) -> None:
        if SERVER and self._graphbase:
            assert(isinstance(self._graphbase, Object3D))
            self._graphbase.x = math.sin((now + self._graphbase.z) * self.a) * 20.0
            self._graphbase.y = math.cos((now + self._graphbase.z) * self.b) * 20.0
            self._graphbase.y_rotation = math.cos((now + self._graphbase.x) * self.c)
            self._graphbase.z_rotation = math.sin((now + self._graphbase.y) * self.d)
