import logging
from typing import Optional, List

from eis import EISEntityId
from eis.actions.animation import AddKeyframeAction
from eis.constants import CLIENT
from eis.graph.behavior import Behavior, behavior
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.object_3d import Object3D

logger = logging.getLogger(__name__)


@behavior(id=EISEntityId.BEHAVIOR_AUTO_KEYFRAME)
class AutoKeyframeBehavior(Behavior):
    """
    Behavior which adds a keyframe at a regular interval while the animation
    is playing, if no keyframe is located nearby
    """

    _fields = ['_attributes', '_interval']  # type: ignore

    def __init__(
        self,
        attributes: Optional[List[str]] = None,
        interval: Optional[int] = None
    ) -> None:
        super().__init__()
        self._attributes = attributes if attributes is not None else ""
        self._interval = interval if interval is not None else 1

    def initialize(self) -> None:
        self._time_last_keyframe_added: Optional[float] = None

    def step(self, now: float, dt: float) -> None:
        assert(isinstance(self._graphbase, Object3D))
        if not CLIENT or not self._graphbase or not self._scene or not self._scene.editor:
            return

        timeline = self._scene.editor.timeline
        if not timeline.running:
            return

        object3d: Object3D = self._graphbase
        for attribute in self._attributes:
            if not hasattr(object3d, attribute):
                return

        time = timeline.time
        animation_behavior = object3d.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is not None:
            if not animation_behavior.writing:
                animation_behavior.writing = True
            for attribute in self._attributes:
                if attribute in animation_behavior.curves:
                    animation_curve = animation_behavior.curves[attribute]
                    previous_frame, next_frame = animation_curve.get_enclosing_keyframes(time)

                    # If we are closer than self._interval to the previous or next frame, do nothing
                    if previous_frame is not None and time - previous_frame[0] < self._interval:
                        return
                    if next_frame is not None and next_frame[0] - time < self._interval:
                        return
                    if self._time_last_keyframe_added is not None and abs(time - self._time_last_keyframe_added) < self._interval:
                        return

        for attribute in self._attributes:
            value = getattr(object3d, attribute)
            if value is not None:
                self._scene.editor.machine.client.session.action(AddKeyframeAction(
                    object=object3d, attribute=attribute, time=time, value=value))

        self._time_last_keyframe_added = time

    def added_to_object(self) -> None:
        super().added_to_object()
        behavior = self._graphbase.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            behavior.writing = True

    def removed_from_object(self) -> None:
        super().removed_from_object()
        behavior = self._graphbase.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            behavior.writing = False
