from eis import EISEntityId
from eis.graph.behavior import Behavior, behavior
from eis.graph.object_3d import Object3D


@behavior(id=EISEntityId.BEHAVIOR_HIDDEN)
class HiddenBehavior(Behavior):
    """
    Behavior which hides the object it is set to
    """

    def __init__(self) -> None:
        super().__init__()

    def added_to_object(self) -> None:
        super().added_to_object()
        assert(isinstance(self._graphbase, Object3D))
        self._graphbase.visible = False

    def removed_from_object(self) -> None:
        super().removed_from_object()
        assert(isinstance(self._graphbase, Object3D))
        self._graphbase.visible = True
