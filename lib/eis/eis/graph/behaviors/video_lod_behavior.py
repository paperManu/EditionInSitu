import logging
from typing import Optional

from eis import EISEntityId
from eis.constants import CLIENT
from eis.graph.behavior import Behavior, behavior
from eis.graph.object_3d import Object3D
from eis.graph.texture_video import TextureVideo

logger = logging.getLogger(__name__)


@behavior(id=EISEntityId.BEHAVIOR_VIDEO_LOD)
class VideoLODBehavior(Behavior):
    """
    Behavior which adds/remove a video behavior based in the distance
    """

    _fields = ['_distance']  # type: ignore

    DEFAULT_DISTANCE = 32.0

    def __init__(
        self,
        distance: Optional[float] = None
    ) -> None:
        super().__init__()
        self._distance = distance if distance is not None else VideoLODBehavior.DEFAULT_DISTANCE

    def initialize(self) -> None:
        self._video_active = False

    def _get_distance(self) -> float:
        obj = self._graphbase
        obj_matrix = obj.matrix_offset * obj.matrix_world
        cam_matrix = self._scene.editor.matrix
        return (cam_matrix.translation - obj_matrix.translation).length

    def step(self, now: float, dt: float) -> None:
        if not CLIENT or self._graphbase is None or self._scene is None:
            return

        assert(isinstance(self._graphbase, Object3D))

        distance = self._get_distance()
        if distance >= self._distance and not self._video_active:
            return

        if distance < self._distance and self._video_active:
            return

        for material in self._graphbase.materials:
            for material_texture in material.material_textures:
                if not material_texture.texture:
                    continue
                texture = material_texture.texture
                if isinstance(texture, TextureVideo):
                    texture.video_playback = self._video_active

        self._video_active = not self._video_active
