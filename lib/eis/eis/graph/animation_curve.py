import logging
import time
from bisect import bisect_left, bisect_right
from copy import copy
from enum import IntEnum
from math import ceil
from numbers import Number
from typing import Any, List, Optional, Tuple, Type, TYPE_CHECKING

from eis import EISEntityId
from eis.entity import Sync
from eis.graph.base import SharedGraphEntity
from satnet.entity import entity
from satmath.base import BaseObject as MathBaseObject
from satmath.vector4 import Vector4
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion

if TYPE_CHECKING:
    from eis.graph.model import Model

logger = logging.getLogger(__name__)


@entity(id=EISEntityId.ANIMATION_CURVE)
class AnimationCurve(SharedGraphEntity):
    """
    Holds the animation curve for a given attribute
    """

    class InterpolationType(IntEnum):
        CONSTANT = 1
        LINEAR = 2
        SMOOTH = 3

    def keyframes_changed(self, sync: Sync, previous_value: List[Tuple[float, Any]], value: List[Tuple[float, Any]]) -> None:
        self._keyframes = value
        self._timestamp = time.time()

    def interpolation_changed(self, sync: Sync, previous_value: InterpolationType, value: InterpolationType) -> None:
        self._interpolation = value

    keyframes = Sync[List[Tuple[float, Any]]]('_keyframes', [], on_changed=keyframes_changed)
    interpolation = Sync[InterpolationType]('_interpolation', [], on_changed=interpolation_changed)

    def __init__(
        self,
        name: Optional[str] = None,
        keyframes: Optional[List[Tuple[float, Any]]] = None,
        interpolation: Optional[InterpolationType] = None
    ) -> None:
        super().__init__(name)
        self._keyframes = keyframes or []
        self._interpolation = interpolation or AnimationCurve.InterpolationType.CONSTANT

    def initialize(self):
        self._timestamp = time.time()
        self._looping = False
        self._loop_delay = 1.0

    @property
    def empty(self) -> bool:
        """
        This property is true if the curve is empty
        :return bool:
        """
        return len(self._keyframes) == 0

    @property
    def looping(self) -> bool:
        """
        This property is true when the curve loops
        :return bool:
        """
        return self._looping

    @looping.setter
    def looping(self, looping: bool) -> None:
        self._looping = looping

    @property
    def loop_delay(self) -> float:
        """
        This property is the time going from the last keyframe back to the first one
        :return float
        """
        return self._loop_delay

    @loop_delay.setter
    def loop_delay(self, delay: float) -> None:
        self._loop_delay = max(0.05, delay)

    @property
    def timestamp(self) -> float:
        return self._timestamp

    def add_keyframe(self, time: float, value: Any) -> None:
        """
        Add a new keyframe, given a time and value
        :param time: float - Time
        :param value: Any - Value for the keyframe
        """
        keyframes = copy(self._keyframes)

        # We take care of removing any frame at the same timing
        simultaneous_position = bisect_right([keyframe[0] for keyframe in self._keyframes], time)
        if simultaneous_position != len(keyframes) and keyframes[simultaneous_position - 1][0] == time:
            keyframes.pop(simultaneous_position - 1)
        keyframes.append((time, value))
        keyframes.sort(key=lambda t: t[0])

        self.keyframes = keyframes

        # We also check that the keyframe type supports interpolations other than constant
        if self.interpolation != AnimationCurve.InterpolationType.CONSTANT and not isinstance(value, MathBaseObject) and not isinstance(value, Number):
            logger.warning("Frame value of type {} does not support interpolation. Reverting to constant".format(type(value)))
            self.interpolation = AnimationCurve.InterpolationType.CONSTANT

    def added_to_model(self, model: 'Model') -> None:
        super().added_to_model(model)
        model.add_animation_curve(self)

    def get_keyframe_at(self, time: float) -> Optional[Tuple[float, Any]]:
        """
        Get the keyframe at the given time, if it exists
        :return: Optional[Tuple[float, Any]] - Keyframe, or None
        """
        keyframes = [keyframe for keyframe in self._keyframes if keyframe[0] == time]
        if not keyframes:
            return None
        return keyframes[0]

    def get_value_at(self, time: float) -> Optional[Any]:
        """
        Get the value at the given time
        :param time: float - Time
        :return: Optional[Any] - Value, or None
        """
        if len(self._keyframes) == 0:
            return None

        total_duration = self._keyframes[-1][0] + self._loop_delay
        if self._looping and len(self._keyframes) >= 2:
            if time <= self._keyframes[0][0]:
                return self._keyframes[0][1]
            time = time % total_duration
        else:
            if time <= self._keyframes[0][0]:
                return self._keyframes[0][1]
            elif time >= self._keyframes[-1][0]:
                return self._keyframes[-1][1]

        exact_keyframe = self.get_keyframe_at(time)
        if exact_keyframe is not None:
            return exact_keyframe[1]

        previous_frame, next_frame = self.get_enclosing_keyframes(time=time)
        if self._looping and next_frame is None:
            next_frame = self._keyframes[0]
            frame_gap = ceil(time / total_duration) * total_duration - previous_frame[0]
        elif previous_frame is None:
            previous_frame = next_frame
            frame_gap = 1.0
        else:
            frame_gap = next_frame[0] - previous_frame[0]

        if self._interpolation == AnimationCurve.InterpolationType.CONSTANT:
            return previous_frame[1]
        elif self._interpolation == AnimationCurve.InterpolationType.LINEAR:
            frame_position = (time - previous_frame[0]) / frame_gap

            if isinstance(previous_frame[1], Quaternion):
                return previous_frame[1].slerp(next_frame[1], frame_position)
            else:
                return previous_frame[1] * (1.0 - frame_position) + next_frame[1] * frame_position
        elif self._interpolation == AnimationCurve.InterpolationType.SMOOTH:
            time_since_frame = time - previous_frame[0]
            normalized_time = time_since_frame / frame_gap

            if isinstance(previous_frame[1], Quaternion):
                # For now, smoothed interpolation is the same as linear for quaternions
                return previous_frame[1].slerp(next_frame[1], normalized_time)
            else:
                # We do a Hermite interpolation, with tangents computed as a fraction of next - previous keyframes
                M = Matrix44(
                    [1, 0, 0, 0,
                     0, 0, 1, 0,
                     -3, 3, -2, -1,
                     2, -2, 1, 1]
                )
                t = Vector4([1, normalized_time, normalized_time ** 2.0, normalized_time ** 3.0])
                coeffs = M.mul_vector4(t)
                direction = (next_frame[1] - previous_frame[1]) / time_since_frame
                return previous_frame[1] * coeffs[0] + next_frame[1] * coeffs[1] + direction * coeffs[2] + direction * coeffs[3]

    def get_enclosing_keyframes(self, time: float) -> Tuple[Optional[Tuple[float, Any]], Optional[Tuple[float, Any]]]:
        """
        Get the previous and next keyframes, skipping the keyframe we might be spot on
        :param time: float - Time
        :return: Tuple[Optional[Tuple[float, Any]], Optional[Tuple[float, Any]]] - Returned keyframes
        """
        position = bisect_left([keyframe[0] for keyframe in self._keyframes], time)
        if position == 0:
            previous_frame = None
        else:
            previous_frame = self._keyframes[position - 1]

        if position == len(self._keyframes):
            next_frame = None
        else:
            next_frame = self._keyframes[position]
            if next_frame[0] == time:
                next_frame = self._keyframes[position + 1] if len(self._keyframes) > position + 1 else None

        return (previous_frame, next_frame)

    def removed_from_model(self, model: 'Model') -> None:
        super().removed_from_model(model)
        model.remove_animation_curve(self)

    def remove_keyframe(self, time: float) -> bool:
        """
        Remove a keyframe given its time position
        :param time: Time position
        :return bool - Return True if the keyframe was present and removed, False otherwise
        """
        keyframes = [keyframe for keyframe in self._keyframes if keyframe[0] != time]
        success = len(self._keyframes) != len(keyframes)
        self.keyframes = keyframes
        return success

    def _copy_shared(self, graph_type: Type['AnimationCurve'], *args: Any, **kwargs: Any) -> 'AnimationCurve':
        return super()._copy_shared(
            graph_type,
            *args,
            interpolation=self._interpolation,
            keyframes=self._keyframes,
            **kwargs
        )
