from typing import Optional, TYPE_CHECKING

from eis.command import CommandId, EISClientCommand
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satnet.command import command

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession


@command(id=CommandId.MOVE)
class MoveCommand(EISClientCommand):
    _fields = ['location', 'rotation']

    def __init__(self, location: Optional[Vector3] = None, rotation: Optional[Quaternion] = None) -> None:
        super().__init__()
        self.location = location
        self.rotation = rotation

    def handle(self, session: 'EISRemoteSession') -> None:
        if self.location is not None:
            session.location = self.location
        if self.rotation is not None:
            session.rotation = self.rotation
