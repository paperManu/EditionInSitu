import logging
from typing import Optional, TYPE_CHECKING

from eis.command import CommandId, EISClientCommand
from satnet.command import command

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession
    from eis.session import EISSession

logger = logging.getLogger(__name__)


@command(id=CommandId.START_TIMELINE)
class StartTimelineCommand(EISClientCommand):

    _fields = ['_reverse', '_until']

    def __init__(
        self,
        reverse: Optional[bool] = None,
        until: Optional[float] = None
    ) -> None:
        super().__init__()
        self._reverse = reverse if reverse is not None else False
        self._until = until

    def handle(self, session: 'EISSession') -> None:
        session.editor.timeline.start(self._reverse, self._until)


@command(id=CommandId.STOP_TIMELINE)
class StopTimelineCommand(EISClientCommand):
    def __init__(self) -> None:
        super().__init__()

    def handle(self, session: 'EISSession') -> None:
        session.editor.timeline.stop()


@command(id=CommandId.RESET_TIMELINE)
class ResetTimelineCommand(EISClientCommand):
    _fields = ['timeline_position']

    def __init__(
            self,
            timeline_position: Optional[float] = None
    ) -> None:
        super().__init__()
        self.timeline_position = timeline_position if timeline_position is not None else 0.0

    def handle(self, session: 'EISSession') -> None:
        if self.timeline_position is None:
            logger.warning('No timeline position provided')
            return
        session.editor.timeline.reset(self.timeline_position)


@command(id=CommandId.NEXT_KEYFRAME)
class NextKeyframeCommand(EISClientCommand):
    def __init__(self) -> None:
        super().__init__()

    def handle(self, session: 'EISRemoteSession') -> None:
        session.editor.timeline.next_keyframe()


@command(id=CommandId.PREVIOUS_KEYFRAME)
class PreviousKeyframeCommand(EISClientCommand):
    def __init__(self) -> None:
        super().__init__()

    def handle(self, session: 'EISRemoteSession') -> None:
        session.editor.timeline.previous_keyframe()
