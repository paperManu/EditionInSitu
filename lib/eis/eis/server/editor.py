import logging
from typing import Any, Dict, Optional

from eis.editor import Editor, EditorDelegate
from eis.engine import Engine
from eis.graph.object_3d import Object3D
from eis.server.user import RemoteEISUser

logger = logging.getLogger(__name__)


class ServerEditorDelegate(EditorDelegate[RemoteEISUser]):
    pass


class ServerEditor(Editor[RemoteEISUser, ServerEditorDelegate]):
    """
    EIS server editor
    Place server-specific editor code or overrides here.
    """

    def __init__(self, engine: Optional[Engine] = None, delegate: Optional[ServerEditorDelegate] = None, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(engine=engine, delegate=delegate, config=config)
        self.initialize_layers()

    def initialize_layers(self) -> None:
        # User objects assets layer, added after loading a new scene
        self._added_objects: Optional[Object3D] = None
        for child in self._scene.model.root.children:
            if child.name == "Added_objects_layer":
                self._added_objects = child
        if self._added_objects is None:
            self._added_objects = Object3D(name="Added_objects_layer")
            self._scene.model.root.add_child(self._added_objects)
