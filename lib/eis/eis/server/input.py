from typing import Optional

from eis.input import InputMethod, PeerInputMethod


class RemoteInputMethod(InputMethod):
    """
    Remote Input Method Base
    This is used to represent an input method on the server side when syncing
    to other clients or implementing things like picking on the server-side.
    """

    def __init__(self) -> None:
        super().__init__()

    def to_peer(self) -> Optional[PeerInputMethod]:
        """
        Return the Peer version of this input method
        that can be used to send to other clients.
        :return: Optional[PeerInputMethod]
        """
        return None
