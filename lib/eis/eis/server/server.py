import re
from typing import Any, Dict, Optional, TYPE_CHECKING

from eis.commands.engine import AddAnimationCurveCommand, RemoveAnimationCurveCommand, AddBehaviorCommand, AddEventHandlerCommand, AddMaterialCommand, AddMeshCommand, AddObject3DCommand, AddTextureCommand, PropertyChangedCommand, RemoveBehaviorCommand, \
    RemoveEventHandlerCommand, RemoveMaterialCommand, RemoveMeshCommand, RemoveObject3DCommand, RemoveTextureCommand
from eis.engine import EngineDelegate
from eis.entity import Sync, SyncEntity
from eis.graph.behavior import Behavior
from eis.graph.event_handler import EventHandler
from eis.graph.object_3d import Object3D
from eis.notifications.session import UserAdded, UserRemoved
from eis.requests.load_scene import LoadScene
from eis.server.editor import ServerEditor, ServerEditorDelegate
from eis.server.session import EISRemoteSession
from eis.server.user import PeerUser, RemoteEISUser
from satnet.server import RemoteSession, Server, ServerAdapter, SessionAdapter

if TYPE_CHECKING:
    from eis.graph.animation_curve import AnimationCurve
    from eis.graph.geometry import Geometry
    from eis.graph.material import Material
    from eis.graph.mesh import Mesh
    from eis.graph.model import Model
    from eis.graph.object_3d import Object3D
    from eis.graph.scene import Scene
    from eis.graph.texture import Texture


class EISServer(Server[EISRemoteSession], ServerEditorDelegate, EngineDelegate):
    """
    EIS server class.
    Place eis-specific editor code or overrides here.
    """

    def __init__(self, editor: ServerEditor, adapter: ServerAdapter, config: Dict[str, Any] = None, project: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config, adapter=adapter, session_factory=self._eis_session_factory)
        self._editor = editor
        self._project = project

        # Go through all objects of the editor's scene to set the behaviors
        def initialize_object_behaviors(object: 'Object3D') -> None:
            self.set_object_behaviors(model=self._editor.scene.model, object=object)
            for child in object.children:
                initialize_object_behaviors(child)

        initialize_object_behaviors(self._editor.scene.model.root)

    def _eis_session_factory(self, client: SessionAdapter) -> RemoteSession:
        """
        Factory for the creation of eis remote sessions.
        Injects the server and editor into the created session.
        :param client: SessionAdapter
        :return: RemoteSession
        """
        return EISRemoteSession(
            id=client.id,
            server=self,
            editor=self._editor
        )

    # region Editor Delegate Handlers

    def on_scene_loaded(self, scene: 'Scene') -> None:
        self.request_all(lambda client: LoadScene(scene=scene))

    # def on_model_loaded(self, model: 'Model') -> None:
    #     self.command_all(AddModelCommand(model=model))

    def on_user_added(self, user: RemoteEISUser) -> None:
        user.session.notify_others(UserAdded(peer_id=user.session.id, user=PeerUser(user=user)))

    def on_user_removed(self, user: RemoteEISUser) -> None:
        user.session.notify_others(UserRemoved(user=user))

    def on_property_changed(self, entity: SyncEntity, sync: Sync) -> None:
        self.command_all(PropertyChangedCommand(entity=entity, sync=sync))

    # endregion

    # region Engine/Model Delegate Handlers

    def set_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        if self._project is None or 'behaviors' not in self._project:
            return

        # This is here specifically to manage the fringe case of the Blender bam exporter
        # exporting objects in pair (the object and the mesh) with the same name.
        if object and len(object.children) == 1 and object.name == object.children[0].name and isinstance(object, Object3D):
            return

        def parse_event_handler(descr: Dict[str, Any]) -> Optional[EventHandler]:
            handler_name = descr.get('handler') or ''
            handler_type = EventHandler.event_handlers.get(handler_name)
            if not handler_type:
                return None

            trigger_behavior = descr.get('trigger_behavior') or ''
            trigger_kwargs = descr.get('trigger_kwargs')
            untrigger_behavior = descr.get('untrigger_behavior') or ''
            untrigger_kwargs = descr.get('untrigger_kwargs')
            return handler_type(
                trigger_behavior=trigger_behavior,
                trigger_kwargs=trigger_kwargs,
                untrigger_behavior=untrigger_behavior,
                untrigger_kwargs=untrigger_kwargs)

        behaviors = self._project.get('behaviors') or {}
        event_handlers = self._project.get('event_handlers') or {}

        # We do this to only parse the descriptions once
        global_handlers = dict()
        for behavior_name, descr in event_handlers.items():
            global_handlers[behavior_name] = dict()
            for handler_key in descr:
                global_handlers[behavior_name][handler_key] = parse_event_handler(descr=descr[handler_key])

        for object_name in behaviors:
            if re.search(object_name, object.name) is None:
                continue

            for behavior_name in behaviors[object_name]:
                if behavior_name not in Behavior.behaviors:
                    continue

                kwargs = behaviors[object_name][behavior_name].get("kwargs") or {}
                behavior = Behavior.behaviors[behavior_name](**kwargs)

                if not behaviors[object_name][behavior_name].get("client_sync"):
                    behavior.managed = True

                if behavior_name in event_handlers:
                    for handler_key in event_handlers[behavior_name]:
                        handler = global_handlers[behavior_name][handler_key]
                        if handler:
                            handler.parent = behavior
                            behavior.event_handlers[handler_key] = handler

                # We overload with the specific event handler if there is one
                local_event_handlers = behaviors[object_name][behavior_name].get("event_handlers") or {}
                for handler_key, descr in local_event_handlers.items():
                    handler = parse_event_handler(descr=descr)
                    if handler:
                        handler.parent = behavior
                        behavior.event_handlers[handler_key] = handler

                object.add_behavior(behavior)

    def unset_object_behaviors(self, model: 'Model', object: 'Object3D') -> None:
        if self._project is None or 'behaviors' not in self._project:
            return
        behaviors = self._project['behaviors']
        for object_name in behaviors:
            match = re.search(object_name, object.name)
            if match is not None:
                for behavior_name in behaviors[object_name]:
                    if behavior_name in Behavior.behaviors:
                        behavior_type = Behavior.behaviors[behavior_name]
                        object.remove_behavior_by_type(behavior_type)

    def on_object3d_added(self, model: 'Model', object: 'Object3D') -> None:
        if not object.managed:
            self.command_all(AddObject3DCommand(object=object))

    def on_object3d_removed(self, model: 'Model', object: 'Object3D') -> None:
        if not object.managed:
            self.command_all(RemoveObject3DCommand(object=object))

    def on_animation_curve_added(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        if not animation_curve.managed:
            self.command_all(AddAnimationCurveCommand(animation_curve=animation_curve))

    def on_animation_curve_removed(self, model: 'Model', animation_curve: 'AnimationCurve') -> None:
        if not animation_curve.managed:
            self.command_all(RemoveAnimationCurveCommand(animation_curve=animation_curve))

    def on_behavior_added(self, model: 'Model', behavior: 'Behavior') -> None:
        if not behavior.managed:
            self.command_all(AddBehaviorCommand(behavior=behavior))

    def on_behavior_removed(self, model: 'Model', behavior: 'Behavior') -> None:
        if not behavior.managed:
            self.command_all(RemoveBehaviorCommand(behavior=behavior))

    def on_event_handler_added(self, model: 'Model', behavior: 'Behavior', key: str, event_handler: 'EventHandler') -> None:
        if not behavior.managed:
            self.command_all(AddEventHandlerCommand(behavior=behavior, key=key, event_handler=event_handler))

    def on_event_handler_removed(self, model: 'Model', behavior: 'Behavior', key: str) -> None:
        if not behavior.managed:
            self.command_all(RemoveEventHandlerCommand(behavior=behavior, key=key))

    def on_geometry_added(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    def on_geometry_removed(self, model: 'Model', geometry: 'Geometry') -> None:
        pass

    def on_material_added(self, model: 'Model', material: 'Material') -> None:
        if not material.managed:
            self.command_all(AddMaterialCommand(material=material))

    def on_material_removed(self, model: 'Model', material: 'Material') -> None:
        if not material.managed:
            self.command_all(RemoveMaterialCommand(material=material))

    def on_texture_added(self, model: 'Model', texture: 'Texture') -> None:
        if not texture.managed:
            self.command_all(AddTextureCommand(texture=texture))

    def on_texture_removed(self, model: 'Model', texture: 'Texture') -> None:
        if not texture.managed:
            self.command_all(RemoveTextureCommand(texture=texture))

    def on_mesh_added(self, model: 'Model', mesh: 'Mesh') -> None:
        if not mesh.managed:
            self.command_all(AddMeshCommand(mesh=mesh))

    def on_mesh_removed(self, model: 'Model', mesh: 'Mesh') -> None:
        if not mesh.managed:
            self.command_all(RemoveMeshCommand(mesh=mesh))

    def on_sync_changed(self, entity: SyncEntity, sync: Sync, value: Any) -> None:
        """
        Here we effectively convert a sync change to a property change.
        We drop the value, since it'll be read later when when needed.

        :param entity: SyncEntity
        :param sync: Sync
        :param value: Any
        :return: None
        """
        self._editor.property_changed(entity=entity, sync=sync)

    # endregion
    ...
