import logging
from abc import ABCMeta
from typing import Dict, Generic, TypeVar, Callable, Any, TYPE_CHECKING
from uuid import UUID

from eis.constants import CLIENT, SERVER
from satlib.categories import Disposable
from satlib.language import fancy_namer
from satlib.tasks import TaskManager
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3
from satnet.base import NetBase
from satnet.entity import Entity
from satnet.session import Session

if TYPE_CHECKING:
    # noinspection PyUnresolvedReferences
    from eis.editor import Editor

    if CLIENT:
        from eis.client.user import PeerUser
    elif SERVER:
        from eis.server.user import PeerUser

logger = logging.getLogger(__name__)

E = TypeVar('E', bound='Editor')


class EISSession(Generic[E], Session, metaclass=ABCMeta):
    """
    Base class for sessions.
    Sessions are a requirement of the satnet library and represent a connection
    (local or remote) to a server.
    """

    # noinspection PyMissingConstructor
    def __init__(self, net: NetBase, editor: E) -> None:
        # NOTE: We don't call `super().__init__()` here because we're in a multiple inheritance diamond ;)
        # super().__init__(net=net)

        self._task_manager = TaskManager()

        self._editor = editor
        self._nickname = fancy_namer.generate_name()

    def __str__(self) -> str:
        return "\"{}\"".format(self._nickname)

    # region Properties

    @property
    def editor(self) -> E:
        return self._editor

    @property
    def nickname(self) -> str:
        return self._nickname

    # endregion

    # region Lifecycle

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

    # endregion

    # region Callbacks

    def ready(self) -> None:
        """
        Called when the session is ready
        :return: None
        """

        logger.info("Session {} ready!".format(self))

    # endregion
    ...


class PeerBase(Entity, Disposable):
    """
    This is the base class for Peers.
    Peers are used on the client side to represent the other sessions
    of the application. They are created from a remote session but don't
    have the server logic, only what is needed to interact with them
    from another client.
    """
    _fields = ['_id', '_nickname', '_location', '_rotation']

    def __init__(self) -> None:
        super().__init__()
        self._id = -1
        self._nickname = ""
        self._location = Vector3()
        self._rotation = Quaternion.identity()
        self._users = {}  # type: Dict[UUID, 'PeerUser']

    def initialize(self) -> None:
        super().initialize()
        for user in self._users.values():
            user.peer = self

    def ready(self) -> None:
        for user in self._users.values():
            user.ready()

    def dispose(self) -> None:
        for user in self.users.values():
            user.dispose()
        self._users = {}

    # region Properties

    @property
    def id(self) -> int:
        return self._id

    @property
    def nickname(self) -> str:
        return self._nickname

    @property
    def users(self) -> Dict[UUID, 'PeerUser']:
        return self._users

    @property
    def location(self) -> Vector3:
        return self._location

    @location.setter
    def location(self, value: Vector3) -> None:
        if self._location != value:
            self._location = value

    @property
    def rotation(self) -> Quaternion:
        return self._rotation

    @rotation.setter
    def rotation(self, value: Quaternion) -> None:
        if self._rotation != value:
            self._rotation = value

    # endregion

    # region Lifecycle

    def step(self, now: float, dt: float) -> None:
        for user in self._users.values():
            user.step(now=now, dt=dt)

    # endregion

    # region Serialization

    def pack(self, write: Callable[[Any], None]) -> None:
        # Manually pack since we don't want to pack the dict keys
        write([user for user in self._users.values()])

    def unpack(self, read: Callable[[], Any]) -> None:
        # Manually pack since we don't want to pack the dict keys
        self._users = {user.uuid: user for user in read()}

    # endregion
    ...
