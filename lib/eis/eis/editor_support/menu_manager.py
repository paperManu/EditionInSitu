import math

from typing import Callable, Dict, Optional, TYPE_CHECKING
from uuid import UUID

from eis.client.input import LocalInputMethod
from eis.client.user import LocalEISUser
from eis.display.components.menus.menu import Menu
from satmath.euler import Euler
from satmath.matrix44 import Matrix44

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.graph.object_3d import Object3D


class UserMenuData:
    def __init__(self, user: LocalEISUser):
        self.user = user
        self.menu: Optional[Menu] = None
        self.next_menu: Optional[Menu] = None
        self.matrix = Matrix44.identity()


class MenuManager:

    def __init__(self, editor: 'ClientEditor', container: 'Object3D'):
        self._editor = editor
        self._container = container
        self._is_submenu = True
        self._user_menu_data: Dict[UUID, UserMenuData] = {}
        self._menu_open = False

    @property
    def editor(self) -> 'ClientEditor':
        return self._editor

    @property
    def menu_open(self) -> bool:
        return self._menu_open

    def _get_user_menu_data(self, user: LocalEISUser) -> UserMenuData:
        user_menu_data = self._user_menu_data.get(user.uuid)
        if not user_menu_data:
            user_menu_data = UserMenuData(user=user)
            self._user_menu_data[user.uuid] = user_menu_data
        return user_menu_data

    def remove_user(self, user: LocalEISUser) -> None:
        # TODO: Close opened menus
        del self._user_menu_data[user.uuid]

    def open_menu(self, input: LocalInputMethod, menu: Menu, is_submenu: bool = False) -> None:
        assert(input.user is not None)
        user_menu_data = self._get_user_menu_data(input.user)

        # Queue menu for user
        menu.user = input.user
        if not self._is_submenu:
            user_menu_data.matrix = input.picker.cursor_matrix
        user_menu_data.next_menu = menu

        if user_menu_data.menu:
            # If a menu is already opened, close it first
            self.close_menu(input.user, lambda: self._open_menu(input, user_menu_data))
        else:
            self._open_menu(input, user_menu_data)

    def _open_menu(self, input: LocalInputMethod, user_menu_data: UserMenuData) -> None:
        if not user_menu_data.next_menu:
            return

        menu = user_menu_data.next_menu
        user_menu_data.menu = menu
        user_menu_data.next_menu = None
        menu.scale(self._editor.config.get('menu.scale') or 1.0)

        menu._manager = self
        menu._state = self._editor.machine.state_object

        menu.on_opening(input)
        # the rotation by pi/2.0 is to flip the 2D menu in front of the user
        menu.matrix_world = user_menu_data.matrix * Matrix44.from_euler(Euler((math.pi / 2.0, 0.0, 0.0)))
        self._container.add_child(menu)
        menu.on_opened(input)

        # NOTE: Not ideal to reference the state from here...
        if self._editor.machine.state_object is not None:
            self._editor.machine.state_object.on_menu_opened(menu)
        self._editor.cursor_manager.on_menu_opened(menu)

    def close_menu(self, user: Optional[LocalEISUser] = None, callback: Optional[Callable[[], None]] = None) -> None:
        # TODO: Handle this better than closing all menus when no input is available

        def close_and_callback(user_menu_data: UserMenuData):
            if not user_menu_data.menu:
                # No menu is opened, execute callback directly
                if callback:
                    callback()
                return
            user_menu_data.menu.close_callback = callback
            user_menu_data.menu.on_closing(user_menu_data, self._close_menu)
        if user:
            close_and_callback(self._get_user_menu_data(user))
        else:
            for user_menu_data in self._user_menu_data.values():
                close_and_callback(user_menu_data)

    def _close_menu(self, user_menu_data: UserMenuData) -> None:
        if user_menu_data.menu:
            user_menu_data.menu.on_closed()
            self._container.remove_child(user_menu_data.menu)

        # XXX: Not ideal to reference the state from here...
        if self._editor.machine.state_object is not None:
            self._editor.machine.state_object.on_menu_closed()
        self._editor.cursor_manager.on_menu_closed()

        if user_menu_data.menu:
            # Keep a reference to the menu,
            # since we might open a new one in the close callback
            previous_menu = user_menu_data.menu

            if user_menu_data.menu.close_callback:
                user_menu_data.menu.close_callback()

            previous_menu.dispose()
            if user_menu_data.menu == previous_menu:
                # If we still have the same menu in user_menu_data
                # it probably means that no new menu was opened so clear it
                user_menu_data.menu = None

    def toggle_menu(self, input: LocalInputMethod, down: bool, is_submenu: bool = True):
        assert(input.user is not None)
        self._is_submenu = is_submenu
        user_menu_data = self._get_user_menu_data(input.user)

        if down and not user_menu_data.menu:
            # Open Menu
            state = self._editor.machine.state_object
            if state is not None and state.menu is not None:
                # Open on press
                # Some actions will need to know where we clicked to open the menu
                self.open_menu(input, state.menu())
                assert(user_menu_data.menu is not None)
                user_menu_data.menu.just_opened = True

        else:
            # Close Menu
            # TODO: Close on release
            if user_menu_data.menu and user_menu_data.menu.just_opened:
                # We just opened the menu, so allow releasing the button without closing it right away
                user_menu_data.menu.on_quick_action(input)
            else:
                self.close_menu(input.user)

    def update(self) -> None:
        """
        Update the menu manager
        :return:
        """

        for user_menu_data in self._user_menu_data.values():
            if user_menu_data.menu is not None:
                user_menu_data.menu.update_menu_components()
                self._menu_open = True
            else:
                self._menu_open = False
