import atexit
import logging
from typing import Any, Dict, Optional

from eis.singleton import Singleton
from pysatie.satie import Satie
from satmath.matrix44 import Matrix44

logger = logging.getLogger(__name__)


class SatieManager(metaclass=Singleton):
    def __init__(self, config: Dict[str, Any]) -> None:
        self._config = config

        self._server = self._config.get("satie.server", "localhost")
        self._destination_port = self._config.get("satie.destination_port", 18032)
        self._server_port = self._config.get("satie.server_port", 12000)
        self._renderer_orientation_offset = tuple(
            [i for i in self._config.get("satie.renderer_orientation_offset", [0, 0])])
        self._satie = Satie()
        self.satie.server_port = self._server_port
        atexit.register(self.clear_scene)

        self._satie.destination = self._server
        self._satie.destination_port = self._destination_port
        self._satie.initialize()

        # tell SATIE to send to us
        self._satie.set_renderer_orientation(*self._renderer_orientation_offset)
        self._satie.audiosources()  # get a list of available plugins

        self._last_editor_matrix = Matrix44.identity()

        self._sources: Dict[str, Matrix44] = {}

    def clear_scene(self) -> None:
        self._satie.clear_scene()

    def update_editor_matrix(self, matrix: Matrix44) -> None:
        self._last_editor_matrix = matrix
        for uuid in self._sources:
            self.set_source_location(uuid=uuid, matrix=self._sources[uuid])

    def set_source_location(self, uuid: str, matrix: Matrix44) -> None:
        target = self._satie.nodes.get(uuid)

        if not target:
            logger.warning(f"Could not find a SATIE source with uuid: {uuid}")
            return

        translation_vector = matrix.translation - self._last_editor_matrix.translation

        # This is temporary, divide the translation by the average scale of the object so that the bigger the scale, the closer we hear it
        # TODO: Update this so that the scale affects the volume rather than the distance. See EIS-398
        scale = matrix.scale
        translation_vector /= (scale[0] + scale[1] + scale[2]) / 3.0
        target.update(self._last_editor_matrix.quaternion.mul_vector3(translation_vector))

    def new_source(self, uuid: str, plugin: str, group: Optional[str], matrix: Matrix44, audio_handle: Optional[int] = None) -> None:
        audio_generator = self._satie.get_plugin_by_name(plugin)
        if audio_generator is None:
            logger.warning(f"Could not find SATIE plugin: {plugin}")
            return

        self._satie.add_source_node(uuid, audio_generator, group)
        if audio_handle is not None:
            self._satie.node_set(uuid, "bus", audio_handle - 1)
        self._sources[uuid] = matrix
        self.set_source_location(uuid, matrix)

    def remove_source(self, uuid) -> None:
        self._satie.delete_node(node_name=uuid)
        self._sources.pop(uuid)

    @property
    def satie(self) -> Satie:
        return self._satie
