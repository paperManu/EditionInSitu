import logging
import os

from typing import Any, Dict, List, Optional
from watchdog.events import FileSystemEvent, PatternMatchingEventHandler  # type: ignore
from watchdog.observers import Observer  # type: ignore

from eis import BASE_PATH
from eis.assets.file_asset import FileAsset as Asset
from eis.editor_support.asset_manager import AssetManager

logger = logging.getLogger(__name__)


class WatcherEventHandler(PatternMatchingEventHandler):
    """
    Callback for file events watchdog
    """

    def __init__(self, manager: 'FileAssetManager') -> None:
        super().__init__()
        self._manager = manager

    def _process(self, event: FileSystemEvent) -> None:
        if event.is_directory:  # Ignore directory changes, only watch files
            return

        file_asset = {'event': event.event_type}
        if event.event_type == 'moved':
            file_asset['old_path'] = event.src_path
            file_asset['path'] = event.dest_path
        else:
            file_asset['path'] = event.src_path
        with self._manager.lock:
            self._manager._changed_files.append(file_asset)

    def on_deleted(self, event: FileSystemEvent) -> None:
        self._process(event)

    def on_moved(self, event: FileSystemEvent) -> None:
        self._process(event)

    def on_modified(self, event: FileSystemEvent) -> None:
        self._process(event)


class FileAssetManager(AssetManager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._asset_path = self._library.editor.config.get("assets.assets_path")

        self._asset: Optional[Asset] = None
        self._observers: List[Observer] = []

        # Check whether the path starts with "/" or not to determine whether it is a real path or a relative path
        if self._asset_path.startswith("/"):
            self._asset_path = os.path.normpath(self._asset_path)
        elif self._asset_path.startswith("~"):
            self._asset_path = os.path.normpath(os.path.expanduser(self._asset_path))
        else:
            self._asset_path = os.path.join(BASE_PATH, self._asset_path)

        self._starting_asset_path = self._asset_path

        if os.path.exists(self._asset_path):
            Asset.init_plugins()

    @property
    def asset_path(self) -> List[str]:
        return self._asset_path

    @asset_path.setter
    def asset_path(self, path: List[str]) -> None:
        self._asset_path = os.path.normpath(path)
        self.deactivate()
        self.activate()

    def activate(self) -> None:
        # Do one pass on the folder because the watchdog only tracks events, and does not detect existing files
        changed_non_folder_files = []
        changed_folder_files = []

        # Add a "back" folder asset if this isn't the root folder
        if self._asset_path != self._starting_asset_path:
            changed_folder_files.append({'event': 'modified', 'path': os.path.join(self._asset_path, "..")})

        # Add assets separately for directories and files to have them in order in the final list (folders before files)
        for file_name in os.listdir(self._asset_path):
            if not file_name.startswith("."):
                complete_path = os.path.join(self._asset_path, file_name)
                if os.path.isdir(complete_path):
                    changed_folder_files.append(
                        {'event': 'modified', 'path': os.path.join(self._asset_path, file_name)})
                else:
                    changed_non_folder_files.append(
                        {'event': 'modified', 'path': os.path.join(self._asset_path, file_name)})

        self._changed_files = changed_folder_files + changed_non_folder_files

        observer = Observer()
        observer.schedule(WatcherEventHandler(self), path=self._asset_path, recursive=False)
        observer.start()
        self._observers.append(observer)

        self._active = True

    def step(self, now: float, dt: float) -> None:
        if not self._active:
            return

        with self._lock:
            changed_files = list(self._changed_files)

        # Step in assets
        for asset in self._library._assets.values():
            asset.step(now=now, dt=dt)

        for f in changed_files:
            event = f['event']
            path = f['path']

            if event == 'modified':
                try:
                    asset = Asset.factory_create_asset(path)
                    if not asset:
                        logger.warning("Could not find an asset type matching the extension of file {}".format(path))
                        continue
                except FileNotFoundError:
                    self._asset_lib.warning("Could not find asset at path: {}".format(path))
                else:
                    self._library.instantiate(asset)
            elif event == 'deleted':
                try:
                    asset = self._library._assets.pop(path)
                    self._unlink_asset(asset)
                except KeyError:
                    logger.warning("Cannot find asset at path {} to delete".format(path))
            elif event == 'moved':
                old_path = f['old_path']
                asset = self._library._assets.get(old_path)
                if asset:
                    self._library._assets[path] = self._library._assets.pop(old_path)

        with self._lock:
            for f in changed_files:
                self._changed_files.remove(f)

    def deactivate(self) -> None:
        self._active = False
        for observer in self._observers:
            observer.stop()
        self._observers = []
        self._changed_files = []
