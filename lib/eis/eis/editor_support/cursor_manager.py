from typing import TYPE_CHECKING, Type, Optional, Callable
from eis.display.components.cursor import Cursor
from eis.client.input import LocalInputMethod
from eis.display.components.menus.menu import Menu

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.graph.object_3d import Object3D


class CursorManager:

    def __init__(self, editor: 'ClientEditor', container: 'Object3D'):
        self._editor = editor
        self._container = container
        self._cursor = None  # type: Optional[Callable[[], Cursor]]

    @property
    def editor(self) -> 'ClientEditor':
        return self._editor

    @property
    def cursor(self) -> Optional[Callable[[], Cursor]]:
        return self._cursor

    @cursor.setter
    def cursor(self, value: Optional[Callable[[], Cursor]]) -> None:
        if self._cursor != value:

            if self._cursor is not None:
                for user in self._editor.users.values():
                    for input in user.input_methods.values():
                        if input.cursor is not None:
                            self._container.remove_child(input.cursor)
                        input.cursor = None

            self._cursor = value

            if self._cursor is not None:
                for user in self._editor.users.values():
                    for input in user.input_methods.values():
                        input.cursor = self._cursor()
                        if input.cursor is not None:
                            self._container.add_child(input.cursor)
                            input.cursor.input_mapping = input.mapping_dict

    def on_menu_opened(self, menu: Menu) -> None:
        for input in menu.user.input_methods.values():
            if input.cursor:
                input.cursor.on_menu_opened(menu)

    def on_menu_closed(self, input: Optional[LocalInputMethod] = None) -> None:
        # TODO: Input is optional here only to get the menu feature out quicker

        if input:
            if input.cursor:
                input.cursor.on_menu_closed()
        else:
            for user in self._editor.users.values():
                for input in user.input_methods.values():
                    if input.cursor:
                        input.cursor.on_menu_closed()

    def update(self) -> None:
        """
        Update the cursor manager
        :return: None
        """

        # if self._cursor:
        #     self._container.location = self.editor.picker.cursor_matrix.to_translation()
        #     self._container.rotation = self.editor.picker.cursor_matrix.to_quaternion()
        #
        #     self._container.scale.x = self.editor.ui_scale
        #     self._container.scale_y = self.editor.ui_scale
        #     self._container.scale_z = self.editor.ui_scale
        #
        #     self._cursor.update_cursor()
