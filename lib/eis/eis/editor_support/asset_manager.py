import logging

from abc import ABCMeta, abstractmethod

from threading import Lock
from typing import Any, Dict, List, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from eis.editor_support.asset_library import AssetLibrary
logger = logging.getLogger(__name__)


class AssetManager(metaclass=ABCMeta):
    """
    EIS asset manager
    Provide assets to the AssetLibrary to display
    """

    def __init__(self, library: 'AssetLibrary') -> None:
        self._active = False
        self._library = library
        self._changed_files: List[Dict[str, Any]] = None
        self._lock = Lock()

    @abstractmethod
    def step(self, now: float, dt: float) -> None:
        pass

    @abstractmethod
    def activate(self) -> None:
        pass

    @abstractmethod
    def deactivate(self) -> None:
        pass
