from typing import Any, Dict, Optional, TYPE_CHECKING

from eis.actions.animation import AddKeyframeAction, RemoveKeyframeAction
from eis.actions.engine import AddModelAction, RemoveObject3DAction
from eis.commands.animation import ResetTimelineCommand, StartTimelineCommand
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.camera_presentation import Camera_Presentation
from eis.graph.object_3d import Object3D
from eis.graph.model import Model

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor


class PresentationManager:
    """
    Class burdened with handling the presentation mode.
    A slide represents a recorded editor pose.
    """

    PRESENTATION_PREFIX = "__presentation"
    DEFAULT_DURATION_BETWEEN_SLIDES = 1.0

    def __init__(
        self,
        editor: 'ClientEditor',
        config: Optional[Dict[str, Any]] = None
    ) -> None:
        self._editor = editor
        self._camera: Optional[Camera_Presentation] = None
        self._camera_name = f"{PresentationManager.PRESENTATION_PREFIX}_camera"
        self._config = config if config is not None else {}

        self._active = False  # True is the presentation mode is active
        self._current_slide_index = 0
        self._slide_count = 1

        self._replaced_camera = False

        self._duration_between_slides = self._config.get(
            'presentation.duration_between_slides', PresentationManager.DEFAULT_DURATION_BETWEEN_SLIDES)

    def _extend_timeline(self) -> None:
        """
        Extends the timeline if necessary
        """
        timeline_duration = self._editor.config.get('timeline.duration')
        if timeline_duration <= self._slide_count * self._duration_between_slides:
            # We keep a buffer after the last keyframe to prevent some undesired behaviors
            self._editor.config['timeline.duration'] = (self._slide_count + 1) * self._duration_between_slides

    def activate(self) -> None:
        """
        Activate the presentation mode
        This creates a presentation camera if it does not already exist
        """
        if self._camera is None:
            self._camera = self._editor.scene.model.root.find_by_name(self._camera_name)
            if self._camera is not None:
                behavior = self._camera.get_behavior_by_type(AnimationBehavior)
                if behavior is not None:
                    # Delete current keyframes
                    for curve in behavior.curves.values():
                        self._slide_count = len(curve.keyframes)
                        pass
            else:
                camera = Camera_Presentation(name=self._camera_name)
                camera.matrix = self._editor.matrix
                self._editor.machine.client.session.action(AddModelAction(model=Model(root=camera)))

        # # Get the current slide count
        # animation_behavior = self._camera.get_behavior_by_type(AnimationBehavior)
        # if animation_behavior is not None:
        #     last_timestamp = animation_behavior.last_curve_timestamp
        #     self._slide_count = round(last_timestamp / self._duration_between_slides) + 1

        self._active = True
        current_time = self._editor.timeline.time
        self._editor.timeline.time = current_time - current_time % self._duration_between_slides
        self._current_slide_index = round(self._editor.timeline.time / self._duration_between_slides)

    def deactivate(self) -> None:
        """
        Deactivates the presentation mode, and hides the presentation camera
        """
        self._editor.camera = None
        self._active = False

    def previous_slide(self) -> None:
        """
        Go to the previous slide
        """
        if self._current_slide_index == 0:
            return

        self._current_slide_index -= 1
        self._editor.machine.client.session.command(StartTimelineCommand(
            reverse=True,
            until=self._current_slide_index * self._duration_between_slides
        ))

    def next_slide(self) -> None:
        """
        Go to the next slide
        """
        if self._current_slide_index == self._slide_count - 1:
            self._slide_count += 1
            self._extend_timeline()

        self._current_slide_index += 1
        self._editor.machine.client.session.command(StartTimelineCommand(
            reverse=False,
            until=self._current_slide_index * self._duration_between_slides
        ))

    def first_slide(self) -> None:
        """
        Go to the first slide
        """
        self._current_slide_index = 0
        self._editor.machine.client.session.command(ResetTimelineCommand(
            timeline_position=self._current_slide_index * self._duration_between_slides
        ))

    def last_slide(self) -> None:
        """
        Go to the last slide
        """
        self._current_slide_index = self._slide_count - 1
        self._editor.machine.client.session.command(ResetTimelineCommand(
            timeline_position=self._current_slide_index * self._duration_between_slides
        ))

    def set_slide(self) -> None:
        """
        Record the current position as the current slide
        """
        assert(self._camera is not None)
        if self._editor.timeline.time % self._duration_between_slides != 0.0:
            return

        behavior = self._camera.get_behavior_by_type(AnimationBehavior)
        if behavior is not None:
            if behavior.selected_keyframe is not None:
                self._editor.machine.client.session.action(RemoveKeyframeAction(
                    object=self._camera, time=behavior.selected_keyframe))

        # TODO : replace the actions with commands
        self._editor.machine.client.session.action(AddKeyframeAction(
            object=self._camera,
            attribute="location",
            time=self._editor.timeline.time,
            value=self._camera.location
        ))

        self._editor.machine.client.session.action(AddKeyframeAction(
            object=self._camera,
            attribute="rotation",
            time=self._editor.timeline.time,
            value=self._camera.rotation
        ))

    def insert_slide(self) -> None:
        """
        Insert a slide at the current position, moving the current slide right after
        """
        assert(self._camera is not None)
        if self._editor.timeline.time % self._duration_between_slides != 0.0:
            return

        behavior = self._camera.get_behavior_by_type(AnimationBehavior)
        if behavior is None:
            return
        animation_curves = behavior.curves.copy()

        for slide_index in range(self._current_slide_index, self._slide_count):
            self._editor.machine.client.session.action(RemoveKeyframeAction(
                object=self._camera,
                time=slide_index * self._duration_between_slides
            ))

        self.set_slide()
        self._slide_count += 1
        self._extend_timeline()

        for attribute, curve in animation_curves.items():
            for slide_index in range(self._current_slide_index, self._slide_count - 1):
                keyframe_value = curve.get_value_at(slide_index * self._duration_between_slides)
                assert(keyframe_value is not None)

                self._editor.machine.client.session.action(AddKeyframeAction(
                    object=self._camera,
                    attribute=attribute,
                    time=(slide_index + 1) * self._duration_between_slides,
                    value=keyframe_value
                ))

    def remove_slide(self) -> None:
        """
        Remove the current slide
        """
        assert(self._camera is not None)

        if self._slide_count == 1:
            return

        behavior = self._camera.get_behavior_by_type(AnimationBehavior)
        if behavior is None:
            return
        animation_curves = behavior.curves.copy()

        for slide_index in range(self._current_slide_index, self._slide_count):
            self._editor.machine.client.session.action(RemoveKeyframeAction(
                object=self._camera,
                time=slide_index * self._duration_between_slides
            ))

        self._slide_count -= 1

        for slide_index in range(self._current_slide_index, self._slide_count):
            for attribute, curve in animation_curves.items():
                keyframe_value = curve.get_value_at((slide_index + 1) * self._duration_between_slides)
                assert(keyframe_value is not None)

                self._editor.machine.client.session.action(AddKeyframeAction(
                    object=self._camera,
                    attribute=attribute,
                    time=slide_index * self._duration_between_slides,
                    value=keyframe_value
                ))

    def step(self, now: float, dt: float) -> None:
        if not self._active:
            if not self._replaced_camera and self._camera is None:
                camera = self._editor.scene.model.root.find_by_name(self._camera_name)
                if camera is not None:
                    # Temporary fix, see EIS-404
                    self._editor.machine.client.session.action(AddModelAction(model=Model(root=camera.copy())))
                    self._editor.machine.client.session.action(RemoveObject3DAction(object=camera))
                    self._replaced_camera = True
            return

        if self._editor.camera is not self._camera:
            self._editor.camera = self._camera

        if self._camera is None:
            self._camera = self._editor.scene.model.root.find_by_name(self._camera_name)
            if self._camera is None:
                return
            behavior = self._camera.get_behavior_by_type(AnimationBehavior)
            if behavior is not None:
                # Delete current keyframes
                for curve in behavior.curves.values():
                    self._slide_count = len(curve.keyframes)
                    pass
