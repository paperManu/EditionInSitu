import logging
from typing import Dict, Generic, Optional, Tuple, TypeVar

from panda3d import core  # type: ignore

from eis.engine import Engine, proxy
from eis.engines.panda3d.engine import Panda3DEngine
from eis.engines.panda3d.proxies.base import Panda3DGraphProxyBase
from eis.engines.panda3d.proxies.material import Panda3DMaterialProxy
from eis.engines.panda3d.proxies.mesh import Panda3DMeshProxy
from eis.engines.panda3d.proxies.texture import Panda3DTextureProxy
from eis.graph.mesh import Mesh
from eis.graph.light import Light
from eis.graph.object_3d import Object3D, Object3DProxy
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)

O = TypeVar('O', bound='Object3D')
N = TypeVar('N', bound=core.PandaNode)


@proxy(Panda3DEngine, Object3D)
class Panda3DObject3DProxy(Generic[O, N], Object3DProxy[O], Panda3DGraphProxyBase[N]):

    culling_conversion_map: Dict[Mesh.Culling, core.CullFaceAttrib] = {
        Mesh.Culling.NONE: core.CullFaceAttrib.MCullNone,
        Mesh.Culling.FRONT: core.CullFaceAttrib.MCullClockwise,
        Mesh.Culling.BACK: core.CullFaceAttrib.MCullCounterClockwise,
    }

    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, Panda3DEngine)
        Object3DProxy.__init__(self, engine=engine, proxied=proxied)
        Panda3DGraphProxyBase.__init__(self, engine=engine)
        self._node_path: Optional[core.NodePath] = None
        self._last_node: Optional[core.PandaNode] = self._node

    @property
    def bound_box(self) -> Tuple[Vector3, Vector3]:
        if self._node_path:
            min_point = core.Point3()
            max_point = core.Point3()
            self.node_path.calc_tight_bounds(min_point, max_point)
            return (Vector3(min_point), Vector3(max_point))
        else:
            return (Vector3(), Vector3())

    @property
    def node_path(self) -> Optional[core.NodePath]:
        return self._node_path

    def _make_node_path(self) -> None:
        if not self._node_path:
            proxied = self._proxied
            self._node_path = core.NodePath(self._node) if self._node else core.NodePath(proxied.name)
            self._node_path.set_tag("uuid", str(proxied.uuid))
            self._node_path.set_mat(core.Mat4(*proxied.matrix.flat))
            self._last_node = self._node

    def update_graph(self) -> None:
        """
        Called when the scene graph from this object has been updated.
        """
        self.make()
        super().update_graph()
        proxied_parent = self.proxied.parent
        if proxied_parent and proxied_parent.proxy:
            self.update()
            self._node_path.reparent_to(proxied_parent.proxy.node_path)

    def make(self) -> None:
        """
        Make the proxy
        :return: None
        """

        if self._proxied.mesh is not None:
            # NOTE: We can't share Mesh/GeomNode in Panda3D
            # Make sure the proxy is up to date
            self._proxied.mesh.update_proxy(self._engine)
            assert isinstance(self._proxied.mesh.proxy, Panda3DMeshProxy)
            self._node = self._proxied.mesh.proxy.geom_node

        super().make()
        self._make_node_path()

        # Set the default shader for any p3d node
        self._panda3d.prepare_default(self._node_path)

        if not self._node_path:
            logger.error("No node path in update")
            return

        proxied = self._proxied
        if proxied.mesh:
            culling = proxied.mesh.culling if proxied.mesh.culling != Mesh.Culling.DEFAULT else self._engine.default_culling
            assert(culling in Panda3DObject3DProxy.culling_conversion_map)
            self._node_path.setAttrib(core.CullFaceAttrib.make(Panda3DObject3DProxy.culling_conversion_map[culling]))

        if proxied.material:
            proxied.material.update_proxy(self._engine)
            assert isinstance(proxied.material.proxy, Panda3DMaterialProxy)

            for material_texture in proxied.material.material_textures:
                texture = material_texture.texture
                if not texture:
                    logger.warning("Material texture \"{}\" for material has no texture".format(material_texture.name))
                    continue

                texture.update_proxy(self._engine)
                self._node_path.set_texture(texture.proxy.texture)
                assert isinstance(texture.proxy, Panda3DTextureProxy)

            self._node_path.set_material(proxied.material.proxy.material)

        # Update children and attach them to our node path
        for child in proxied.children:
            child.update_proxy(engine=self._engine)
            if child.proxy:
                child.proxy.node_path.reparent_to(self._node_path)

        self._engine.prepare(self)

    def update(self) -> None:
        """
        Update the proxy
        Since it handles replacing the node path super().update() needs to be called
        after custom code.

        :return: None
        """

        # If the node has changed we need to replace the node path with a new one
        if self._node != self._last_node:
            parent = None
            if self._node_path:
                parent = self._node_path.get_parent()
                self._node_path.remove_node()

            self._make_node_path()

            if parent:
                # If we changed node path we have to reparent to the previous parent
                # since we're not going through the path of make() again here
                self._node_path.reparent_to(parent)

            if self._proxied.material:
                assert isinstance(self._proxied.material.proxy, Panda3DMaterialProxy)
                self._node_path.set_material(self._proxied.material.proxy.material)
                for material_texture in self._proxied.material.material_textures:
                    self._node_path.set_texture(material_texture.texture.proxy.texture)

            # Reattach children to the new node path
            for child in self._proxied.children:
                if child.proxy:
                    child.proxy.node_path.reparent_to(self._node_path)

            self._engine.prepare(self)

        assert(self._node_path)

        # Visibility
        if self._proxied.visible:
            self._node_path.show()
        else:
            self._node_path.hide()

        # Culling
        if self._proxied.mesh:
            self._node_path.clearAttrib(core.CullFaceAttrib)
            culling = self._proxied.mesh.culling if self._proxied.mesh.culling != Mesh.Culling.DEFAULT else self._engine.default_culling
            assert(culling in Panda3DObject3DProxy.culling_conversion_map)
            self._node_path.setAttrib(core.CullFaceAttrib.make(Panda3DObject3DProxy.culling_conversion_map[culling]))

        super().update()

    def add_child(self, child: O) -> None:
        if not child.proxy:
            logger.error("Trying to add a child ({}) without a proxy".format(child))
            return

        assert isinstance(child.proxy, Panda3DObject3DProxy)
        assert child.proxy.node_path
        child.proxy.node_path.reparent_to(self._node_path)

    def gui(self, gui: bool) -> None:
        # Only the top parent GUI (which should be the GUI layer) should be 'prepared'
        if gui and self._proxied.parent is not None and not self._proxied.parent.gui:
            self._panda3d.prepare_gui(self._node_path)
        else:
            self._panda3d.prepare_default(self._node_path)

    def remove_child(self, child: O) -> None:
        if child.proxy:
            assert isinstance(child.proxy, Panda3DObject3DProxy)
            assert child.proxy.node_path

            # Make sure to remove every child thoroughly, to clean up everything
            # Some proxies need to unregister from the root
            def remove_lights(obj: Object3D) -> None:
                for child in obj.children:
                    remove_lights(child)
                    if type(child) == Light:
                        if child.proxy is not None:
                            child.proxy.remove_from_root()

            remove_lights(child)
            child.proxy.node_path.detach_node()

    def remove(self) -> None:
        if self._node_path:
            self._node_path.detach_node()

    def dispose(self) -> None:
        if self._node_path:
            self._node_path.remove_node()

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        if self._node_path:
            self._node_path.set_mat(core.Mat4(*(matrix_offset * matrix).flat))
