from eis.graph.material import MaterialTexture

TEXTURE_SORT_TO_USAGE = {
    0: MaterialTexture.TextureUsage.BASE_COLOR,
    10: MaterialTexture.TextureUsage.NORMAL,
    20: MaterialTexture.TextureUsage.SPECULAR,
    30: MaterialTexture.TextureUsage.ROUGHNESS
}

TEXTURE_USAGE_TO_SORT = {
    MaterialTexture.TextureUsage.BASE_COLOR: 0,
    MaterialTexture.TextureUsage.NORMAL: 10,
    MaterialTexture.TextureUsage.SPECULAR: 20,
    MaterialTexture.TextureUsage.ROUGHNESS: 30
}
