import logging
from typing import Any, TypeVar

from eis.editor_support.osc_manager import OscManager
from eis.engine import Engine, proxy
from eis.engines.sound.proxies.sound_object import SoundEngineSoundObjectProxy
from eis.engines.sound.varays_engine.engine import VaraysEngine
from eis.graph.sound_objects.video_sound_object import VideoSoundObject
from satmath.matrix44 import Matrix44
from satmath.vector3 import Vector3

O = TypeVar('O', bound='VaraysVideoObject')

logger = logging.getLogger(__name__)


@proxy(VaraysEngine, VideoSoundObject)
class VaraysEngineVideoObjectProxy(SoundEngineSoundObjectProxy):
    def __init__(self, engine: Engine, proxied: O) -> None:
        assert isinstance(engine, VaraysEngine)
        SoundEngineSoundObjectProxy.__init__(self, engine=engine, proxied=proxied)
        self._audio_handle = proxied.audio_handle
        self._varays_entity = False
        if self._audio_handle is not None:
            self._create_sound(proxied.matrix_world_with_offset.translation)

    def _create_sound(self, position: Vector3) -> None:
        if self._audio_handle is None:
            logger.info("Could not create audio source, handle is None")
            return
        self._varays_entity = True
        OscManager().send_message("/new_source", [str(self._audio_handle), *position[0:3]])

    def _remove_sound(self) -> None:
        if self._audio_handle is not None and self._varays_entity:
            self._varays_entity = False
            OscManager().send_message("/remove_source", [str(self._audio_handle)])

    def remove(self) -> None:
        self._remove_sound()
        super().remove()

    def dispose(self) -> None:
        self._remove_sound()
        super().dispose()

    def set_matrix(self, matrix: Matrix44, matrix_offset: Matrix44) -> None:
        super().set_matrix(matrix, matrix_offset)
        if self._audio_handle is not None:
            OscManager().send_message("/move_source", [str(self._audio_handle), *matrix.translation[0:3]])
