import logging
from typing import Any, Dict, Optional

from eis.editor_support.osc_manager import OscManager
from eis.editor_support.switcher_manager import SwitcherManager
from eis.engine import register_engine
from eis.engines.sound.engine import SoundEngine
from eis.engines.sound.object import SoundEngineObject
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


@register_engine("vaRays")
class VaraysEngine(SoundEngine):
    """
    Varays sound engine
    """

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config)
        self._last_camera_matrix = Matrix44.identity()

    def initialize(self) -> None:
        OscManager().send_message("/remove_all_sources", [])
        logger.info("Initialized vaRays sound engine")

    def _update_position(self, matrix: Matrix44) -> None:
        OscManager().send_message(prefix="/editor_position", args=[*matrix.translation[0:3]])

    def get_jack_address(self) -> str:
        return "vaRays Convolver:"