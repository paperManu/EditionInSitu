import logging
from abc import abstractmethod
from typing import Any, Dict, Optional

from eis.client.input import LocalInputMethod
from eis.engine import register_engine, Engine
from eis.engines.sound.object import SoundEngineObject
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.picker import Picker
from satmath.matrix44 import Matrix44
from satmath.quaternion import Quaternion
from satmath.vector3 import Vector3

logger = logging.getLogger(__name__)


@register_engine("Sound")
class SoundEngine(Engine[SoundEngineObject, Matrix44, Quaternion, Vector3]):
    """
    Sound Engine
    """

    def __init__(self, config: Optional[Dict[str, Any]] = None) -> None:
        super().__init__(config=config)
        self._last_camera_matrix = Matrix44.identity()

    def initialize(self) -> None:
        pass

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

    @abstractmethod
    def get_jack_address(self) -> str:
        """
        Returns the jack address the switcher_manager should connect to
        """
        return

    # Not really required for the sound engine
    def read_model_from_file(self, path: str) -> Optional[Model]:
        return None

    def convert_model(self, model: Model) -> SoundEngineObject:
        pass

    def _add_model(self, model: Model, parent: Optional[Object3D] = None) -> None:
        assert isinstance(model, Model)
        assert not parent or isinstance(parent, Object3D)

        return True

    def move_camera(self, matrix: Matrix44) -> None:
        if self._last_camera_matrix != matrix:
            self._update_position(matrix)
            self._last_camera_matrix = matrix.copy()

    def _update_position(self, matrix: Matrix44) -> None:
        pass

    def get_picker(self, editor: 'Editor') -> Picker:
        return Picker(self, editor)

    def get_movement_controller(
        self,
        config: Dict[str, Any],
        mapping_config: Dict[str, Any],
        picker: Picker
    ) -> Optional[LocalInputMethod]:
        return None

    def show_status(self, status: str) -> None:
        logger.info(status)

    def hide_status(self) -> None:
        pass

    def engine_matrix(self, mat: Matrix44) -> Matrix44:
        return mat

    def eis_matrix(self, mat: Matrix44) -> Matrix44:
        return mat

    def engine_quaternion(self, quat: Quaternion) -> Quaternion:
        return quat

    def eis_quaternion(self, quat: Quaternion) -> Quaternion:
        return quat

    def engine_vector(self, vec: Vector3) -> Vector3:
        return vec

    def eis_vector(self, vec: Vector3) -> Vector3:
        return vec
