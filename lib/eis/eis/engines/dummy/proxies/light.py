from eis.engine import Engine, proxy
from eis.engines.dummy.engine import DummyEngine
from eis.engines.dummy.proxies.object_3d import DummyObject3DProxy
from eis.graph.light import Light, LightProxy


@proxy(DummyEngine, Light)
class DummyLightProxy(LightProxy, DummyObject3DProxy[Light]):
    def __init__(self, engine: Engine, proxied: Light) -> None:
        super().__init__(engine=engine, proxied=proxied)
