import logging
from typing import Optional

from panda3d import core

from eis.engine import Engine, proxy
from eis.engines.render_pipeline.constants import TEXTURE_USAGE_TO_SORT
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.engines.render_pipeline.proxies.base import RenderPipelineGraphProxyBase
from eis.engines.render_pipeline.proxies.material import RenderPipelineMaterialProxy
from eis.engines.render_pipeline.proxies.texture import RenderPipelineTextureProxy
from eis.graph.geometry import Geometry, GeometryProxy

logger = logging.getLogger(__name__)


@proxy(RenderPipelineEngine, Geometry)
class RenderPipelineGeometryProxy(GeometryProxy, RenderPipelineGraphProxyBase[core.Geom]):
    def __init__(self, engine: Engine, proxied: Geometry) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._geom: Optional[core.Geom] = None
        self._state: Optional[core.RenderState] = None

    @property
    def geom(self) -> core.Geom:
        return self._geom

    @property
    def state(self) -> core.RenderState:
        return self._state

    def update_graph(self) -> None:
        super().update_graph()
        self.make()
        proxied_mesh = self.proxied.mesh
        if proxied_mesh and proxied_mesh.proxy:
            proxied_mesh.proxy.update_graph()

    def make(self) -> None:
        super().make()
        material = self._proxied.material
        if material is None:
            return

        material.update_proxy(self._engine)
        if material.proxy is None:
            return

        assert isinstance(material.proxy, RenderPipelineMaterialProxy)
        for material_texture in material.material_textures:
            texture = material_texture.texture
            if not texture:
                logger.warning("Material texture for material \"{}\" has no texture".format(material.name))
                continue

            texture.update_proxy(self._engine)
            assert isinstance(texture.proxy, RenderPipelineTextureProxy)

    def update(self) -> None:
        super().update()

        geometry = self._proxied
        material = geometry.material

        # TODO: Only update if material is dirty
        if material is None or material.proxy is None:
            self._state = core.RenderState.make_empty()
        else:
            p3d_texture_attrib = core.TextureAttrib.makeDefault()
            p3d_material_attrib = core.MaterialAttrib.make(material.proxy.material)
            p3d_color_attrib = core.ColorAttrib.make_vertex()
            for material_texture in material.material_textures:
                texture = material_texture.texture
                if not texture:
                    continue  # Already logged in make()

                texture_stage = core.TextureStage(texture.name)
                texture_stage.set_sort(TEXTURE_USAGE_TO_SORT.get(material_texture.usage) or 0)
                # add_on_stage returns a new modified/combined texture_attrib (think array.concat()))
                p3d_texture_attrib = p3d_texture_attrib.add_on_stage(texture_stage, texture.proxy.texture, 0)

            # Create the geometry RenderState
            self._state = core.RenderState.make(p3d_material_attrib, p3d_texture_attrib, p3d_color_attrib)

        # We do not want to regenerate the geometry if the proxied geometry has not changed
        if not self._proxied.geometry_updated:
            return

        # Create the geometry vertex data
        p3d_geom_vertex_data = core.GeomVertexData(
            geometry.name, core.GeomVertexFormat.get_v3n3c4t2(), core.Geom.UHStatic)
        p3d_geom_vertex_data.set_num_rows(geometry.vertex_count)

        if len(geometry.vertices) > 0:
            p3d_vertex_writer = core.GeomVertexWriter(p3d_geom_vertex_data, 'vertex')
            for vertex in geometry.vertices:
                p3d_vertex_writer.add_data3f(*vertex)

        if len(geometry.normals) > 0:
            p3d_normal_writer = core.GeomVertexWriter(p3d_geom_vertex_data, 'normal')
            for normal in geometry.normals:
                p3d_normal_writer.add_data3f(*normal)

        if len(geometry.texcoords) > 0:
            p3d_texcoord_writer = core.GeomVertexWriter(p3d_geom_vertex_data, 'texcoord')
            for texcoord in geometry.texcoords:
                p3d_texcoord_writer.add_data2f(*texcoord)

        if len(geometry.colors) > 0:
            p3d_color_writer = core.GeomVertexWriter(p3d_geom_vertex_data, 'color')
            for color in geometry.colors:
                p3d_color_writer.add_data4f(*color)

        geom_primitive_type = geometry.primitive_type
        if geom_primitive_type == Geometry.PrimitiveType.POINT:
            p3d_geom_primitive = core.GeomPoints(core.Geom.UHStatic)
        elif geom_primitive_type == Geometry.PrimitiveType.TRIANGLE:
            p3d_geom_primitive = core.GeomTriangles(core.Geom.UHStatic)
        elif geom_primitive_type == Geometry.PrimitiveType.TRIANGLE_STRIP:
            p3d_geom_primitive = core.GeomTristrips(core.Geom.UHStatic)
        else:
            logger.warning("Primitive type \"{}\" not found for geometry \"{}\"".format(
                geom_primitive_type, geometry.name))
            return

        for primitive in geometry.primitives:
            p3d_geom_primitive.add_vertices(*primitive)
            p3d_geom_primitive.close_primitive()

        if not self._geom:
            # First call, we make the geom
            self._geom = core.Geom(p3d_geom_vertex_data)
        else:
            # Subsequent calls, we update the current geom
            self._geom.clear_primitives()
            self._geom.clear_cache()  # NOTE: Not sure this really does help
            self._geom.set_vertex_data(p3d_geom_vertex_data)

            # NOTE: This is a fix for the bounds not being reset correctly
            # (02:54:34 PM) rdb: Ubald, paperManu: ah, I see.  What the intended way to use GeomNode
            # is that you call node.modify_geom(0) to re-acquire a write pointer to the Geom every time
            # you wish to modify it; it will update the bounds automatically that way.  You'll also run
            # into trouble with the multithreaded pipeline if you hold on to your Geom pointer rather
            # than re-acquiring it.
            # (02:54:52 PM) rdb: However, if you don't wish to change that approach, mark_internal_bounds_stale()
            # should help in the meantime.
            if self.proxied.mesh and self.proxied.mesh.proxy and self.proxied.mesh.proxy.geom_node:
                self.proxied.mesh.proxy.geom_node.mark_internal_bounds_stale()
            else:
                logger.error("Can't mark internal bounds stale!")

        self._geom.add_primitive(p3d_geom_primitive)
