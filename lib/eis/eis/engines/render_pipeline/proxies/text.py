import logging
from typing import ClassVar, Dict, TYPE_CHECKING
import os
from panda3d import core
from panda3d.core import TextFont, TextNode

from eis import BASE_PATH
from eis.engine import Engine, proxy
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.engines.render_pipeline.proxies.object_3d import RenderPipelineObject3DProxy
from eis.graph.text import Text

if TYPE_CHECKING:
    pass

logger = logging.getLogger(__name__)

fonts_path = os.path.join(BASE_PATH, "res/fonts")


@proxy(RenderPipelineEngine, Text)
class RenderPipelineTextProxy(RenderPipelineObject3DProxy[Text, core.TextNode]):
    font_cache = {}  # type: ClassVar[Dict[str, TextFont]]

    def __init__(self, engine: Engine, proxied: Text) -> None:
        super().__init__(engine=engine, proxied=proxied)
        self._node = core.TextNode(self.proxied.name)
        self._last_font = None

    def make(self) -> None:
        super().make()
        self._render_pipeline.prepare_text(self._node_path)

    def update(self) -> None:
        super().update()

        if self._node:
            # FONT
            font_name = self.proxied.font
            if font_name and font_name != "" and font_name != self._last_font:
                font = RenderPipelineTextProxy.font_cache.get(font_name)
                if not font:
                    font = self.render_pipeline.base.loader.load_font(
                        os.path.join(fonts_path, font_name),
                        okMissing=True
                    )
                    RenderPipelineTextProxy.font_cache[font_name] = font
                self._last_font = font_name
                if font:
                    self._node.set_font(font)

            # SHADOW
            # self._node.set_shadow(self.proxied.shadow_x, self.proxied.shadow_y)
            # self._node.set_shadow_color(core.LColor(*self.proxied.shadow_color))

            # ALIGN
            if self.proxied.align == Text.Align.CENTER:
                self._node.set_align(TextNode.ACenter)
            elif self.proxied.align == Text.Align.LEFT:
                self._node.set_align(TextNode.ALeft)
            elif self.proxied.align == Text.Align.RIGHT:
                self._node.set_align(TextNode.ARight)

            # TEXT
            self._node.set_text_scale(self.proxied.text_scale)
            self._node.set_text(self.proxied.text)
