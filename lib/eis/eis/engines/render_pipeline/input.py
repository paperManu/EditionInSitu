import logging
from typing import Any, Dict

from panda3d import core  # type: ignore

from eis.client.input import LocalInputMethod
from eis.engines.render_pipeline.engine import RenderPipelineEngine
from eis.inputs.support.buttons import ButtonStateEnum
from eis.picker import Picker
from eis.engines.render_pipeline.picker import RenderPipelinePicker
from satmath.vector3 import Vector3
from satmath.matrix44 import Matrix44

# Silly global task manager from render_pipeline
global taskMgr

logger = logging.getLogger(__name__)


class RenderPipelineMovementController(LocalInputMethod):
    """ This is a helper class, used to control the camera and enable various
    debugging features. It is not really part of the pipeline, but included to
    view the demo scenes. """

    def __init__(self, engine: RenderPipelineEngine, config: Dict[str, Any], mapping_config: Dict[str, Any], picker: Picker) -> None:
        super().__init__(config=config, mapping_config=mapping_config, picker=picker)

        # Doesn't make sense to get another kind of picker here
        assert isinstance(picker, RenderPipelinePicker)

        picker.manual = True
        self._engine = engine

        self._modifiers = [
            'meta', 'lmeta', 'rmeta',
            'alt', 'lalt', 'ralt',
            'control', 'lcontrol', 'rcontrol',
            'shift', 'lshift', 'rshift'
        ]

        def prepend_modifier(btn: str, state: ButtonStateEnum):
            if btn not in self._modifiers:
                is_down = self._engine.base.mouseWatcherNode.is_button_down
                if is_down(core.KeyboardButton.meta()):
                    btn = "meta-" + btn
                if is_down(core.KeyboardButton.alt()):
                    btn = "alt-" + btn
                if is_down(core.KeyboardButton.control()):
                    btn = "control-" + btn
                if is_down(core.KeyboardButton.shift()):
                    btn = "shift-" + btn
            self._button_subject.on_next((btn, state))

        self._engine.base.buttonThrowers[0].node().set_button_down_event('keyDown')
        self._engine.base.accept('keyDown', lambda btn: prepend_modifier(btn, ButtonStateEnum.PRESSED))

        self._engine.base.buttonThrowers[0].node().set_button_up_event('keyUp')
        self._engine.base.accept('keyUp', lambda btn: prepend_modifier(btn, ButtonStateEnum.RELEASED))

        self._distance = 1.0
        self._engine.base.accept('wheel_up', self.distance_up)
        self._engine.base.accept('wheel_down', self.distance_down)

        # Disable modifier buttons, we prefix ourselves because the default behavior was not
        # prefixing the key up events
        self._engine.base.mouseWatcherNode.set_modifier_buttons(core.ModifierButtons())
        self._engine.base.buttonThrowers[0].node().set_modifier_buttons(core.ModifierButtons())

        # Disable pandas builtin mouse control
        self._engine.base.disableMouse()

        self._mapping_dict['free_move_forward'] = None
        self._mapping_dict['free_move_backward'] = None
        self._mapping_dict['constrained_move_forward'] = ('map_movement', ['w', 'arrow_up'])
        self._mapping_dict['constrained_move_backward'] = ('map_movement', ['s', 'arrow_down'])
        self._mapping_dict['constrained_move_left'] = ('map_movement', ['a', 'arrow_left'])
        self._mapping_dict['constrained_move_right'] = ('map_movement', ['d', 'arrow_right'])
        self._mapping_dict['constrained_move_up'] = ('map_movement', ['space'])
        self._mapping_dict['constrained_move_down'] = ('map_movement', ['shift'])
        self._mapping_dict['rotate'] = ('map_head_rotation', ['mouse2'])

        self._mapping_dict['select'] = ('map_toggle_action', ['mouse1'])
        self._mapping_dict['hold'] = ('map_toggle_action', ['mouse3'])
        self._mapping_dict['confirm'] = ('map_action', ['enter'])
        self._mapping_dict['help'] = ('map_toggle_action', ['h'])
        self._mapping_dict['recall'] = None
        self._mapping_dict['alternative_select'] = ('map_toggle_action', ['shift-mouse1'])
        self._mapping_dict['quick_translate'] = ('map_long_toggle_action', ['mouse1'])
        self._mapping_dict['quick_rotate'] = ('map_long_toggle_action', ['0'])
        self._mapping_dict['quick_scale'] = ('map_long_toggle_action', ['9'])
        self._mapping_dict['menu'] = ('map_toggle_action', ['mouse3', 'escape'])
        self._mapping_dict['undo'] = ('map_action', ['control-x'])
        self._mapping_dict['reset'] = ('map_action', ['5'])
        self._mapping_dict['slow_down'] = ('map_action', ['1'])
        self._mapping_dict['speed_up'] = ('map_action', ['2'])
        self._mapping_dict['navigate'] = ('map_toggle_action', ['tab'])
        self._mapping_dict['redo'] = ('map_action', ['shift-control-z', 'control-y'])
        self._mapping_dict['axis_x'] = ('map_keyboard_axis', ['d', 'a'])
        self._mapping_dict['axis_y'] = ('map_keyboard_axis', ['w', 's'])
        self._mapping_dict['axis_z'] = None
        self._mapping_dict['axis_w'] = None
        self._mapping_dict['engage'] = None
        self._mapping_dict['hands'] = None
        self._mapping_dict['drag'] = None
        self._mapping_dict['distance'] = None
        self._mapping_dict['toggle_timeline'] = ('map_double_click_action', ['7'])
        self._apply_config()

    @property
    def render_pipeline_picker(self) -> RenderPipelinePicker:
        assert self._picker and isinstance(self._picker, RenderPipelinePicker)
        return self._picker

    def ready(self) -> None:
        super().ready()

        if not self._user or not self._user.editor:
            logger.warning("Ready called on RenderPipelineMovementController without a user or editor")
            return

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        if self._engine.base.mouseWatcherNode.has_mouse():
            mpos = self._engine.base.mouseWatcherNode.getMouse()
            fov = self._engine.base.camLens.get_fov()

            # Set the origin and direction of the picking ray
            if self._picker_active:
                self.render_pipeline_picker.imm_space_ray.setFromLens(self._engine.base.camNode, mpos.get_x(), mpos.get_y())

                # To get the picker direction in the 3D space, we need to rotate its 'screen' direction
                # wrt the editor's orientation
                self.render_pipeline_picker.direction = self.render_pipeline_picker.editor.rotation.mul_vector3(
                    Vector3(self.render_pipeline_picker.imm_space_ray.get_direction()))

                self._primary_orientation = Vector3(self.render_pipeline_picker.imm_space_ray.get_direction()).quaternion().inverse
                self._secondary_orientation = self._primary_orientation

            # Stream the mouse pos
            self._mouse_subject.on_next((
                mpos.get_x() * fov.x,
                mpos.get_y() * fov.y
            ))

            # Update common controls
            self._common_subject.on_next(('distance', self._distance))

    def distance_up(self) -> None:
        self._distance *= 1.1

    def distance_down(self) -> None:
        self._distance /= 1.1
