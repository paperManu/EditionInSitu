"""
Panda3d Engine Implementation
"""

# Import proxies so that they register with the system first
from eis.engines.render_pipeline.proxies.geometry import RenderPipelineGeometryProxy
from eis.engines.render_pipeline.proxies.light import RenderPipelineLightProxy
from eis.engines.render_pipeline.proxies.material import RenderPipelineMaterialProxy
from eis.engines.render_pipeline.proxies.mesh import RenderPipelineMeshProxy
from eis.engines.render_pipeline.proxies.object_3d import RenderPipelineObject3DProxy
from eis.engines.render_pipeline.proxies.primitives.shape import RenderPipelineShapeProxy
from eis.engines.render_pipeline.proxies.text import RenderPipelineTextProxy
from eis.engines.render_pipeline.proxies.texture import RenderPipelineTextureProxy
