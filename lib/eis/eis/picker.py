import logging
from abc import abstractmethod
from typing import Generic, List, Optional, TYPE_CHECKING, Tuple, TypeVar
from uuid import UUID

from rx.subjects import Subject  # type: ignore

from eis.constants import DEBUG_PICKER
from eis.graph.material import Material
from eis.graph.object_3d import Object3D
from eis.graph.primitives.sphere import Sphere
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from eis.client.input import LocalInputMethod

if TYPE_CHECKING:
    from eis.client.editor import ClientEditor
    from eis.graph.model import Model

logger = logging.getLogger(__name__)

T = TypeVar('T')


class Picker(Generic[T]):
    """
    Picker Base Class
    """

    def __init__(self, engine: T, editor: 'ClientEditor') -> None:
        """
        Create a picker

        :param engine: T
        :param editor: ClientEditor
        """
        self._engine = engine
        self._editor = editor

        self._input: Optional[LocalInputMethod] = None
        self._layer = 0

        self._origin = Vector3([0.0, 0.0, 0.0])
        self._origin_corrected = Vector3([0.0, 0.0, 0.0])
        self._direction = Vector3([0.0, 1.0, 0.0])
        self._direction_corrected = Vector3([0.0, 1.0, 0.0])
        self._ray_vector = Vector3([0.0, 1.0, 0.0])

        self._immersive_space_model: Optional[Model] = None
        self._picker_subject = Subject()
        self._cursor_matrix = Matrix44.identity()

        self._picks: List[Tuple[UUID, Vector3]] = []
        self._picked_gui = False
        self._picked_selectively = False

        self._picked_object: Optional[Object3D] = None
        self._picked_location = Vector3()

        """DEBUG"""
        if DEBUG_PICKER:
            from eis.graph.model import Model
            pick_ball_model = Model()
            pick_ball_model.root.add_child(Sphere(
                radius=0.125,
                material=Material(color=(0.0, 0.0, 1.0, 1.0), shading_model=Material.ShadingModel.EMISSIVE)
            ))
            self._pick_ball = pick_ball_model.root
            self._editor.engine.add_model(model=pick_ball_model)

    def __str__(self) -> str:
        return "{}\033[0;0m({}, \033[1;30m{}\033[0;0m)".format(self.__class__.__name__, self._origin, self._direction)

    def _get_model(self) -> Optional['Model']:
        """
        Get the model to pick from

        :return: Optional[Model] - The model, or None
        """
        return self._editor.scene.model

    @property
    def editor(self) -> 'ClientEditor':
        return self._editor

    @property
    def input(self) -> Optional[LocalInputMethod]:
        return self._input

    @input.setter
    def input(self, value: Optional[LocalInputMethod]) -> None:
        if self._input != value:
            self._input = value

    @property
    def layer(self) -> int:
        return self._layer

    @layer.setter
    def layer(self, layer: int) -> None:
        self._layer = max(0, layer)

    @property
    def picker_subject(self) -> Subject:
        return self._picker_subject

    @property
    def origin(self) -> Vector3:
        """
        Picking ray origin in editor/pod coordinates, prior to being projected on the immersive space
        :return: Vector3
        """
        return self._origin.copy()

    @origin.setter
    def origin(self, position: Vector3) -> None:
        self._origin = position.copy()

    @property
    def direction(self) -> Vector3:
        """
        Picking ray direction relative to origin, prior to being projected on the immersive space
        :return: Vector3
        """
        return self._direction.copy()

    @direction.setter
    def direction(self, direction: Vector3) -> None:
        self._direction = direction.normalized.copy()

    @property
    def origin_corrected(self) -> Vector3:
        """
        Get the corrected origin according to the intersection of the picker with the projection surface
        :return: The corrected origin point
        """
        return self._origin_corrected.copy()

    @property
    def direction_corrected(self) -> Vector3:
        """
        Get the corrected direction vector according to the intersection of the picker with the projection surface
        :return:
        """
        return self._direction_corrected.copy()

    @property
    def ray_orientation(self) -> Quaternion:
        """
        Get the quaternion representing the necessary rotation around the up axis to look at the picked direction in the camera referential.
        :return: The quaternion representing the rotation of the picker around the up axis
        """
        return Quaternion.from_vector_track_up(self._direction_corrected)

    @property
    def ray_vector(self) -> Vector3:
        return self._ray_vector.copy()

    @property
    def cursor_matrix(self) -> Matrix44:
        return self._cursor_matrix.copy()

    @property
    def picked_object(self) -> Optional[Object3D]:
        return self._picked_object

    @property
    def picked_location(self) -> Vector3:
        return self._picked_location.copy()

    def step(self, now: float, dt: float) -> None:
        self._picked_gui = False
        self._picked_selectively = False

        self.update_rays()
        self._picker_subject.on_next(self.ray_orientation)

    def _update_pick_from(self, object3d: Optional[Object3D] = None) -> None:
        self._picks = self._pick(object3d)

    @abstractmethod
    def update_rays(self) -> None:
        """
        Update the rays for picking through a projection surface
        :return: None
        """

    @abstractmethod
    def _pick(self, object3d: Optional[Object3D] = None) -> List[Tuple[UUID, Vector3]]:
        """
        Pick the first object

        :return: The picked objects' uuid and the picking position
        """

    def pick_gui(self) -> Tuple[Optional[Object3D], Vector3]:
        """
        Pick inside the gui.

        :return: The picked gui element and the picking position
        """

        self._update_pick_from(self._editor.gui_layer)

        picked_interactive: Optional[Object3D] = None
        picked_location = Vector3()

        for pick in self._picks:
            object3d = self._editor.gui_layer.find_by_uuid(pick[0])
            if object3d is not None and object3d.pickable and object3d.interactive_ancestry:
                interactive = object3d.closest_interactive
                if not interactive:
                    continue

                self._picked_gui = True
                picked_interactive = interactive
                picked_location = pick[1]
                break

        return picked_interactive, picked_location

    def pick_in(self, root: Object3D, interactive_objects: bool = True) -> Tuple[Optional[Object3D], Vector3]:
        """
        Pick in the selected object (including its children), in the same layer as the picker.
        If interactive_objects, it will bypass any notion of layer and look for the closest interactive
        ancestor.

        :param root: Object3D to look from
        :param interactive_objects: If true, only consider objects with interative attribute set to True
        :return: The picked object and the picking position
        """

        self._update_pick_from(root)

        picked_interactive: Optional[Object3D] = None
        picked_location = Vector3()

        for pick in self._picks:
            picked_interactive = root.find_by_uuid(pick[0])
            if picked_interactive is not None:
                if not picked_interactive.pickable:
                    picked_interactive = None
                    continue
                if interactive_objects:
                    interactive = picked_interactive.closest_interactive
                    if not interactive:
                        continue

                    picked_interactive = interactive
                elif picked_interactive.layer != self._layer:
                    continue

                picked_location = pick[1]
                self._picked_selectively = True
                break

        return picked_interactive, picked_location

    def pick(self, interactive_objects: bool = True) -> Tuple[Optional[Object3D], Vector3]:
        """
        Pick the first object with the same layer as the picker.

        :param interactive_objects: If true, try to get the closest interactive object
        :return: The picked object and the picking position
        """

        self._update_pick_from(None)

        if self._picked_gui or self._picked_selectively:
            # We already picked a GUI item so don't try picking the scene
            return (None, Vector3())

        self._picked_object = None

        model = self._get_model()
        if model is not None:
            for pick in self._picks:
                object3d = model.get_object_by_uuid(pick[0])
                if object3d is not None and object3d.pickable:
                    # Return interactive if there is one, target if there isn't any
                    interactive = object3d.closest_interactive if interactive_objects else None

                    if interactive is not None:
                        object3d = interactive

                    if object3d.pickable and object3d.layer is self._layer:
                        self._picked_object = object3d
                        self._picked_location = pick[1]
                        break

            if DEBUG_PICKER:
                self._pick_ball.matrix = Matrix44.from_translation(self._picked_location)

        return (self._picked_object, self._picked_location) if self._picked_object else (None, Vector3())

    def pick_all(self) -> List[Tuple[Object3D, Vector3]]:
        """
        Pick all the objects along the ray as long as they are on the same layer as the picker.

        :return: A list of the objects and their pointed positions
        """

        self._update_pick_from(None)

        model = self._get_model()
        if model is None:
            return []

        object_list: List[Tuple[Object3D, Vector3]] = []
        for pick in self._picks:
            object3d = model.get_object_by_uuid(pick[0])
            if object3d is not None and object3d.pickable and object3d.layer is self._layer:
                object_list.append((object3d, pick[1]))
        return object_list

    def pick_object(self, object3d: Object3D) -> Tuple[bool, Vector3]:
        """
        Pick the position on the given object, ignoring layer it is on.

        :return: Whether the object is along the ray, and the picking position
        """

        self._update_pick_from(None)

        objects = self.pick_all()
        for obj in objects:
            if obj[0] == object3d:
                return True, obj[1]
        return False, Vector3([0, 0, 0])
