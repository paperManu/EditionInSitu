# Import all serializable classes, they should be imported even if not directly used in code because a serializable
# might be coming from the network and we should be able to deserialize and handle it.

from typing import Generic, Optional, Type, TypeVar, cast

import numpy as np  # type: ignore

from eis.constants import CLIENT, SERVER
from satmath.base import BaseObject  # type: ignore
from satmath.euler import Euler  # type: ignore
from satmath.matrix33 import Matrix33  # type: ignore
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore
from satmath.vector4 import Vector4  # type: ignore
from satnet.serialization import CustomSerializer, Serializable

T = TypeVar('T', bound=BaseObject)


class MathSerializer(Generic[T], CustomSerializer[T]):
    def __init__(self, math_type: Type[T]) -> None:
        self.math_type = math_type

    def serialize(self, obj: T) -> Optional[bytes]:
        if not isinstance(obj, self.math_type):
            return None
        return bytes([obj.dtype.num]) + cast(bytes, obj.tobytes())

    def deserialize(self, data: bytes) -> Optional[T]:
        return cast(T, self.math_type(np.frombuffer(buffer=data[1:], dtype=np.dtype(data[0:1]))))


Serializable.add_custom_serializer(0x02, MathSerializer(Matrix44))
Serializable.add_custom_serializer(0x03, MathSerializer(Matrix33))
Serializable.add_custom_serializer(0x04, MathSerializer(Quaternion))
Serializable.add_custom_serializer(0x05, MathSerializer(Euler))
Serializable.add_custom_serializer(0x06, MathSerializer(Vector4))
Serializable.add_custom_serializer(0x07, MathSerializer(Vector3))


# noinspection PyUnresolvedReferences
def import_all() -> None:
    # Non serializable that should still be loaded
    from eis.converters.eisf import EISConverter
    from eis.converters.gltf import GLTFConverter

    # Actions
    from eis.actions.animation import AddKeyframeAction, RemoveKeyframeAction
    from eis.actions.transform_object import TransformObjectAction
    from eis.actions.engine import (
        AddModelAction,
        AddBehaviorAction, RemoveBehaviorAction,
        AddMaterialAction, AddMeshAction,
        AddObject3DAction, AddTextureAction,
        RemoveMaterialAction, RemoveMeshAction,
        RemoveObject3DAction, RemoveTextureAction,
        ChangeSyncAction
    )

    # Commands
    from eis.commands.animation import (
        NextKeyframeCommand, PreviousKeyframeCommand,
        StartTimelineCommand, StopTimelineCommand, ResetTimelineCommand
    )
    from eis.commands.test import TestCommand
    from eis.commands.move import MoveCommand
    from eis.commands.transform import TransformCommand
    from eis.commands.transform_offset import TransformOffsetCommand
    from eis.commands.ready import ReadyCommand
    from eis.commands.session import AddUserCommand, RemoveUserCommand
    from eis.commands.engine import (
        AddModelCommand,
        AddBehaviorCommand, RemoveBehaviorCommand, RemoveBehaviorByTypeCommand,
        AddEventHandlerCommand, RemoveEventHandlerCommand,
        AddMaterialCommand, AddMeshCommand,
        AddObject3DCommand, AddTextureCommand,
        RemoveMaterialCommand, RemoveMeshCommand,
        RemoveObject3DCommand, RemoveTextureCommand,
        PropertyChangedCommand,
        SaveSceneFileCommand, LoadSceneFileCommand,
        LoadSceneCommand
    )

    # Notifications
    from eis.notifications.location import Location
    from eis.notifications.session import PeerConnected, PeerDisconnected, UserAdded, UserRemoved

    # Requests
    from eis.requests.show_me_what_you_got import ShowMeWhatYouGot
    from eis.requests.ping import Ping
    from eis.requests.open_scene import OpenScene
    from eis.requests.load_scene import LoadScene, SceneLoaded, ListScenes, ListedScenes, ListGLTFScenes, ListedGLTFScenes

    # Graph objects
    from eis.graph.camera import Camera
    from eis.graph.camera_presentation import Camera_Presentation
    from eis.graph.light import Light
    from eis.graph.texture import Texture
    from eis.graph.texture_video import TextureVideo
    from eis.graph.sound_object import SoundObject
    from eis.graph.sound_objects.file_sound_object import FileSoundObject
    from eis.graph.sound_objects.satie_object import SatieObject
    from eis.graph.sound_objects.video_sound_object import VideoSoundObject

    # Engines
    from eis.engines.dummy.engine import DummyEngine
    from eis.engines.panda3d.engine import Panda3DEngine
    from eis.engines.render_pipeline.engine import RenderPipelineEngine

    # Entities
    from eis.inputs.htc_vive import HTCVive, PeerHTCVive, ViveStateClient, ViveStatePeer
    if CLIENT:
        from eis.client.session import Peer
        from eis.client.user import LocalEISUser, PeerUser
    elif SERVER:
        from eis.server.session import Peer  # type: ignore
        from eis.server.user import RemoteEISUser, PeerUser  # type: ignore

    # Behaviors
    from eis.graph.behaviors.animation_behavior import AnimationBehavior
    from eis.graph.behaviors.hidden_behavior import HiddenBehavior
    from eis.graph.behaviors.highlight_behavior import HighlightBehavior
    from eis.graph.behaviors.physics.physics_behavior import PhysicsBehavior
    from eis.graph.behaviors.physics.physics_body_behavior import PhysicsBodyBehavior
    from eis.graph.behaviors.random_motion_behavior import RandomMotionBehavior
    from eis.graph.behaviors.test_behavior import TestBehavior
    from eis.graph.behaviors.tracker_behavior import TrackerBehavior
    from eis.graph.behaviors.video_lod_behavior import VideoLODBehavior

    # Event handlers
    from eis.graph.behaviors.physics.physics_behavior import CollisionHandler

    # Primitives
    from eis.graph.text import Text
    from eis.graph.primitives.box import Box
    from eis.graph.primitives.circle import Circle
    from eis.graph.primitives.cone import Cone
    from eis.graph.primitives.cylinder import Cylinder
    from eis.graph.primitives.plane import Plane
    from eis.graph.primitives.ring import Ring
    from eis.graph.primitives.sphere import Sphere
    from eis.graph.primitives.torus import Torus
    from eis.graph.primitives.shape import Shape
    from eis.graph.primitives.shapes.arrow import Arrow
    from eis.graph.primitives.shapes.circle import Circle  # type: ignore
    from eis.graph.primitives.shapes.crosshair import Crosshair
    from eis.graph.primitives.shapes.rectangle import Rectangle
    from eis.graph.primitives.shapes.ring import Ring  # type: ignore
