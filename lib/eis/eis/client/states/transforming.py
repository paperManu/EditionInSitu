# Transforming idle state
from typing import Any
from transitions.core import EventData  # type: ignore

from eis.client.input import LocalInputMethod
from eis.client.states.base_eis_state import BaseEISState
from eis.display.components.cursor import Cursor

import eis.display.components.menu_description as menu_description
import eis.display.components.menus.editor_menu as editor_menu
from eis.display.components.menus.menu_actions.editing_menus import transforming_menu, Transformations

from satnet.commands.actions import RedoCommand, UndoCommand


class TransformingState(BaseEISState):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, name="transforming", verb="transform", recallable=True, **kwargs)

        # Cursor
        self._cursor = lambda: Cursor(
            selection_chevron=False,
            highlight_on_hover=False,
            label="",
            detail="",
            color=(0.15, 0.65, 0.95, 1.00),
        )

        # Menu
        menu_geometry = menu_description.MenuGeometry(self.editor.config).current_menu_geometry
        self.menu = lambda: transforming_menu(editor_menu.get_menu(menu_geometry))()

        # Syntax: ("user subject", "_local_method (without self.)", "help description (None disables the help)")
        self._inputs_mapping.extend([("navigate", "_navigate", "Navigate (while pressed)"),
                                     ("select", "_start_transforming", "Modify the object"),
                                     ("undo", "_undo", "Undo"),
                                     ("redo", "_redo", "Redo")])

        self._enabled_transformation = None
        self._transform_lock = False

    def enter(self, event: EventData) -> None:
        super().enter(event)
        transformation = event.kwargs.get('transformation')
        if transformation is not None:
            self._enabled_transformation = Transformations(transformation)

    def should_enter(self, event: EventData) -> bool:
        should = super().should_enter(event)
        can = self.client.connected
        return should and can

    @property
    def enabled_transformation(self) -> str:
        return self._enabled_transformation.value

    @enabled_transformation.setter
    def enabled_transformation(self, value: str) -> None:
        if self._enabled_transformation is not Transformations(value):
            self._enabled_transformation = Transformations(value)

    @property
    def transform_lock(self) -> bool:
        return self.transform_lock

    @transform_lock.setter
    def transform_lock(self, value: bool) -> None:
        if self._transform_lock != value:
            self._transform_lock = value

    def _navigate(self, args: LocalInputMethod) -> None:
        if args[1]:
            if self.editor.synchronized_physical_body is not None:
                self.machine.walk()
            else:
                self.machine.fly()

    def _undo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(UndoCommand())

    def _redo(self, input: LocalInputMethod) -> None:
        if self._machine.state_object != self:
            return
        self._machine.client.session.command(RedoCommand())

    def _start_transforming(self, args: LocalInputMethod) -> None:
        if args[1] and not self._transform_lock:
            if self._enabled_transformation == Transformations.TRANSLATING:
                self.machine.translate(input=args[0], quick=False)
            elif self._enabled_transformation == Transformations.ROTATING:
                self.machine.rotate(input=args[0], quick=False)
            elif self._enabled_transformation == Transformations.SCALING:
                self.machine.scale(input=args[0], quick=False)
