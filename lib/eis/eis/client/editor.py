import logging
import time
from threading import Thread
from typing import Any, Dict, List, Optional, TYPE_CHECKING

import math
import pybullet as bullet

from eis import BASE_PATH
from eis.actions.transform_object import TransformObjectAction
from eis.client.user import LocalEISUser
from eis.editor import Editor, EditorDelegate, defaultEditorConfig
from eis.editor_support.asset_library import AssetLibrary
from eis.editor_support.cursor_manager import CursorManager
from eis.editor_support.menu_manager import MenuManager
from eis.editor_support.osc_manager import OscManager
from eis.editor_support.presentation_manager import PresentationManager
from eis.editor_support.satie_manager import SatieManager
from eis.editor_support.switcher_manager import SwitcherManager
from eis.engine import Engine
from eis.graph.behaviors.auto_keyframe_behavior import AutoKeyframeBehavior
from eis.graph.behaviors.highlight_behavior import HighlightBehavior
from eis.graph.behaviors.physics.physics_body_behavior import PhysicsBodyBehavior
from eis.graph.camera import Camera
from eis.graph.material import Material
from eis.graph.model import Model
from eis.graph.object_3d import Object3D
from eis.graph.primitives.cylinder import Cylinder
from eis.graph.scene import Scene
from eis.requests.load_scene import ListScenes, ListGLTFScenes
from satmath.euler import Euler  # type: ignore
from satmath.matrix44 import Matrix44  # type: ignore
from satmath.quaternion import Quaternion  # type: ignore
from satmath.vector3 import Vector3  # type: ignore

if TYPE_CHECKING:
    from eis.client.state_machine import StateMachine

logger = logging.getLogger(__name__)

defaultEditorConfig['offset.rotation'] = [0.0, 0.0, 0.0]
defaultEditorConfig['assets.assets_path'] = "./res"
defaultEditorConfig['assets.assets_per_floor'] = 8
defaultEditorConfig['assets.asset_loading_distance'] = 2.0
defaultEditorConfig['navigation.height'] = 0
defaultEditorConfig['navigation.reset_position'] = [0, 0, 0]
defaultEditorConfig['osc.port'] = 9000
defaultEditorConfig['osc.destination'] = "localhost"


class ClientEditorDelegate(EditorDelegate[LocalEISUser]):

    def on_location_changed(self, location: Optional[Vector3] = None, rotation: Optional[Quaternion] = None) -> None:
        """
        Called when the user's location has changed.
        If only one of the location or rotation has changed, the other will be `None`.

        :param location: Optional[Vector3]
        :param rotation: Optional[Vector3]
        :return: None
        """


class ClientEditor(Editor[LocalEISUser, ClientEditorDelegate]):
    """
    EIS client editor
    Place client-specific editor code or overrides here.
    """

    def __init__(
        self,
        engine: Engine,
        machine: 'StateMachine',
        delegate: Optional[ClientEditorDelegate] = None,
        config: Optional[Dict[str, Any]] = None
    ) -> None:
        super().__init__(engine=engine, delegate=delegate, config=config)

        self._machine = machine

        # 3D model of the immersion space
        self._immersive_space_model: Optional[Model] = None
        self.load_immersive_space_model()

        # Location (same as in server/session)
        self._location = Vector3()
        self._last_location = Vector3()
        self._rotation = Quaternion()
        self._last_rotation = Quaternion()
        self._matrix = Matrix44.identity()
        self._dirty_matrix = False
        self._tilted_matrix = False
        self._rotation_offset = Quaternion.from_euler(
            Euler([angle * math.pi / 180.0 for angle in self.config.get('offset.rotation')]))
        self._physical_offset = Vector3((0.0, 0.0, self.config.get('navigation.height')))

        # Sessions owns the location since we don't do split screen for users
        self._check_update_location_task = self._task_manager.create_task(
            callback=self._check_update_location,
            frequency=self.config.get('location.update.rate')
        )

        # GUI scene
        self._gui_scene = Scene(engine=self._engine)
        if self._engine is not None:
            self._engine.add_model(self._gui_scene.model, asynchrone=False)
        self._gui_scene.editor = self

        """ Gui Layer """
        self._gui_layer: Object3D = Object3D(name='_gui_layer')
        self._gui_scene.model.root.add_child(self._gui_layer)
        self._gui_layer.gui = True

        """ Menu Manager """
        self._menu_layer: Object3D = Object3D(name='_menu_layer')
        self._gui_layer.add_child(self._menu_layer)
        self._menu_manager = MenuManager(self, self._menu_layer)

        """ Cursor Manager """
        self._cursor_layer: Object3D = Object3D(name='_cursor_layer')
        self._gui_layer.add_child(self._cursor_layer)
        self._cursor_manager = CursorManager(self, self._cursor_layer)

        """ Helpers Layer """
        self._helpers_layer: Object3D = Object3D(name='_helpers_layer')
        self._gui_scene.model.root.add_child(self._helpers_layer)

        """ Asset loader layer """
        self._asset_layer: Object3D = Object3D()
        self._asset_library = AssetLibrary(
            self,
            self.asset_layer,
            config=self._config
        )
        self._gui_scene.model.root.add_child(self._asset_layer)

        """ Physics engine initialization """
        gui_physics = True if (self.config.get('physics.gui') or 'no') == 'yes' else False
        self._root_physics = bullet.connect(bullet.GUI if gui_physics else bullet.DIRECT)
        bullet.setGravity(0, 0, -9.8)
        if gui_physics:
            bullet.setRealTimeSimulation(1)
        else:
            bullet.setTimeStep(0.01)

        def step_simulation(period: float) -> None:
            while True:
                start = time.time()
                bullet.stepSimulation()
                step_duration = time.time() - start
                if step_duration < period:
                    time.sleep(period - step_duration)

        if not gui_physics:
            # Because of the loop in step_simulation, we won't exit on our own.
            # the flag daemon allows us to be killed off when non-daemon threads are terminated
            # In our case, when the main program exit
            self._physics_simulation_thread = Thread(target=step_simulation, args=(0.01,), daemon=True)
            self._physics_simulation_thread.start()

        """ Camera """
        self._camera: Optional[Object3D] = None
        self._previous_frame_time = 0

        """ Physics object """
        self._local_physical_body = Cylinder(name="body", matrix=Matrix44.from_x_rotation(
            theta=-math.pi / 2), material=Material(color=(0.0, 100.0, 0.0, 1.0)), height=0.1)
        self._local_physical_body.interactive = True
        self._local_physical_body.visible = False
        self._local_physical_body.pickable = False
        self._local_physical_body.add_behavior(behavior=PhysicsBodyBehavior(density=1.0))
        self._synchronized_physical_body: Optional[Object3D] = None

        """ Presentation manager """
        self._presentation_manager = PresentationManager(editor=self, config=self._config)

        """ Switcher manager """
        self._switcher_manager = SwitcherManager(config=self._config, sound_engine=self.machine.sound_engine)

        """ Satie manager """
        SatieManager(config=self._config)

        """ OSC Manager """
        self._osc_manager = OscManager(port=self.config.get('osc.port'), destination=self.config.get(
            'osc.destination'))  # Shoot config stuff as params

        """ Object multi-selection """
        self._active_object: Optional[Object3D] = None
        self._last_active_object: Optional[Object3D] = None
        self._passive_objects: List[Object3D] = []

        # Initialize behaviors for active and passive objects
        self._active_behavior = HighlightBehavior(color=(0.0, 1.0, 0.0, 1.0), speed=0.5)
        self._passive_behaviors: Dict[Object3D, HighlightBehavior] = {}
        self._passive_behavior_color = (0.0, 1.0, 1.0, 1.0)
        self._passive_behavior_speed = 0.5

        """ UI """
        self._ui_scale = 1.0

        """ For replacing objects """
        self._delete_active_object = False  # Will delete the active object when next entering the editing_scene state

    @property
    def local_physical_body(self) -> Object3D:
        return self._local_physical_body

    @local_physical_body.setter
    def local_physical_body(self, new_physical_body) -> None:
        self._local_physical_body = new_physical_body

    @property
    def synchronized_physical_body(self) -> Optional[Object3D]:
        return self._synchronized_physical_body

    @synchronized_physical_body.setter
    def synchronized_physical_body(self, new_synchronized_physical_body) -> None:
        self._synchronized_physical_body = new_synchronized_physical_body

    @property
    def camera(self) -> Optional[Object3D]:
        return self._camera

    @camera.setter
    def camera(self, new_camera: Optional[Camera]):
        if new_camera is not self._camera:
            if self._camera is not None:
                self._camera.set_as_editor_camera(False, None)
            self._camera = new_camera

            if new_camera is not None:
                # add a callback
                def callback() -> None:
                    if self._camera.get_behavior_by_type(AutoKeyframeBehavior) is not None:
                        return
                    if not self.timeline.running and self.timeline.time == self._previous_frame_time:
                        return
                    if self._matrix == self._camera.matrix_world:
                        return
                    self._location = self._camera.matrix_world.translation
                    self._rotation = Quaternion.from_matrix(self._camera.matrix_world)
                    self._dirty_matrix = True

                self._camera.set_as_editor_camera(True, callback)
                if self._matrix != self._camera.matrix_world:
                    self._location = self._camera.matrix_world.translation
                    self._rotation = Quaternion.from_matrix(self._camera.matrix_world)
                    self._dirty_matrix = True

        if self._synchronized_physical_body is not None:
            self._move_synchronized_physical_body()

    @property
    def machine(self) -> 'StateMachine':
        """
        Parent machine this editor is owned by
        :return: StateMachine
        """
        return self._machine

    @property
    def cursor_manager(self) -> CursorManager:
        return self._cursor_manager

    @property
    def menu_manager(self) -> MenuManager:
        return self._menu_manager

    @property
    def asset_library(self) -> AssetLibrary:
        return self._asset_library

    @property
    def presentation_manager(self) -> PresentationManager:
        return self._presentation_manager

    @property
    def asset_layer(self) -> Object3D:
        return self._asset_layer

    @property
    def helpers_layer(self) -> Object3D:
        return self._helpers_layer

    @property
    def gui_layer(self) -> Object3D:
        return self._gui_layer

    @property
    def physical_offset(self) -> Vector3:
        return self._physical_offset.copy()

    @property
    def location(self) -> Vector3:
        """
        Location of the editor.
        This is not the user's location as we allow multiple users per session,
        but they all share the same "editor/session location".
        :return: Vector3
        """
        return self._location.copy()

    @location.setter
    def location(self, value: Vector3) -> None:
        if self._location != value:
            self._location = value.copy()
            self._dirty_matrix = True

    @property
    def rotation(self) -> Quaternion:
        """
        Rotation of the editor.
        This is not the user's rotation as we allow multiple users per session,
        but they all share the same "editor/session rotation".
        :return: Vector3
        """
        return self._rotation.copy()

    @rotation.setter
    def rotation(self, value: Quaternion) -> None:
        if self._rotation != value:
            self._rotation = value.copy()
            self._dirty_matrix = True

    @property
    def matrix(self) -> Matrix44:
        """
        Matrix (location+rotation) of the editor
        This is not the user's matrix as we allow multiple users per session,
        but they all share the same "editor/session matrix".
        :return: Matrix44
        """
        return self._matrix.copy()

    @matrix.setter
    def matrix(self, value: Matrix44) -> None:
        if self._matrix != value:
            self._matrix = value.copy()
            loc, rot, scale = self._matrix.decompose()
            self._location = loc
            self._rotation = rot
            self._dirty_matrix = False
            self._tilted_matrix = True
            self._gui_scene.model.root.matrix = self._matrix

    @property
    def rotation_offset(self) -> Quaternion:
        """
        Rotation offset applied to the editor
        :return: Quaternion
        """
        return self._rotation_offset.copy()

    @rotation_offset.setter
    def rotation_offset(self, value: Quaternion) -> None:
        """
        Rotation offset setter
        :return: None
        """
        if self._rotation_offset != value:
            self._rotation_offset = value.copy()

    @property
    def immersive_space_model(self) -> Optional[Model]:
        """
        Get the 3D model of the immersive space
        :return: Model
        """
        return self._immersive_space_model

    @immersive_space_model.setter
    def immersive_space_model(self, model: Model) -> None:
        """
        Set the 3D model of the immersive space
        :param model: Model
        :return: None
        """
        logger.info("Setting model {} as immersive space model".format(model))
        self._immersive_space_model = model

    @property
    def active_object(self) -> Optional[Object3D]:
        """
        Get the currently active object
        :return: Object3D
        """
        return self._active_object

    @active_object.setter
    def active_object(self, value: Optional[Object3D]) -> None:
        """
        Setter for the currently active object. The last active object
        is set to self._active_object if the latter is not None.
        :param value: Object3D
        """
        if self._active_object is not value:
            if self._active_object is not None:
                self._active_object.remove_behavior(self._active_behavior)
                self._last_active_object = self._active_object
            if value is not None:
                value.add_behavior(self._active_behavior)
                if value in self._passive_objects:
                    self.remove_passive_object(value)
            self._active_object = value

    @property
    def last_active_object(self) -> Optional[Object3D]:
        """
        Get the last active object
        :return: Object3D
        """
        return self._last_active_object

    @property
    def passive_objects(self) -> List[Object3D]:
        """
        Get the list of passive objects (selected but not active)
        :return: List of Object3D
        """
        return self._passive_objects

    def add_passive_object(self, value: Object3D) -> None:
        """
        Add an Object3D to the list of passive objects
        :param value: Object3D to add as passive object
        """
        if value not in self._passive_objects:
            if self._active_object is value:
                self.active_object = None
            behavior = HighlightBehavior(color=self._passive_behavior_color, speed=self._passive_behavior_speed)
            value.add_behavior(behavior)
            self._passive_behaviors[value] = behavior
            self._passive_objects.append(value)

    def remove_passive_object(self, value: Object3D) -> None:
        """
        Remove a passive object from the list
        :param value: Object3D to remove
        """
        if value in self._passive_objects:
            value.remove_behavior(self._passive_behaviors[value])
            self._passive_behaviors.pop(value)
            self._passive_objects.remove(value)

    def clear_passive_objects(self) -> None:
        """
        Clear the list of passive objects
        """
        for this_object in self._passive_objects:
            this_object.remove_behavior(self._passive_behaviors[this_object])
        self._passive_objects.clear()
        self._passive_behaviors.clear()

    @property
    def delete_active_object(self) -> bool:
        return self._delete_active_object

    @delete_active_object.setter
    def delete_active_object(self, delete_active_object: bool) -> None:
        self._delete_active_object = delete_active_object

    @property
    def ui_scale(self) -> float:
        return self._ui_scale

    @ui_scale.setter
    def ui_scale(self, value: float) -> None:
        if self._ui_scale != value:
            self._ui_scale = value

    def step(self, now: float, dt: float) -> None:
        super().step(now=now, dt=dt)

        # Note: Should this be before or after user updates?
        if self._dirty_matrix:
            self._matrix = Matrix44.from_translation(self._location).mul_quaternion(self._rotation)

            # If the editor matrix changes, so does the camera object associated to it
            if self._camera is not None:
                if (not self._timeline.running and self._timeline.time == self._previous_frame_time) or self._camera.get_behavior_by_type(AutoKeyframeBehavior) is not None:
                    self._machine.client.session.action(TransformObjectAction(object=self._camera, matrix=self.matrix))

            self._dirty_matrix = False
            self._tilted_matrix = True

        # This takes care of making sure the camera is orientated upwards
        if self._tilted_matrix:
            self._rotation = self._rotation.mul_vector3(Vector3((0.0, 1.0, 0.0))).quaternion()
            self._matrix = Matrix44.from_translation(self._location).mul_quaternion(self._rotation)
            self._tilted_matrix = False

        # Make sure that the cursor follows the picker location
        self._gui_scene.model.root.matrix = self._matrix

        if self._synchronized_physical_body:
            if (not self.timeline.running and self._timeline.time == self._previous_frame_time) or (self._camera is not None and self._camera.get_behavior_by_type(AutoKeyframeBehavior) is not None):
                self.location = self._synchronized_physical_body.matrix_world.translation + self._physical_offset
            else:
                self._move_synchronized_physical_body()

        # Move the engine camera to match our location/rotation
        self.machine.engine.move_camera(self._matrix * Matrix44.from_quaternion(self._rotation_offset))
        self.machine.sound_engine.move_camera(self._matrix * Matrix44.from_quaternion(self._rotation_offset))

        # Update the cursor manager, I'm not kidding
        self._cursor_manager.update()
        self._menu_manager.update()

        # Update the asset library
        self._asset_library.step(now=now, dt=dt)

        # Update the presentation manager
        self._presentation_manager.step(now=now, dt=dt)

        self._previous_frame_time = self._timeline.time

    def remove_user(self, user: LocalEISUser) -> None:
        self.menu_manager.remove_user(user)
        super().remove_user(user=user)

    def load_immersive_space_model(self) -> None:
        import os

        immersion_space_model_path = self._config.get('pod.model_path')
        if immersion_space_model_path is None:
            return

        immersion_space_model_path = os.path.realpath(os.path.join(BASE_PATH, immersion_space_model_path))

        def load_callback(model: Optional[Model]) -> None:
            self._immersive_space_model = model

        self._engine.read_model_from_file_async(path=immersion_space_model_path, callback=load_callback)

    def _move_synchronized_physical_body(self):
        self._synchronized_physical_body.location = self._location - self._physical_offset
        self._synchronized_physical_body.get_behavior_by_type(PhysicsBodyBehavior).reset_base_position(
            position=Vector3((self._synchronized_physical_body.location)))

    def _check_update_location(self) -> None:
        """
        Task callback that checks if the location/rotation has changed and calls
        the delegate only when it has.
        :return: None
        """
        location_changed = self._location != self._last_location
        rotation_changed = self._rotation != self._last_rotation

        if not location_changed and not rotation_changed:
            return

        if self._delegate:
            self._delegate.on_location_changed(
                location=self._location if location_changed else None,
                rotation=self._rotation if rotation_changed else None
            )

        self._last_location = self._location
        self._last_rotation = self._rotation

    def list_scenes(self, callback) -> None:
        self._machine.client.session.request(ListScenes(callback))

    def list_gltf_scenes(self, callback) -> None:
        self._machine.client.session.request(ListGLTFScenes(callback))
