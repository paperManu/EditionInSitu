import logging
from abc import abstractmethod
from time import time
from typing import Any, Dict, List, Optional

logger = logging.getLogger(__name__)


class Cacheable:
    def __init__(self) -> None:
        self._lifetime = CacheManager.get_lifetime()
        self._total_usage = 0.0
        self._last_used = time()

    @property
    def lifetime(self) -> float:
        return self._lifetime

    @lifetime.setter
    def lifetime(self, value: float) -> None:
        self._lifetime = value

    @property
    def total_usage(self) -> float:
        return self._total_usage

    @total_usage.setter
    def total_usage(self, value: float) -> None:
        self._total_usage = value

    @property
    def last_used(self) -> float:
        return self._last_used

    @last_used.setter
    def last_used(self, value: float) -> None:
        self._last_used = value

    @abstractmethod
    def get_content(self) -> Any:
        raise NotImplementedError


class CacheManager:
    """
    Manages cached objects lifecycle.
    Removes them when they expire (not used for a specified lifetime) and gives access to them by key and hash signature.
    It has to be a fully static class because objects do not know the editor before being added to the scene and the
    caching might need to happen before that (e.g: geometry generation in primitives).
    """
    _caches: Dict[str, Dict[int, Cacheable]] = {}
    _lifetime = 0.0

    @classmethod
    def get_lifetime(cls) -> float:
        return cls._lifetime

    @classmethod
    def set_lifetime(cls, lifetime: float) -> None:
        cls._lifetime = lifetime

    @classmethod
    def check_validity(cls) -> None:
        for _, cache in cls._caches.items():
            to_remove = []  # type: List[int]
            for signature, cacheable in cache.items():
                current_time = time()
                cacheable.total_usage += current_time - cacheable.last_used
                if cacheable.lifetime != 0.0 and cacheable.total_usage > cacheable.lifetime:
                    to_remove.append(signature)
            for signature in to_remove:
                cache.pop(signature)

    @classmethod
    def get_cacheable(cls, key: str, signature: int) -> Optional[Cacheable]:
        cache = cls._caches.get(key)
        if cache:
            cacheable = cache.get(signature)
            if cacheable:
                cacheable.last_used = time()
            return cacheable
        return None

    @classmethod
    def add_cacheable(cls, key: str, signature: int, cacheable: Cacheable) -> None:
        cache = cls._caches.get(key)
        if not cache:
            cache = cls._caches[key] = {}
        cache[signature] = cacheable
