import logging
from typing import Any, Dict, Optional, Union, TYPE_CHECKING
from uuid import UUID

from eis.action import ActionId, EISAction
from eis.graph.behaviors.animation_behavior import AnimationBehavior
from eis.graph.object_3d import Object3D
from eis.graph.texture import Texture
from satnet.action import action

if TYPE_CHECKING:
    from eis.server.session import EISRemoteSession

logger = logging.getLogger(__name__)


@action(id=ActionId.ADD_KEYFRAME)
class AddKeyframeAction(EISAction):

    _fields = ['object_id', 'attribute', 'time', 'value']

    def __init__(
        self,
        context: Optional[UUID] = None,
        object: Optional[Union[Object3D, Texture]] = None,
        attribute: Optional[str] = None,
        time: Optional[float] = None,
        value: Optional[Any] = None
    ) -> None:
        super().__init__(context=context)
        self.object_id = object.uuid if object is not None else None
        self.attribute = attribute if attribute is not None else None
        self.time = time if time is not None else None
        self.value = value if value is not None else None

    def apply(self, session: 'EISRemoteSession') -> None:
        assert(self.attribute is not None)
        assert(self.time is not None)
        assert(self.value is not None)

        if self.object_id is None:
            logger.warning("No object id in action")
            return

        object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        assert(isinstance(object, Object3D) or isinstance(object, Texture))
        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            animation_behavior = AnimationBehavior()
            object.add_behavior(animation_behavior)

        assert(isinstance(animation_behavior, AnimationBehavior))
        animation_behavior.add_timed_keyframe_with_value(self.attribute, self.time, self.value)

    def revert(self, session: 'EISRemoteSession') -> None:
        assert(self.attribute is not None)
        assert(self.time is not None)

        if self.object_id is None:
            logger.warning("No object id in action")
            return

        object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        assert(isinstance(object, Object3D) or isinstance(object, Texture))
        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            logger.warning("The object \"{}\" has no animation behavior".format(object.name))
            return

        assert(isinstance(animation_behavior, AnimationBehavior))
        animation_behavior.remove_keyframe(self.attribute, self.time)


@action(id=ActionId.REMOVE_KEYFRAME)
class RemoveKeyframeAction(EISAction):

    _fields = ['object_id', 'time']

    def __init__(
        self,
        context: Optional[UUID] = None,
        object: Optional[Union[Object3D, Texture]] = None,
        time: Optional[float] = None
    ) -> None:
        super().__init__(context=context)
        self.object_id = object.uuid if object is not None else None
        self.time = time if time is not None else None
        self.keyframes: Optional[Dict[str, Any]] = None

    def apply(self, session: 'EISRemoteSession') -> None:
        assert(self.time is not None)

        if self.object_id is None:
            logger.warning("No object id in action")
            return

        object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        assert(isinstance(object, Object3D) or isinstance(object, Texture))
        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            logger.warning("No animation behavior")
            return

        assert(isinstance(animation_behavior, AnimationBehavior))
        self.keyframes = animation_behavior.get_keyframe_for_all_attributes(self.time)
        animation_behavior.remove_keyframe_for_all_attributes(self.time)

    def revert(self, session: 'EISRemoteSession') -> None:
        if self.keyframes is None:
            return

        object = session.editor.scene.model.get_entity_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        assert(isinstance(object, Object3D) or isinstance(object, Texture))
        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            animation_behavior = AnimationBehavior()
            object.add_behavior(animation_behavior)

        assert(isinstance(animation_behavior, AnimationBehavior))
        for attribute, value in self.keyframes.items():
            animation_behavior.add_timed_keyframe_with_value(attribute, self.time, value)


@action(id=ActionId.LOOP_ANIMATION)
class LoopAnimation(EISAction):

    _fields = ['object_id', 'looping']

    def __init__(
        self,
        context: Optional[UUID] = None,
        object: Optional[Object3D] = None,
        looping: Optional[bool] = None
    ) -> None:
        super().__init__(context=context)
        self.object_id = object.uuid if object is not None else None
        self.looping = looping
        self.previous_looping: Optional[bool] = None

    def apply(self, session: 'EISRemoteSession') -> None:
        assert(self.looping is not None)

        if self.object_id is None:
            logger.warning("No object id in action")
            return

        object = session.editor.scene.model.get_object_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            logger.warning("No animation behavior")
            return

        self.previous_looping = animation_behavior.looping
        animation_behavior.looping = self.looping

    def revert(self, session: 'EISRemoteSession') -> None:
        assert(self.previous_looping is not None)

        if self.object_id is None:
            logger.warning("No object id in action")
            return

        object = session.editor.scene.model.get_object_by_uuid(self.object_id)
        if object is None:
            logger.warning("Could not find object with id \"{}\"".format(self.object_id))
            return

        animation_behavior = object.get_behavior_by_type(AnimationBehavior)
        if animation_behavior is None:
            logger.warning("No animation behavior")
            return

        animation_behavior.looping = self.previous_looping
