import os

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext


# Utility function to read the README.md
def read(fname: str) -> str:
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='EiS',
    version="0.0.2",
    author="François Ubald Brien, Emmanuel Durand, Émile Ouellet-Delorme, Michal Seta, Jérémie Soria",
    description="Immersive environment editing tool",
    long_description=read("README.md"),
    license="GPL",
    url="https://gitlab.com/sat-metalab/EditionInSitu",
    packages=[
        "eis"
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Multimedia :: Graphics",
        "Topic :: Multimedia :: Sound/Audio",
        "Topic :: Multimedia :: Video",
        "Topic :: Internet",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
    ],
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize([
        Extension("eis/*", ["eis/*.py"], language='c++'),
        Extension("eis/actions/*", ["eis/actions/**/*.py"], language='c++'),
        # Extension("eis/assets/*", ["eis/assets/**/*.py"], language='c++'),  # Not activated because of dynamic module plugin loading
        Extension("eis/client/*", ["eis/client/**/*.py"], language='c++'),
        Extension("eis/commands/*", ["eis/commands/**/*.py"], language='c++'),
        Extension("eis/converters/*", ["eis/converters/**/*.py"], language='c++'),
        Extension("eis/display/*", ["eis/display/**/*.py"], language='c++'),
        Extension("eis/editor_support/*", ["eis/editor_support/**/*.py"], language='c++'),
        Extension("eis/engines/*", ["eis/engines/**/*.py"], language='c++'),
        Extension("eis/graph/*", ["eis/graph/*.py"], language='c++'),
        Extension("eis/graph/behaviors/*", ["eis/graph/behaviors/*.py"], language='c++'),
        Extension("eis/graph/components/*", ["eis/graph/components/*.py"], language='c++'),
        Extension("eis/graph/helpers/*", ["eis/graph/helpers/*.py"], language='c++'),
        Extension("eis/graph/primitives/*", ["eis/graph/primitives/*.py"], language='c++'),
        Extension("eis/inputs/*", ["eis/inputs/**/*.py"], language='c++'),
        Extension("eis/notifications/*", ["eis/notifications/**/*.py"], language='c++'),
        Extension("eis/physics/*", ["eis/physics/**/*.py"], language='c++'),
        Extension("eis/requests/*", ["eis/requests/**/*.py"], language='c++'),
        Extension("eis/server/*", ["eis/server/**/*.py"], language='c++'),
        Extension("eis/utils/*", ["eis/utils/**/*.py"], language='c++'),
    ],
        compiler_directives={
        'always_allow_keywords': True,
        'binding': True,
        'language_level': 3
    }
    )
)
