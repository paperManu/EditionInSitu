"""
Simple ZMQ proxy listening on port 9999 and forwarding to localhost:9000
"""

import zmq
from zmq import Context, Socket, Again
from time import sleep

context = Context.instance()

frontend = context.socket(zmq.ROUTER)
frontend.setsockopt(zmq.LINGER, 0)
frontend.bind("tcp://*:9999")

backend = context.socket(zmq.DEALER)
backend.setsockopt(zmq.LINGER, 0)
backend.connect("tcp://localhost:9000")

while True:
    front_receiving = True
    while front_receiving:
        try:
            msg = frontend.recv_multipart(zmq.NOBLOCK)
            print("<<<", msg)
            backend.send_multipart(msg)
        except Again:
            front_receiving = False
            break

    back_receiving = True
    while back_receiving:
        try:
            msg = backend.recv_multipart(zmq.NOBLOCK)
            print(">>>", msg)
            frontend.send_multipart(msg)
        except Again:
            front_receiving = False
            break

    sleep(0.001)
