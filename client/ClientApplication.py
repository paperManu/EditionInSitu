import logging
import os
import time
from time import sleep
from typing import Optional

from eis.application import EISApplication
from eis.client.state_machine import StateMachine

logger = logging.getLogger(__name__)


class ClientApplication(EISApplication):
    """
    Main client application class
    """

    def __init__(
            self,
            repl: bool = False,
            config: Optional[str] = None,
            profile: bool = False
    ) -> None:
        """
        Create a client application

        :param repl: bool - Whether to start as a REPL or not
        :param config: Optional[str] - Config file
        """
        super().__init__(
            repl=repl,
            app_config=os.path.join(EISApplication.config_path, "client.json"),
            override_config=config,
            profile=profile
        )

        # State Machine
        self._machine = StateMachine(config=self._config, config_local=self._config_local)

        def trace_model():
            print(self._machine.editor.scene.model.trace())

        # Setup REPL
        self._repl_dict = {
            **self._repl_dict,
            'app': self,
            'machine': self._machine,
            'engine': self._machine.engine,
            'editor': self._machine.editor,
            'client': self._machine.client,
            'trace_model': trace_model
        }

    def run(self) -> None:
        self._last = time.time()

        # Engine main loop
        super().run()

    def step(self) -> None:
        now = time.time()
        dt = now - self._last
        self._last = now

        # Step the State Machine
        self._machine.step(now, dt)

        # Sleep
        sleep(0.001)

    if __debug__:
        def test(self) -> None:
            """
            Sample test method, can be used in the REPL
            :return:
            """
            ...
