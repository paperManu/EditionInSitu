import json
import logging
import os
import time
from time import sleep
from typing import Any, Dict, Optional, TYPE_CHECKING

from eis.application import EISApplication
from eis.graph.scene import Scene
from eis.message import EISMessageParser
from eis.server.editor import ServerEditor
from eis.server.server import EISServer
from satnet.adapters.zmq.server import ZMQServer

if TYPE_CHECKING:
    from satnet.server import Server

logger = logging.getLogger(__name__)


class ServerApplication(EISApplication):
    """
    Main server application class
    """

    def __init__(
            self,
            repl: bool = False,
            scene: Optional[str] = None,
            config: Optional[str] = None,
            project: Optional[str] = None,
            profile: bool = False
    ) -> None:
        """
        Create a server application

        :param repl: bool - Whether to start as a REPL or not
        :param config: Optional[str] - Config file
        """
        super().__init__(
            repl=repl,
            app_config=os.path.join(EISApplication.config_path, "server.json"),
            override_config=config,
            profile=profile
        )

        self._stop_application = False
        self._last = 0.0
        self._interval = 1.0 / 60.0

        # Setup the editor
        self._editor = ServerEditor(config=self._config['editor'])

        # Setup the project if specified
        self._project: Dict[Any, Any] = dict()
        if project is not None:
            if project.startswith("/"):
                project_path = os.path.realpath(project)
            else:
                project_path = os.path.realpath(os.path.join(os.getcwd(), project))
            if os.path.exists(project_path):
                with open(project_path, 'r') as project_file:
                    self._project = json.loads(project_file.read())

        # Load startup scene
        if not scene:
            scene = self._project.get("scene")
            if scene:
                if not scene.startswith("/"):
                    scene = os.path.realpath(os.path.join(os.path.dirname(project_path), scene))

        if scene:
            if scene.startswith("/"):
                path = os.path.realpath(scene)
            else:
                path = os.path.realpath(os.path.join(os.getcwd(), scene))
            logger.info("Loading startup scene \"{}\"".format(path))

            scene = self._editor.load_scene_from_file(path=path)
            if scene:
                self._editor.load_scene(scene)
            else:
                logger.error("Could not load scene file")

        # Setup networking
        self._server = EISServer(
            config=self._config['network'],
            adapter=ZMQServer(
                message_parser=EISMessageParser(),
                config=self._config['network.adapter']
            ),
            editor=self._editor,
            project=self._project
        )

        # Setup the editor delegate
        self._editor.delegate = self._server

        def trace_model():
            print(self._editor.scene.model.trace())

        # Setup REPL
        self._repl_dict = {
            **self._repl_dict,
            'app': self,
            'editor': self._editor,
            'server': self._server,
            'trace_model': trace_model
        }

    def run(self) -> None:
        # Run the network server
        self._server.start(port=self._config['network'].get('port'))

        # Save python some precious resolving time
        self._server: Server = self._server
        self._editor: ServerEditor = self._editor

        self._last = time.time()

        # Engine main loop
        super().run()

    def step(self) -> None:
        now = time.time()
        dt = now - self._last
        self._last = now

        # Process incoming messages in the main thread
        self._server.step(now, dt)

        # Step the editor
        self._editor.step(now, dt)

        # Sleep
        after = time.time()
        remaining = self._interval - (after - now)
        sleep(remaining if remaining > 0 else 0.001)

    if __debug__:
        def test(self) -> None:
            """
            Sample test method, can be used in the REPL
            :return:
            """
            ...
