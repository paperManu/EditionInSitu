![Icon](./logo/PNG/Edition_in_situ_HCB.png)

[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![pipeline status](https://gitlab.com/sat-metalab/EditionInSitu/badges/develop/pipeline.svg)](https://gitlab.com/sat-metalab/EditionInSitu/commits/develop)
[![coverage report](https://gitlab.com/sat-metalab/EditionInSitu/badges/develop/coverage.svg)](https://gitlab.com/sat-metalab/EditionInSitu/commits/develop)

Introduction
------------

EiS is an in-situ immersive environment creation tool. It stands for _E_dition _I_n _S_itu. EiS allows for editing an immersive scene from inside the immersive space, using VR controllers. It is meant to be used in a creation workflow similar to what follows:

- create 2D, 3D and sound assets in separate software
- import them into EiS
- create a draft of a scenography with these assets, possibly involving animations
- export to GLTF format
- import in a 3D editor of choice (Blender for example) and improve on the draft

EiS is not meant to be used by itself, it relies on other software from the SAT-Metalab suite:

      ┌───────────────┐ HTC Vive controls
      │  vrpn-openvr  │──────────────────┐
      └───────────────┘                  │
                                         ↓
      ┌────────────┐  Media reading  ┌───────┐   Video output   ┌──────────┐
      │  Switcher  │ ──────────────→ │  EiS  │ ───────────────→ │  Splash  │
      └────────────┘                 └───────┘                  └──────────┘
                                         │
                                         │     OSC messages     ┌─────────┐
                                         └────────────────────→ │  SATIE  │
                                                                └─────────┘

You can find these software here:

- [Switcher](https://gitlab.com/sat-metalab/switcher): Low latency integration software, used to read audio and video files (installed automatically through `setup.sh`)
- [vrpn-openvr](https://gitlab.com/sat-metalab/vrpn-openvr): VRPN server used to communicate with the HTC Vive controllers
- [Splash](https://sat-metalab.gitlab.io/splash): Projection mapping software
- [SATIE](https://gitlab.com/sat-metalab/satie): Sound spatialization engine

Installation
------------

See [Installation](./Installation) for installation instructions.


Usage
-----

See [Usage](./Usage) for usage and configurations.


Contributing and Development
----------------------------

See [Contributing](./Contributing) for development instructions.
