# GRAPHIC CHARTER 


## PRIMARY AND SECONDARY LOGOS

#### GRAPHIC GRID

Here is the graphic grid of the logo, with the proportions to be respected at each use of the logo. Whether it is to appear in an application, on a website, on a letterhead, on a poster or in a series of logos, the proportions must ALWAYS be respected.

![](img/graphic-grid.jpg)

#### PROTECTIVE SPACE

At all times, the logo must be surrounded by a clear area to be legible and communicate effectively. No text, lines, typographical elements, images, photographs or illustrations must occupy this space.

![](img/protection_space_1.jpg)
![](img/protection_space_2.jpg)

#### MINIMUM SIZE

The logo should never be used when the main icon is less than 0.5 inches high.

![](img/minimum-size.jpg)

## COLOR CODES

#### COLORS (GRADIENT)

![](../PNG/Edition_in_situ_C.png)
```
C2 M62 Y0 K0
R247 G100 B255
#f764ff

C1 M95 Y0 K0
R255 G0 B255
#ff00ff

C34 M94 Y0 K0
R165 G0 B169
#a500a9
```
#### BLACK

![](../PNG/Edition_in_situ_N.png)
```
C0 M0 Y0 K30
R179 G179 B179
#b3b3b3

C0 M0 Y0 K70
R77 G77 B77
#4d4d4d

C0 M0 Y0 K100
R0 G0 B0
#000000
```
#### WHITE

![](img/eis-white-with-black-background.jpg)
```
C0 M0 Y0 K0
R255 G255 B255
#ffffff

C0 M0 Y0 K40
R153 G153 B153
#999999

C0 M0 Y0 K70
R77 G77 B77
#4d4d4d
```
## GENERAL USES

### Plain and dark background

To maximize the legibility of the logo on a dark, plain background, it is recommended to use the color or white version of the logo.

NEVER use the black version.

![](img/plain-and-dark-background.jpg)


### Plain and clear background

To maximize the legibility of the logo on a plain and clear background, only the color or black version of the logo is allowed.

NEVER use the white version.

![](img/plain-and-clear-background.jpg)

### Very clear photo

To maximize the legibility of the logo on a very clear photo, it is recommended to use the black version of the logo. 

NEVER use the white version.

![](img/very-clear-photo.jpg)

## Maximum readability

To ensure maximum legibility of the logo, you must never modify or alter any part of the logo.
