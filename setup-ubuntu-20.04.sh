#!/usr/bin/env bash

set -e

source_directory="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
SUDO=''

check_sudo()
{
    if ((${EUID} != 0)); then
        SUDO='sudo'
    fi
}

install_system_packages()
{
    echo Installing EiS system dependencies

    ${SUDO} apt -y install git git-lfs cmake python3 python3-pip python3-setuptools python3-venv python3-pyqt5 libsndfile1-dev
}

install_python_packages()
{
    echo Installing EiS Python dependencies

    pip install wheel
    pip install -r requirements.txt
}

update_submodules()
{
    echo Fetching submodules and LFS

    # Make sure git lfs is up to date
    git lfs fetch && git lfs pull
    git submodule sync --recursive
    git submodule update --init --recursive

    # Required to use gltf addon outside of blender.
    file=../extlib/glTF-Blender-IO/addons/io_scene_gltf2/__init__.py
    if [ -f "$file" ]; then
      rm -f $file
    fi
}

setup_render_pipeline()
{
    echo Setting up render pipeline

    pushd extlib/render_pipeline
    if [[ -z ${DISPLAY} ]]; then
        yes | python3 setup.py --ci-build
    else
        yes | python3 setup.py
    fi
    popd
}

setup_satie()
{
    echo Setting up PySATIE

    pushd extlib/PySATIE
    ${SUDO} apt install -y liblo-dev
    pip3 install -e .
    popd
}

install_shmdata()
{
    echo Installing Shmdata

    pushd extlib
    if [ ! -d 'shmdata' ]; then git clone https://gitlab.com/sat-metalab/shmdata; fi
    cd shmdata
    git checkout master && git pull origin master
    ${SUDO} apt -y -qq install cmake build-essential
    ${SUDO} apt -y -qq install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev python3-dev
    mkdir -p build && cd build && rm -rf *
    cmake ..
    make -j$(nproc) && ${SUDO} make install
    ${SUDO} ldconfig
    popd
}

install_switcher()
{
    echo Installing Switcher

    pushd extlib
    if [ ! -d 'switcher' ]; then git clone --recursive https://gitlab.com/sat-metalab/switcher; fi
    cd switcher
    git checkout master && git pull origin master
    ${SUDO} apt install -y -qq $(cat ./deps/apt-build-ubuntu-20.04) $(cat ./deps/apt-runtime-ubuntu-20.04)
    mkdir -p build && cd build && rm -rf *
    cmake -DPLUGIN_GSOAP=OFF -DPLUGIN_NVENC=OFF -DENABLE_GPL=ON ..
    make -j$(nproc) && ${SUDO} make install
    ${SUDO} ldconfig
    popd
}

install_vrpn()
{
    echo Installing VRPN

    pushd extlib
    if [ ! -d 'vrpn' ]; then git clone --recursive https://gitlab.com/sat-metalab/vrpn; fi
    cd vrpn
    git checkout master && git pull origin master
    mkdir -p build && cd build && rm -rf *
    cmake -DCMAKE_INSTALL_PREFIX=${source_directory}/env ..
    make -j$(nproc) && ${SUDO} make install
    ${SUDO} ldconfig
    popd
}

replace_default_cubemap()
{
    if [ -n "$DISPLAY" ]; then
        # Replace the default cubemap
        pushd res/cubemap
        python3 filter.py
        cp cubemap.txo.pz "${source_directory}"/extlib/render_pipeline/data/default_cubemap
        popd
    fi
}

copy_renderpipeline_config()
{
    # Copy RenderPipeline configuration
    cp res/rp/* extlib/render_pipeline/config/
}

setup_virtual_env()
{
    echo Create and activate the Python virtual environment

    if [ ! -d env ]; then
        python3 -m venv env
    fi
    source ./env/bin/activate
}

exit_virtual_env()
{
    echo Deactivate the Python virtual environment

    deactivate
}

remove_gltf_init()
{
  ./remove_gltf_init.sh
}

check_sudo
install_system_packages
setup_virtual_env
install_python_packages
update_submodules
install_shmdata
install_switcher
install_vrpn
setup_render_pipeline
setup_satie
replace_default_cubemap
copy_renderpipeline_config
remove_gltf_init
exit_virtual_env
